import 'package:get_it/get_it.dart';
import 'package:mim_shmup/core/utils/database/high_scores.service.dart';
import 'package:mim_shmup/core/utils/database/settings.service.dart';

GetIt getIt = GetIt.instance;

Future<void> getDependencies() async {
  getIt.registerSingletonAsync<HighScoresService>(() async {
    final highScores = HighScoresService();
    await highScores.init();
    return highScores;
  });

  getIt.registerSingletonAsync<SettingsService>(() async {
    final parameters = SettingsService();
    await parameters.init();
    return parameters;
  });
}
