import 'dart:ui';

import 'package:flame/extensions.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/utils/path.helper.dart';

abstract class CanvasHelper {
  static double drawPowerUpLevels(
    Canvas canvas, {
    Vector2? position,
    required Vector2 levelSize,
    required double levelPadding,
    required List<bool> levels,
    bool mirror = false,
  }) {
    final Vector2 initialPosition = position ?? Vector2.zero();
    for (int index in List<int>.generate(levels.length, (index) => index)) {
      Vector2 nextPosition = initialPosition +
          Vector2(levelSize.x * index + levelPadding * index, 0) * (mirror ? -1 : 1);
      _drawLevel(
        canvas,
        activated: levels[index],
        position: nextPosition,
        size: levelSize,
        mirror: mirror,
      );
    }
    return 0.0;
  }
}

void _drawLevel(
  Canvas canvas, {
  bool activated = false,
  required Vector2 position,
  required Vector2 size,
  bool mirror = false,
}) {
  final borderPaint = Paint()
    ..color = carbonGrey
    ..style = PaintingStyle.stroke
    ..strokeWidth = 1.5;

  if (activated) {
    final paint = Paint()
      ..color = halloweenOrange
      ..style = PaintingStyle.fill;
    canvas.drawPath(
      PathHelper.getPowerUpPath(
        Offset(position.x, position.y),
        Size(size.x, size.y),
        mirror: mirror,
      ),
      paint,
    );
  }

  canvas.drawPath(
    PathHelper.getPowerUpPath(
      Offset(position.x, position.y),
      Size(size.x, size.y),
      mirror: mirror,
    ),
    borderPaint,
  );
}
