import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';

abstract class PathHelper {
  static Path getCustomButtonPath(Size size) {
    final double widthSegment = size.width - 2 * MiMSizes.clipperButtonDelta;
    final double heightSegment = size.height - 2 * MiMSizes.clipperButtonDelta;

    return Path()
      // Upper side
      ..moveTo(MiMSizes.clipperButtonDelta, 0)
      ..lineTo(widthSegment + MiMSizes.clipperButtonDelta, 0)
      ..lineTo(widthSegment + MiMSizes.clipperButtonDelta * 2, MiMSizes.clipperButtonDelta)
      // Right side
      ..lineTo(size.width, heightSegment + MiMSizes.clipperButtonDelta)
      ..lineTo(
          size.width - MiMSizes.clipperButtonDelta, heightSegment + 2 * MiMSizes.clipperButtonDelta)
      // Bottom Side
      ..lineTo(widthSegment + MiMSizes.clipperButtonDelta, size.height)
      ..lineTo(MiMSizes.clipperButtonDelta, size.height)
      // Left side
      ..lineTo(0, heightSegment + MiMSizes.clipperButtonDelta)
      ..lineTo(0, MiMSizes.clipperButtonDelta)
      ..close();
  }

  static Path getTopComponentPath(Size size) {
    final double topComponentDelta = size.height * MiMSizes.topComponentDeltaRatio;
    final double segmentWidth = (size.width - 2 * topComponentDelta) / 3;
    return Path()
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width - segmentWidth, size.height)
      ..lineTo(size.width - segmentWidth - topComponentDelta, size.height - topComponentDelta)
      ..lineTo(size.width - 2 * segmentWidth - topComponentDelta, size.height - topComponentDelta)
      ..lineTo(size.width - 2 * segmentWidth - 2 * topComponentDelta, size.height)
      ..lineTo(0, size.height)
      ..close();
  }

  static Path getTopComponentBorderPath(Size size) {
    final double topComponentDelta = size.height * MiMSizes.topComponentDeltaRatio;
    final double segmentWidth = (size.width - 2 * topComponentDelta) / 3;

    Path path = Path()
      ..moveTo(0, size.height)
      ..lineTo(segmentWidth, size.height)
      ..lineTo(segmentWidth + size.height * 2, -size.height)
      ..moveTo(size.width - segmentWidth - size.height * 2, -size.height)
      ..lineTo(size.width - segmentWidth, size.height)
      ..lineTo(size.width, size.height);

    return path;
  }

  static Path getPowerUpPath(Offset position, Size size, {bool mirror = false}) {
    final double radius = size.width / 2;
    if (mirror) {
      return Path()
        ..moveTo(position.dx, position.dy + radius)
        ..lineTo(position.dx - 1 * radius, position.dy)
        ..lineTo(position.dx - 2 * radius, position.dy + radius)
        ..lineTo(position.dx - 2 * radius, position.dy + size.height - radius)
        ..lineTo(position.dx - 1 * radius, position.dy + size.height)
        ..lineTo(position.dx, position.dy + size.height - radius)
        ..close();
    }
    return Path()
      ..moveTo(position.dx, position.dy + radius)
      ..lineTo(position.dx + 1 * radius, position.dy)
      ..lineTo(position.dx + 2 * radius, position.dy + radius)
      ..lineTo(position.dx + 2 * radius, position.dy + size.height - radius)
      ..lineTo(position.dx + 1 * radius, position.dy + size.height)
      ..lineTo(position.dx, position.dy + size.height - radius)
      ..close();
  }

  static Path getCustomPagePath(Size size) {
    final double widthSegment = (size.width - 4 * MiMSizes.clipperPageDelta) / 3;
    final double heightSegment = (size.height - 4 * MiMSizes.clipperPageDelta) / 3;

    return Path()
      // Upper side
      ..moveTo(MiMSizes.clipperPageDelta, 0)
      ..lineTo(MiMSizes.clipperPageDelta + widthSegment, 0)
      ..lineTo(widthSegment + MiMSizes.clipperPageDelta * 2, MiMSizes.clipperPageInnerDelta)
      ..lineTo(widthSegment * 2 + MiMSizes.clipperPageDelta * 2, MiMSizes.clipperPageInnerDelta)
      ..lineTo(widthSegment * 2 + MiMSizes.clipperPageDelta * 3, 0)
      ..lineTo(widthSegment * 3 + MiMSizes.clipperPageDelta * 3, 0)
      ..lineTo(size.width, MiMSizes.clipperPageDelta)
      // Right side
      ..lineTo(size.width, heightSegment + MiMSizes.clipperPageDelta)
      ..lineTo(size.width - MiMSizes.clipperPageInnerDelta,
          heightSegment + 2 * MiMSizes.clipperPageDelta)
      ..lineTo(size.width - MiMSizes.clipperPageInnerDelta,
          heightSegment * 2 + MiMSizes.clipperPageDelta * 2)
      ..lineTo(size.width, heightSegment * 2 + MiMSizes.clipperPageDelta * 3)
      ..lineTo(size.width, heightSegment * 3 + MiMSizes.clipperPageDelta * 3)
      ..lineTo(
          size.width - MiMSizes.clipperPageDelta, heightSegment * 3 + MiMSizes.clipperPageDelta * 4)
      // Bottom Side
      ..lineTo(widthSegment * 2 + MiMSizes.clipperPageDelta * 3, size.height)
      ..lineTo(widthSegment * 2 + MiMSizes.clipperPageDelta * 2,
          size.height - MiMSizes.clipperPageInnerDelta)
      ..lineTo(widthSegment + MiMSizes.clipperPageDelta * 2,
          size.height - MiMSizes.clipperPageInnerDelta)
      ..lineTo(widthSegment + MiMSizes.clipperPageDelta, size.height)
      ..lineTo(MiMSizes.clipperPageDelta, size.height)
      ..lineTo(0, size.height - MiMSizes.clipperPageDelta)
      // Left side
      ..lineTo(0, heightSegment * 2 + MiMSizes.clipperPageDelta * 3)
      ..lineTo(MiMSizes.clipperPageInnerDelta, heightSegment * 2 + MiMSizes.clipperPageDelta * 2)
      ..lineTo(MiMSizes.clipperPageInnerDelta, heightSegment + MiMSizes.clipperPageDelta * 2)
      ..lineTo(0, heightSegment + MiMSizes.clipperPageDelta)
      ..lineTo(0, MiMSizes.clipperPageDelta)
      ..close();
  }
}
