// ignore_for_file: implementation_imports
import 'package:objectdb/objectdb.dart';
import 'package:objectdb/src/objectdb_storage_indexeddb.dart';

Future<ObjectDB> getObjectDb() async => ObjectDB(IndexedDBStorage('shmup'));
