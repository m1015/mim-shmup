import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

enum Settings {
  onScreenGamepad('onScreenGamepad'),
  portraitOrientation('portraitOrientation');

  final String value;

  const Settings(this.value);
}

class SettingsService {
  late SharedPreferences prefs;

  Map<Settings, StreamController> _controllers = {};

  Future<void> init() async => prefs = await SharedPreferences.getInstance();

  bool? getBoolParameters(Settings setting) => prefs.getBool(setting.value);

  void setBoolParameter(Settings setting, bool value) {
    prefs.setBool(setting.value, value);
    _getStreamForSetting(setting).add(value);
  }

  Stream<bool?> listenToBoolParameter(Settings setting) {
    StreamController<bool?> controller = _getStreamForSetting(setting);

    controller.add(getBoolParameters(setting));

    return controller.stream;
  }

  StreamController<bool?> _getStreamForSetting(Settings setting) {
    StreamController<bool?>? controller = _controllers[setting] as StreamController<bool?>?;

    if (controller == null) {
      controller = StreamController<bool?>.broadcast();
      _controllers[setting] = controller;
    }

    return controller;
  }
}
