// ignore_for_file: implementation_imports
import 'package:mim_shmup/core/utils/database/models/player_score.model.dart';
import 'package:objectdb/objectdb.dart';

import 'stub_database_factory.dart'
    if (dart.library.io) 'device_database_factory.dart'
    if (dart.library.js) 'web_database_factory.dart';

class LocalRepository {
  static LocalRepository? _instance;

  static Future<LocalRepository> getInstance() async {
    if (_instance == null) {
      _instance = LocalRepository._();
      await _instance!.init();
    }
    return _instance!;
  }

  late ObjectDB _db;

  LocalRepository._();

  Future<void> init() async => _db = await getObjectDb();

  Future<PlayerScore?> getHighScoreForPlayer(String player) async {
    final result = await _db.find({'user': player});

    if (result.isEmpty) return null;

    return PlayerScore.fromJson(result.first);
  }

  Future<void> saveScoreForPlayer(PlayerScore playerScore) async {
    final previous = await getHighScoreForPlayer(playerScore.player);

    if (previous == null) {
      await _db.insert(playerScore.toJson());
    } else {
      await _db.update(
        {'user': playerScore.player},
        {'score': playerScore.score, 'date': playerScore.date.toIso8601String()},
      );
    }
  }

  Future<List<PlayerScore>> getHighScores() async {
    final data = await _db.find();

    final scores = data.map((d) => PlayerScore.fromJson(d)).toList();

    scores.sort((a, b) {
      final scoreDiff = b.score - a.score;
      if (scoreDiff == 0) {
        return a.date.compareTo(b.date);
      }
      return scoreDiff;
    });

    return scores.toList();
  }
}
