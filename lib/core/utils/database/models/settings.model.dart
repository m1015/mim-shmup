class Settings {
  final bool onScreenGamepad;

  Settings({required this.onScreenGamepad});

  factory Settings.fromJson(Map<dynamic, dynamic> json) => Settings(
        onScreenGamepad: json['onScreenGamepad'],
      );

  Map<dynamic, dynamic> toJson() => {
        'onScreenGamepad': onScreenGamepad,
      };
}
