// ignore_for_file: implementation_imports
import 'package:objectdb/objectdb.dart';
import 'package:objectdb/src/objectdb_storage_filesystem.dart';
import 'package:path_provider/path_provider.dart';

Future<ObjectDB> getObjectDb() async {
  final path = '${(await getApplicationDocumentsDirectory()).path}/shmup.db';
  return ObjectDB(FileSystemStorage(path));
}
