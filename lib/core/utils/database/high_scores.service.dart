import 'package:mim_shmup/core/utils/database/local_repository.dart';
import 'package:mim_shmup/core/utils/database/models/player_score.model.dart';

class HighScoresService {
  late LocalRepository repository;

  Future<void> init() async => repository = await LocalRepository.getInstance();

  Future<PlayerScore?> loadHighScoreForPlayer(String player) => repository.getHighScoreForPlayer(player);

  Future<void> saveHighScore(String player, int score) =>
      repository.saveScoreForPlayer(PlayerScore(player: player, score: score, date: DateTime.now()));

  Future<List<PlayerScore>> loadAllHighScores() => repository.getHighScores();
}
