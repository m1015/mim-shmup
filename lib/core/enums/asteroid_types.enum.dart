import 'package:flame/game.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/core/extensions/list.extension.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

enum AsteroidType {
  big,
  medium,
  small;

  String get sprite => switch (this) {
        AsteroidType.big => [
            Assets.images.asteroids.meteorbig1.filename,
            Assets.images.asteroids.meteorbig2.filename,
            Assets.images.asteroids.meteorbig4.filename,
            Assets.images.asteroids.meteorbig4.filename,
          ].oneOff(),
        AsteroidType.medium => [
            Assets.images.asteroids.meteormed1.filename,
            Assets.images.asteroids.meteormed2.filename,
            Assets.images.asteroids.meteormed1.filename,
            Assets.images.asteroids.meteormed2.filename,
          ].oneOff(),
        AsteroidType.small => [
            Assets.images.asteroids.meteorsm1.filename,
            Assets.images.asteroids.meteorsm2.filename,
            Assets.images.asteroids.meteorsm3.filename,
            Assets.images.asteroids.meteorsm4.filename,
          ].oneOff(),
      };

  Vector2 get _textureSize => switch (this) {
        AsteroidType.big => Vector2(160, 145),
        AsteroidType.medium => Vector2(75, 60),
        AsteroidType.small => Vector2(40, 35),
      };

  double get _responsiveHeight => switch (this) {
        AsteroidType.big => 145 / GameConstants.baseScreenHeight,
        AsteroidType.medium => 60 / GameConstants.baseScreenHeight,
        AsteroidType.small => 35 / GameConstants.baseScreenHeight,
      };

  Vector2 getSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _responsiveHeight;
    final spriteWidth = spriteHeight * _textureSize.x / _textureSize.y;
    return Vector2(spriteWidth, spriteHeight);
  }

  Vector2 getHalfSize(Vector2 screenSize) => getSize(screenSize) / 2;

  double get health => switch (this) {
        AsteroidType.big => 200,
        AsteroidType.medium => 150,
        AsteroidType.small => 50,
      };

  int get score => switch (this) {
        AsteroidType.big => 100,
        AsteroidType.medium => 50,
        AsteroidType.small => 10,
      };

  AsteroidType? next() => switch (this) {
        AsteroidType.big => AsteroidType.medium,
        AsteroidType.medium => AsteroidType.small,
        AsteroidType.small => null,
      };
}
