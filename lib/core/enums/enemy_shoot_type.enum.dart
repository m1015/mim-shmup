enum EnemyShootType {
  noShoot,
  line,
  tripleShoot,
  playerFocus,
  bigShoot,
  laser,
}
