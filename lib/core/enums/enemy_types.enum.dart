import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_races.enum.dart';
import 'package:mim_shmup/core/enums/enemy_shoot_type.enum.dart';
import 'package:mim_shmup/game/sprites/boss.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

enum EnemyTypes {
  klaedBomber(
    health: 2,
    clazz: EnemyClasses.bomber,
    race: EnemyRaces.klaed,
    score: 100,
    shootType: EnemyShootType.line,
    weight: 2,
    number: 5,
  ),
  klaedFighter(
    health: 4,
    clazz: EnemyClasses.fighter,
    race: EnemyRaces.klaed,
    score: 200,
    shootType: EnemyShootType.line,
    weight: 5,
    number: 5,
  ),
  klaedFrigate(
    health: 5,
    clazz: EnemyClasses.frigate,
    race: EnemyRaces.klaed,
    score: 500,
    shootType: EnemyShootType.line,
    weight: 5,
    number: 5,
  ),
  klaedScout(
    health: 7,
    clazz: EnemyClasses.scout,
    race: EnemyRaces.klaed,
    score: 700,
    shootType: EnemyShootType.line,
    weight: 7,
    number: 5,
  ),
  klaedSupport(
    health: 10,
    clazz: EnemyClasses.support,
    race: EnemyRaces.klaed,
    score: 1000,
    shootType: EnemyShootType.line,
    weight: 7,
    number: 5,
  ),
  klaedTorpedoShip(
    health: 12,
    clazz: EnemyClasses.torpedoShip,
    race: EnemyRaces.klaed,
    score: 1200,
    shootType: EnemyShootType.line,
    weight: 10,
    number: 5,
  ),
  klaedBattleCruiser(
    health: 100,
    clazz: EnemyClasses.battlecruiser,
    race: EnemyRaces.klaed,
    score: 10000,
    shootType: EnemyShootType.laser,
    weight: 1,
    number: 1,
    isBoss: true,
  ),
  klaedDreadNought(
    health: 200,
    clazz: EnemyClasses.dreadnought,
    race: EnemyRaces.klaed,
    score: 20000,
    shootType: EnemyShootType.playerFocus,
    weight: 1,
    number: 1,
    isBoss: true,
  ),
  nautolanBomber(
    health: 14,
    clazz: EnemyClasses.bomber,
    race: EnemyRaces.nautolan,
    score: 100,
    shootType: EnemyShootType.line,
    weight: 15,
    number: 5,
  ),
  nautolanFighter(
    health: 16,
    clazz: EnemyClasses.fighter,
    race: EnemyRaces.nautolan,
    score: 200,
    shootType: EnemyShootType.line,
    weight: 20,
    number: 5,
  ),
  nautolanFrigate(
    health: 18,
    clazz: EnemyClasses.frigate,
    race: EnemyRaces.nautolan,
    score: 500,
    shootType: EnemyShootType.line,
    weight: 20,
    number: 5,
  ),
  nautolanScout(
    health: 20,
    clazz: EnemyClasses.scout,
    race: EnemyRaces.nautolan,
    score: 700,
    shootType: EnemyShootType.line,
    weight: 25,
    number: 5,
  ),
  nautolanSupport(
    health: 22,
    clazz: EnemyClasses.support,
    race: EnemyRaces.nautolan,
    score: 1000,
    shootType: EnemyShootType.line,
    weight: 35,
    number: 5,
  ),
  nautolanTorpedoShip(
    health: 24,
    clazz: EnemyClasses.torpedoShip,
    race: EnemyRaces.nautolan,
    score: 1200,
    shootType: EnemyShootType.line,
    weight: 40,
    number: 5,
  ),
  nautolanBattleCruiser(
    health: 300,
    clazz: EnemyClasses.battlecruiser,
    race: EnemyRaces.nautolan,
    score: 10000,
    shootType: EnemyShootType.playerFocus,
    weight: 1,
    number: 1,
    isBoss: true,
  ),
  nautolanDreadNought(
    health: 400,
    clazz: EnemyClasses.dreadnought,
    race: EnemyRaces.nautolan,
    score: 20000,
    shootType: EnemyShootType.playerFocus,
    weight: 1,
    number: 1,
    isBoss: true,
  ),
  nairanBomber(
    health: 26,
    clazz: EnemyClasses.bomber,
    race: EnemyRaces.nairan,
    score: 100,
    shootType: EnemyShootType.line,
    weight: 45,
    number: 5,
  ),
  nairanFighter(
    health: 28,
    clazz: EnemyClasses.fighter,
    race: EnemyRaces.nairan,
    score: 200,
    shootType: EnemyShootType.line,
    weight: 50,
    number: 5,
  ),
  nairanFrigate(
    health: 30,
    clazz: EnemyClasses.frigate,
    race: EnemyRaces.nairan,
    score: 500,
    shootType: EnemyShootType.line,
    weight: 55,
    number: 5,
  ),
  nairanScout(
    health: 32,
    clazz: EnemyClasses.scout,
    race: EnemyRaces.nairan,
    score: 700,
    shootType: EnemyShootType.line,
    weight: 60,
    number: 5,
  ),
  nairanSupport(
    health: 34,
    clazz: EnemyClasses.support,
    race: EnemyRaces.nairan,
    score: 1000,
    shootType: EnemyShootType.line,
    weight: 65,
    number: 5,
  ),
  nairanTorpedoShip(
    health: 36,
    clazz: EnemyClasses.torpedoShip,
    race: EnemyRaces.nairan,
    score: 1200,
    shootType: EnemyShootType.line,
    weight: 70,
    number: 5,
  ),
  nairanBattleCruiser(
    health: 500,
    clazz: EnemyClasses.battlecruiser,
    race: EnemyRaces.nairan,
    score: 10000,
    shootType: EnemyShootType.playerFocus,
    weight: 1,
    number: 1,
    isBoss: true,
  ),
  nairanDreadNought(
    health: 1000,
    clazz: EnemyClasses.dreadnought,
    race: EnemyRaces.nairan,
    score: 20000,
    shootType: EnemyShootType.playerFocus,
    weight: 1,
    number: 1,
    isBoss: true,
  );

  const EnemyTypes({
    required this.clazz,
    required this.health,
    required this.race,
    required this.score,
    required this.shootType,
    required this.weight,
    required this.number,
    this.isBoss = false,
  });

  final EnemyClasses clazz;

  final double health;

  final EnemyRaces race;

  final int score;

  final EnemyShootType shootType;

  final int weight;

  final int number;

  final bool isBoss;

  double get _responsiveHeight => switch (clazz) {
        EnemyClasses.bomber ||
        EnemyClasses.fighter ||
        EnemyClasses.frigate ||
        EnemyClasses.scout ||
        EnemyClasses.support ||
        EnemyClasses.torpedoShip =>
          85 / GameConstants.baseScreenHeight,
        EnemyClasses.battlecruiser || EnemyClasses.dreadnought => 188 / GameConstants.baseScreenHeight,
      };

  Vector2 getSize(Vector2 screenSize) {
    if (shootType == EnemyShootType.laser) {
      final spriteHeight = screenSize.y;
      final spriteWidth = spriteHeight * textureSize.x / textureSize.y;
      return Vector2(spriteWidth, spriteHeight);
    }
    final spriteHeight = screenSize.y * _responsiveHeight;
    final spriteWidth = spriteHeight * textureSize.x / textureSize.y;
    return Vector2(spriteWidth, spriteHeight);
  }

  Vector2 getHalfSize(Vector2 screenSize) => getSize(screenSize) / 2;

  Vector2 get textureSize => switch (clazz) {
        EnemyClasses.bomber ||
        EnemyClasses.fighter ||
        EnemyClasses.frigate ||
        EnemyClasses.scout ||
        EnemyClasses.support ||
        EnemyClasses.torpedoShip =>
          Vector2(64, 64),
        EnemyClasses.battlecruiser || EnemyClasses.dreadnought => Vector2(128, 128),
      };

  AssetGenImage getShipSprite() => switch (clazz) {
        EnemyClasses.bomber => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.bomber,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.bomber,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.bomber,
          },
        EnemyClasses.fighter => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.fighter,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.fighter,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.fighter,
          },
        EnemyClasses.frigate => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.frigate,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.frigate,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.frigate,
          },
        EnemyClasses.scout => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.scout,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.scout,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.scout,
          },
        EnemyClasses.support => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.support,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.support,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.support,
          },
        EnemyClasses.torpedoShip => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.torpedoShip,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.torpedoShip,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.torpedoShip,
          },
        EnemyClasses.battlecruiser => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.battlecruiser,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.battlecruiser,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.battlecruiser,
          },
        EnemyClasses.dreadnought => switch (race) {
            EnemyRaces.klaed => Assets.images.sprites.enemies.klaed.ships.dreadnought,
            EnemyRaces.nautolan => Assets.images.sprites.enemies.nautolan.ships.dreadnought,
            EnemyRaces.nairan => Assets.images.sprites.enemies.nairan.ships.dreadnought,
          },
      };

  (AssetGenImage, int) getFiringShipSprite() => switch (clazz) {
        EnemyClasses.bomber => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.ships.bomber, 1),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.ships.bomber, 1),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.ships.bomber, 1),
          },
        EnemyClasses.fighter => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.weapons.fighter, 6),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.weapons.fighter, 9),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.weapons.fighter, 28),
          },
        EnemyClasses.frigate => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.weapons.frigate, 6),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.weapons.frigate, 9),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.weapons.frigate, 5),
          },
        EnemyClasses.scout => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.weapons.scout, 6),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.weapons.scout, 7),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.weapons.scout, 6),
          },
        EnemyClasses.support => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.ships.support, 1),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.ships.support, 1),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.ships.support, 1),
          },
        EnemyClasses.torpedoShip => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.weapons.torpedoShip, 16),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.weapons.torpedoShip, 16),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.weapons.torpedoShip, 12),
          },
        EnemyClasses.battlecruiser => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.weapons.battlecruiser, 30),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.weapons.battlecruiser, 9),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.weapons.battlecruiser, 9),
          },
        EnemyClasses.dreadnought => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.weapons.dreadnought, 60),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.weapons.dreadnought, 35),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.weapons.dreadnought, 34),
          },
      };

  (AssetGenImage, int) getEngineSprite() => switch (clazz) {
        EnemyClasses.bomber => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.bomber, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.bomber, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.bomber, 8),
          },
        EnemyClasses.fighter => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.fighter, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.fighter, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.fighter, 8),
          },
        EnemyClasses.frigate => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.frigate, 12),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.frigate, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.frigate, 8),
          },
        EnemyClasses.scout => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.scout, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.scout, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.scout, 8),
          },
        EnemyClasses.support => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.support, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.support, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.support, 8),
          },
        EnemyClasses.torpedoShip => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.torpedoShip, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.torpedoShip, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.torpedoShip, 8),
          },
        EnemyClasses.battlecruiser => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.battlecruiser, 12),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.battlecruiser, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.battlecruiser, 8),
          },
        EnemyClasses.dreadnought => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.engines.dreadnought, 12),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.engines.dreadnought, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.engines.dreadnought, 8),
          },
      };

  (AssetGenImage, int) getExplosionSprite() => switch (clazz) {
        EnemyClasses.bomber => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.bomber, 8),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.bomber, 10),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.bomber, 16),
          },
        EnemyClasses.fighter => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.fighter, 8),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.fighter, 9),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.fighter, 18),
          },
        EnemyClasses.frigate => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.frigate, 8),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.frigate, 9),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.frigate, 16),
          },
        EnemyClasses.scout => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.scout, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.scout, 9),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.scout, 16),
          },
        EnemyClasses.support => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.support, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.support, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.support, 16),
          },
        EnemyClasses.torpedoShip => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.torpedoShip, 10),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.torpedoShip, 8),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.torpedoShip, 16),
          },
        EnemyClasses.battlecruiser => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.battlecruiser, 14),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.battlecruiser, 13),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.battlecruiser, 18),
          },
        EnemyClasses.dreadnought => switch (race) {
            EnemyRaces.klaed => (Assets.images.sprites.enemies.klaed.destructions.dreadnought, 12),
            EnemyRaces.nautolan => (Assets.images.sprites.enemies.nautolan.destructions.dreadnought, 12),
            EnemyRaces.nairan => (Assets.images.sprites.enemies.nairan.destructions.dreadnought, 18),
          },
      };

  EnemySprite getEnemySprite() => switch (clazz) {
        EnemyClasses.battlecruiser || EnemyClasses.dreadnought => BossSprite(
            key: ComponentKey.named(GameConstants.boss),
            type: this,
            shootType: shootType,
          ),
        _ => EnemySprite(
            type: this,
          ),
      };
}

enum EnemyClasses {
  bomber,
  fighter,
  frigate,
  scout,
  support,
  torpedoShip,
  battlecruiser,
  dreadnought,
}
