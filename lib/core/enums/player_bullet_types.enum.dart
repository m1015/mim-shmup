import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

enum PlayerBulletTypesEnum {
  main,
  satellite;

  AssetGenImage get sprite => switch (this) {
        PlayerBulletTypesEnum.main => Assets.images.sprites.bullets.mainBullet,
        PlayerBulletTypesEnum.satellite => Assets.images.sprites.bullets.satelliteBullet,
      };

  int get columns => switch (this) {
        PlayerBulletTypesEnum.main => 4,
        PlayerBulletTypesEnum.satellite => 3,
      };

  Vector2 getSize(Vector2 screenSize) => switch (this) {
        PlayerBulletTypesEnum.main => MiMSizes.getMainBulletSize(screenSize),
        PlayerBulletTypesEnum.satellite => MiMSizes.getSatelliteBulletSize(screenSize),
      };
}
