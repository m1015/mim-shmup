import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

enum PlayerUpgradeTypes {
  power(GameConstants.maxFirePowerLevel, 3),
  multiFire(GameConstants.maxMultiFireLevel, 3),
  satellite(GameConstants.maxSatelliteLevel, 3);

  const PlayerUpgradeTypes(this.maxLevel, this.maxExp);

  final int maxLevel;
  final int maxExp;

  bool get isLeft => switch (this) {
        PlayerUpgradeTypes.power => true,
        PlayerUpgradeTypes.multiFire => false,
        PlayerUpgradeTypes.satellite => true,
      };

  String get hudAsset => switch (this) {
        PlayerUpgradeTypes.power => Assets.images.hud.hudMainWeapon.filename,
        PlayerUpgradeTypes.multiFire => Assets.images.hud.hudMultifire.filename,
        PlayerUpgradeTypes.satellite => Assets.images.hud.hudSatellite.filename,
      };

  int currentLevel(GameSessionController session) => switch (this) {
        PlayerUpgradeTypes.power => session.currentFirePowerLevel,
        PlayerUpgradeTypes.multiFire => session.currentMultiFireLevel,
        PlayerUpgradeTypes.satellite => session.currentSatelliteLevel,
      };
}
