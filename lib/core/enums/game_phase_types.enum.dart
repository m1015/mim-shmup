enum GamePhaseTypes {
  enemies,
  asteroids,
  boss,
  rewards,
  endGame;
}
