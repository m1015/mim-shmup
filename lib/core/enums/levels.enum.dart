import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

enum Levels {
  one(1, [EnemyTypes.klaedBomber, EnemyTypes.klaedFighter, EnemyTypes.klaedFrigate]),
  two(2, [EnemyTypes.klaedScout, EnemyTypes.klaedSupport, EnemyTypes.klaedTorpedoShip]),
  three(3, [EnemyTypes.nautolanBomber, EnemyTypes.nautolanFighter, EnemyTypes.nautolanFrigate]),
  four(4, [EnemyTypes.nautolanScout, EnemyTypes.nautolanSupport, EnemyTypes.nautolanTorpedoShip]),
  five(5, [EnemyTypes.nairanBomber, EnemyTypes.nairanFighter, EnemyTypes.nairanFrigate]),
  six(6, [EnemyTypes.nairanScout, EnemyTypes.nairanSupport, EnemyTypes.nairanTorpedoShip]);

  const Levels(this.number, this.enemyTypes);

  final int number;

  final List<EnemyTypes> enemyTypes;

  static Levels fromNumber(int number) => Levels.values.firstWhere(
        (level) => level.number == number,
        orElse: () => Levels.six,
      );

  static Levels fromPhaseNumber(int phaseNumber) => Levels.values.firstWhere(
        (level) => level.number == (phaseNumber / 4).ceil(),
        orElse: () => Levels.six,
      );

  AssetGenImage get farback => switch (this) {
        Levels.one => Assets.images.backgrounds.level1.farback,
        Levels.two => Assets.images.backgrounds.level2.farback,
        Levels.three => Assets.images.backgrounds.level3.farback,
        Levels.four => Assets.images.backgrounds.level4.farback,
        Levels.five => Assets.images.backgrounds.level5.farback,
        Levels.six => Assets.images.backgrounds.level6.farback,
      };

  List<AssetGenImage> get planets => switch (this) {
        Levels.one || Levels.two => [
            Assets.images.backgrounds.planets.planet2,
            Assets.images.backgrounds.planets.planet3,
            Assets.images.backgrounds.planets.planet15,
            Assets.images.backgrounds.planets.planet16,
            Assets.images.backgrounds.planets.planet17,
            Assets.images.backgrounds.planets.planet18,
          ],
        Levels.three || Levels.four => [
            Assets.images.backgrounds.planets.planet1,
            Assets.images.backgrounds.planets.planet7,
            Assets.images.backgrounds.planets.planet10,
            Assets.images.backgrounds.planets.planet11,
            Assets.images.backgrounds.planets.planet19,
            Assets.images.backgrounds.planets.planet20,
          ],
        Levels.five || Levels.six => [
            Assets.images.backgrounds.planets.planet4,
            Assets.images.backgrounds.planets.planet5,
            Assets.images.backgrounds.planets.planet6,
            Assets.images.backgrounds.planets.planet12,
            Assets.images.backgrounds.planets.planet13,
            Assets.images.backgrounds.planets.planet14,
          ],
      };
}
