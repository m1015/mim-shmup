import 'package:mim_shmup/core/enums/bullet_types.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

enum SatelliteTypesEnum {
  bottom,
  top;

  String get sprite => Assets.images.sprites.player.satellite.filename;

  BulletTypesEnum get bulletType => BulletTypesEnum.satellite;
}
