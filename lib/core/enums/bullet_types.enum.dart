import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';

enum BulletTypesEnum {
  bullet,
  bigBullet,
  satellite,
  laser;

  Vector2 getSize(Vector2 screenSize) => switch (this) {
        BulletTypesEnum.bullet => MiMSizes.getBulletSize(screenSize),
        BulletTypesEnum.bigBullet => MiMSizes.getBigBulletSize(screenSize),
        BulletTypesEnum.satellite => MiMSizes.getSatelliteBulletSize(screenSize),
        BulletTypesEnum.laser => Vector2(screenSize.x, 70),
      };

  bool get fromSatellite => this == BulletTypesEnum.satellite;
}
