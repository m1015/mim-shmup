import 'dart:math';

import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/eight.pattern.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/patterns/line.pattern.dart';
import 'package:mim_shmup/game/patterns/loop.pattern.dart';
import 'package:mim_shmup/game/patterns/wave.pattern.dart';

enum EnemyPatternTypes {
  loop,
  wave,
  line,
  eight;

  EnemyPattern getPatternFromType(MiMGame game, EnemyTypes type) => switch (this) {
        EnemyPatternTypes.loop => LoopPattern(
            game: game,
            type: type,
            reversed: Random().nextBool(),
          ),
        EnemyPatternTypes.wave => WavePattern(
            game: game,
            type: type,
            amplitude: 100,
            frequency: 8,
          ),
        EnemyPatternTypes.line => LinePattern(
            game: game,
            type: type,
          ),
        EnemyPatternTypes.eight => EightPattern(
            game: game,
            type: type,
          ),
      };
}
