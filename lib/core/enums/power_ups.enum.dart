import 'dart:ui';

import 'package:flame/game.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/power_ups_types.enum.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

enum PowerUps {
  life(PowerUpsTypes.inGame),
  shield(PowerUpsTypes.inGame),
  missile(PowerUpsTypes.inGame),
  nuke(PowerUpsTypes.inGame),
  power(PowerUpsTypes.bonusPhase),
  multiFire(PowerUpsTypes.bonusPhase),
  satellite(PowerUpsTypes.bonusPhase);

  final PowerUpsTypes type;

  const PowerUps(this.type);

  String get asset => 'assets/images/$sprite';

  bool get animated => switch (this) {
        PowerUps.life || PowerUps.shield || PowerUps.missile => true,
        _ => false,
      };

  (AssetGenImage, int) get sprite => switch (this) {
        PowerUps.life => (Assets.images.powerUps.healUp, 10),
        PowerUps.shield => (Assets.images.sprites.player.shipShield, 10),
        PowerUps.missile => (Assets.images.powerUps.powerupMissile, 15),
        PowerUps.nuke => (Assets.images.powerUps.powerupNuke, 1),
        PowerUps.power => (Assets.images.powerUps.powerupPower, 1),
        PowerUps.multiFire => (Assets.images.powerUps.powerupMultiFire, 1),
        PowerUps.satellite => (Assets.images.powerUps.powerupSatelite, 1),
      };

  Vector2 getSize(Vector2 screenSize) => switch (this) {
        PowerUps.life => MiMSizes.getHealUpSize(screenSize),
        PowerUps.shield => MiMSizes.getPlayerSize(screenSize),
        PowerUps.missile => MiMSizes.getMissilePowerUpSize(screenSize),
        PowerUps.nuke || PowerUps.power || PowerUps.multiFire || PowerUps.satellite => MiMSizes.getPowerUpSize(screenSize),
      };

  Vector2 get textureSize => switch (this) {
        PowerUps.life => MiMSizes.healUpTextureSize,
        PowerUps.shield => MiMSizes.playerTextureSize,
        PowerUps.missile => MiMSizes.missilePowerUpTextureSize,
        PowerUps.nuke || PowerUps.power || PowerUps.multiFire || PowerUps.satellite => MiMSizes.powerUpTextureSize,
      };

  Color get color => switch (this) {
        PowerUps.life => gameGreen,
        PowerUps.shield => halloweenOrange,
        PowerUps.missile => gameRed,
        PowerUps.nuke => gameGreen,
        PowerUps.power => gameYellow,
        PowerUps.multiFire => gameYellow,
        PowerUps.satellite => gameBlue,
      };

  int get weight => switch (this) {
        PowerUps.life => 10,
        PowerUps.shield => 6,
        PowerUps.missile => 10,
        PowerUps.nuke => 1,
        PowerUps.power => 3,
        PowerUps.multiFire => 3,
        PowerUps.satellite => 3,
      };
}
