abstract class GameConstants {
  /// General about game
  static String get gameName => 'SHMUP 2024';
  static const int maxSpeedLevel = 4;
  static double get speedMultiplierPerLevel => 0.5;
  static const int maxFirePowerLevel = 5;
  static const int maxMultiFireLevel = 2;
  static double get maxMissileCoolDown => 0.5;
  static const int maxSatelliteLevel = 3;
  static int get timerPhaseAsteroid => 15;
  static int get minBonusPointsDistance => 100;
  static int get bonusPointsNumber => 4;
  static int get minMineDistance => 200;

  /// Reference screen size for responsive display : adjust as wanted
  static const double baseScreenWidth = 1264;
  static const double baseScreenHeight = 681;

  /// Name to identify game elements
  static String get gameView => 'gameView';
  static String get player => 'player';
  static String get background => 'background';
  static String get debug => 'debug';
  static String get enemyGenerator => 'enemyGenerator';
  static String get bossGenerator => 'bossGenerator';
  static String get powerUpGenerator => 'powerUpGenerator';
  static String get missileGenerator => 'missileGenerator';
  static String get particleGenerator => 'particleGenerator';
  static String get asteroidGenerator => 'asteroidGenerator';
  static String get mineGenerator => 'mineGenerator';
  static String get planetGenerator => 'planetGenerator';
  static String get satelliteBottom => 'satelliteBottom';
  static String get satelliteTop => 'satelliteTop';
  static String get eventController => 'eventController';
  static String get joystick => 'joystick';
  static String get hudScore => 'hudScore';
  static String get boss => 'boss';

  /// Session elements
  static String get multiFire => 'multiFire';
  static String get missileController => 'missileController';
  static String get nuke => 'nuke';
  static String get speed => 'speed';
  static String get phaseController => 'phaseController';
  static String get levelController => 'levelController';
  static String get asteroidCollisionsController => 'asteroidCollisionsController';
  static String get gameSession => 'gameSession';

  /// About player
  static const int baseLifePoint = 3;
  static const int baseFirePowerLevel = 1;
  static const int baseMultiFireLevel = 0;
  static const double baseMissileCoolDown = 2;
  static const int baseSatelliteLevel = 0;
  static const int baseMissileFirePower = 0;

  /// About planets
  static double get basePlanetSpeed => 35;
  static double get timerSpawnPlanet => 10;

  /// Speeds of elements
  static double get basePlayerSpeed => 280;
  static double get baseAsteroidSpeed => 50.0;
  static double get variableAsteroidSpeed => 50.0;
  static double get bulletsSpeed => 500;
  static double get missilesSpeed => 600;
  static double get powerUpSpeed => 50;
  static double get bonusPointsSpeed => 50;
  static double get mineSpeed => 50;

  /// rewards values
  static int get allEnemiesKilledOnLevel => 10000;
  static int get notTouchedOnLevel => 10000;
}
