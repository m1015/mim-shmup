import 'dart:ui';

/// colors
const halloweenOrange = Color(0xFFEC7338);
const white = Color(0xFFffffff);
const black = Color(0xFF000000);
const dimmedBlack = Color(0x88000000);
const rangoonGreen = Color(0xFF1B1B1B);
const carbonGrey = Color(0xFF5C5C5C);
const turkishRed = Color(0xFFC8102E);

const gameRed = Color(0xffac3939);
const gameGreen = Color(0xff71c937);
const gameBlue = Color(0xff36bbF5);
const gameYellow = Color(0xffffcc00);
const gamePurple = Color(0xff976ec1);
const gameWhite = Color(0xffffffff);

const focusedButton = Color(0x55ffcc00);
const unFocusedButton = Color(0x00DDDDDD);

/// paints
final knobPaint = Paint()..color = halloweenOrange.withOpacity(0.8);
final backgroundPaint = Paint()..color = white.withOpacity(0.1);
