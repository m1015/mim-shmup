import 'package:flame/components.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/laser_boss_missile.enum.dart';
import 'package:mim_shmup/core/enums/satellite_types.enum.dart';

class MiMSizes {
  static final double zero = 0.sp;

  static final double xxxSmall = 2.sp;
  static final double xxSmall = 4.sp;
  static final double xSmall = 8.sp;
  static final double small = 12.sp;
  static final double medium = 16.sp;
  static final double regular = 24.sp;
  static final double large = 32.sp;
  static final double xLarge = 48.sp;
  static final double xxLarge = 56.sp;
  static final double xxxLarge = 64.sp;

  static final double clipperButtonDelta = 10.sp;
  static final double clipperPageDelta = 25.sp;
  static final double clipperPageInnerDelta = 15.sp;
  static final double topComponentDeltaRatio = 0.2.sp;

  static final double minPlanetSize = 400.sp;
  static final double maxPlanetSize = 600.sp;

  /// Sprites height in screen percentage
  static const double _playerHeight = 75 / GameConstants.baseScreenHeight;
  static const double _healUpHeight = 50 / GameConstants.baseScreenHeight;
  static const double _bulletHeight = 40 / GameConstants.baseScreenHeight;
  static const double _bigBulletHeight = 80 / GameConstants.baseScreenHeight;
  static const double _powerUpHeight = 33 / GameConstants.baseScreenHeight;
  static const double _satelliteHeight = 20 / GameConstants.baseScreenHeight;
  static const double _engineHeight = 75 / GameConstants.baseScreenHeight;
  static const double _missilePowerUpHeight = 32 / GameConstants.baseScreenHeight;
  static const double _bonusPointHeight = 48 / GameConstants.baseScreenHeight;
  static const double _mineIdleHeight = 42 / GameConstants.baseScreenHeight;
  static const double _mineExplodingHeight = 112 / GameConstants.baseScreenHeight;

  /// Sprites texture sizes
  static Vector2 playerTextureSize = Vector2(48, 48);
  static Vector2 playerHitBoxSize = Vector2(26, 30);
  static Vector2 lifeBarTextureSize = Vector2(100, 300);
  static Vector2 healUpTextureSize = Vector2(32, 32);
  static Vector2 bulletTextureSize = Vector2(13, 37);
  static Vector2 powerUpTextureSize = Vector2(33, 33);
  static Vector2 satelliteTextureSize = Vector2(24, 24);
  static Vector2 satelliteBulletTextureSize = Vector2(7, 20);
  static Vector2 mainBulletTextureSize = Vector2(12, 17);
  static Vector2 mainBulletFromCenterPosition = Vector2(14, 9);
  static Vector2 backgroundTextureSize = Vector2(600, 512);
  static Vector2 starsTextureSize = Vector2(576, 380);
  static Vector2 engineTextureSize = Vector2(24, 24);
  static Vector2 missileTextureSize = Vector2(45, 20);
  static Vector2 missilePowerUpTextureSize = Vector2(32, 32);
  static Vector2 upgradeLevelTextureSize = Vector2(20, 30);
  static Vector2 topBossMissileSize = Vector2(55, 35);
  static Vector2 middleBossMissileSize = Vector2(20, 65);
  static Vector2 bottomBossMissileSize = Vector2(55, 95);
  static Vector2 middleTopBossMissileSize = Vector2(25, 45);
  static Vector2 middleBottomBossMissileSize = Vector2(25, 85);
  static Vector2 bonusPointTextureSize = Vector2(128, 128);
  static Vector2 mineIdleTextureSize = Vector2(96, 64);
  static Vector2 mineExplodingTextureSize = Vector2(172, 172);
  static Vector2 klaedLaserTextureSize = Vector2(38, 18);
  static Vector2 laserSatelliteTextureSize = Vector2(32, 64);
  static Vector2 laserRedTextureSize = Vector2(32, 64);

  /// Virtual Joystick
  static double joystickBackgroundRadius = 100;
  static double joystickKnobRadius = 30;
  static double joystickPadding = 10;

  /// HUD related sizes
  static Vector2 getUpgradeLevelSize(Vector2 size) {
    final spriteHeight = size.y * 0.45;
    final spriteWidth = spriteHeight * upgradeLevelTextureSize.x / upgradeLevelTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  /// Player related sizes
  static Vector2 getPlayerSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _playerHeight;
    final spriteWidth = spriteHeight * playerTextureSize.x / playerTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getEngineSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _engineHeight;
    final spriteWidth = spriteHeight * engineTextureSize.x / engineTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getPlayerHitBoxSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _playerHeight * playerHitBoxSize.y / playerTextureSize.y;
    final spriteWidth = spriteHeight * playerHitBoxSize.x / playerHitBoxSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getLifeBarSize(Vector2 screenSize) {
    final spriteHeight = getPlayerHitBoxSize(screenSize).y;
    final spriteWidth = spriteHeight * lifeBarTextureSize.x / lifeBarTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getMainBulletSize(Vector2 screenSize) {
    final playerSize = getPlayerSize(screenSize) * 2 / 3;
    final bulletWidth = playerSize.x * mainBulletTextureSize.x / playerTextureSize.x;
    final bulletHeight = bulletWidth * mainBulletTextureSize.y / mainBulletTextureSize.x;

    return Vector2(bulletWidth, bulletHeight);
  }

  static Vector2 getMissileSize(Vector2 screenSize) {
    final playerSize = getPlayerSize(screenSize) * 2 / 3;
    final bulletWidth = playerSize.x * missileTextureSize.x / playerTextureSize.x;
    final bulletHeight = bulletWidth * missileTextureSize.y / missileTextureSize.x;

    return Vector2(bulletWidth, bulletHeight);
  }

  static Vector2 getMainBulletPosition(Vector2 screenSize, int level) {
    final playerSize = getPlayerSize(screenSize);
    switch (level) {
      case 0:
        return mainBulletFromCenterPosition * playerSize.x / playerTextureSize.x;
      case 1:
        return Vector2(playerSize.x / 2, 0);
      default:
        final Vector2 basePosition = mainBulletFromCenterPosition * playerSize.x / playerTextureSize.x;
        final middleBulletPosition = Vector2(playerSize.x / 2, 0);
        return Vector2(basePosition.x - (middleBulletPosition.x - basePosition.x), basePosition.y * 2);
    }
  }

  /// Bullet related sizes
  static Vector2 getBulletSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _bulletHeight;
    final spriteWidth = spriteHeight * bulletTextureSize.x / bulletTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getBigBulletSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _bigBulletHeight;
    final spriteWidth = spriteHeight * bulletTextureSize.x / bulletTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  /// Pick and power ups related sizes
  static Vector2 getPowerUpSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _powerUpHeight;
    final spriteWidth = spriteHeight * powerUpTextureSize.x / powerUpTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getMissilePowerUpSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _missilePowerUpHeight;
    final spriteWidth = spriteHeight * missilePowerUpTextureSize.x / missilePowerUpTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getHealUpSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _healUpHeight;
    final spriteWidth = spriteHeight * healUpTextureSize.x / healUpTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  /// Satellite related sizes
  static Vector2 getSatelliteSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _satelliteHeight;
    final spriteWidth = spriteHeight * satelliteTextureSize.x / satelliteTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getSatelliteBulletSize(Vector2 screenSize) {
    final satelliteSize = getSatelliteSize(screenSize) / 2;
    final spriteHeight = satelliteSize.x * satelliteBulletTextureSize.y / satelliteBulletTextureSize.x;

    return Vector2(satelliteSize.x, spriteHeight);
  }

  static Vector2 getSatellitePosition(Vector2 screenSize, SatelliteTypesEnum type) {
    Vector2 satelliteSize = getSatelliteSize(screenSize);
    return switch (type) {
      SatelliteTypesEnum.bottom => Vector2(satelliteSize.x, MiMSizes.getPlayerSize(screenSize).y),
      SatelliteTypesEnum.top => Vector2(satelliteSize.x, 0),
    };
  }

  static Vector2 getLaserBossPosition(Vector2 laserBossSize, Vector2 laserBossTextureSize, LaserBossMissileEnum type) {
    switch (type) {
      case LaserBossMissileEnum.top:
        return topBossMissileSize * laserBossSize.x / laserBossTextureSize.x;
      case LaserBossMissileEnum.middle:
        return middleBossMissileSize * laserBossSize.x / laserBossTextureSize.x;
      case LaserBossMissileEnum.bottom:
        return bottomBossMissileSize * laserBossSize.x / laserBossTextureSize.x;
      case LaserBossMissileEnum.bottomMiddle:
        return middleBottomBossMissileSize * laserBossSize.x / laserBossTextureSize.x;
      case LaserBossMissileEnum.topMiddle:
        return middleTopBossMissileSize * laserBossSize.x / laserBossTextureSize.x;
    }
  }

  static Vector2 getBackgroundSize(Vector2 screenSize) {
    double spriteHeight = screenSize.y;
    double spriteWidth = spriteHeight * backgroundTextureSize.x / backgroundTextureSize.y;
    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getBonusPointTextureSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _bonusPointHeight;
    final spriteWidth = spriteHeight * bonusPointTextureSize.x / bonusPointTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getMineIdleTextureSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _mineIdleHeight;
    final spriteWidth = spriteHeight * mineIdleTextureSize.x / mineIdleTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }

  static Vector2 getMineExplodingTextureSize(Vector2 screenSize) {
    final spriteHeight = screenSize.y * _mineExplodingHeight;
    final spriteWidth = spriteHeight * mineExplodingTextureSize.x / mineExplodingTextureSize.y;

    return Vector2(spriteWidth, spriteHeight);
  }
}
