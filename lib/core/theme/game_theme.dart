import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/gen/fonts.gen.dart';

abstract class GameTheme {
  static ThemeData gameTheme = ThemeData(
    useMaterial3: true,
    brightness: Brightness.dark,
    colorScheme: GameTheme._colorScheme,
    textTheme: GameTheme._textTheme.apply(fontSizeFactor: 0.75.sp),
    fontFamily: FontFamily.valorax,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.only(
          bottom: 2,
          left: 10,
          right: 12,
          top: 2,
        ),
        shape: const ContinuousRectangleBorder(),
      ),
    ),
  );

  static const ColorScheme _colorScheme = ColorScheme.dark(
    brightness: Brightness.dark,
    primary: halloweenOrange,
    onPrimary: white,
    background: black,
    onBackground: white,
    surface: rangoonGreen,
    onSurface: white,
    surfaceContainerHighest: carbonGrey,
    onSurfaceVariant: white,
  );

  static const TextTheme _textTheme = TextTheme(
    displayLarge: TextStyle(
      fontSize: 75,
    ),
    displayMedium: TextStyle(
      fontSize: 55,
    ),
    displaySmall: TextStyle(
      fontSize: 45,
    ),
    headlineLarge: TextStyle(
      fontSize: 32,
    ),
    headlineMedium: TextStyle(
      fontSize: 28,
    ),
    headlineSmall: TextStyle(
      fontSize: 26,
    ),
    titleLarge: TextStyle(
      fontSize: 22,
    ),
    titleMedium: TextStyle(
      fontSize: 16,
    ),
    titleSmall: TextStyle(
      fontSize: 14,
    ),
    bodyLarge: TextStyle(
      fontSize: 16,
    ),
    bodyMedium: TextStyle(
      fontSize: 14,
    ),
    bodySmall: TextStyle(
      fontSize: 12,
    ),
    labelLarge: TextStyle(
      fontSize: 14,
    ),
    labelMedium: TextStyle(
      fontSize: 12,
    ),
    labelSmall: TextStyle(
      fontSize: 11,
    ),
  );
}
