import 'package:mim_shmup/gen/assets.gen.dart';

extension AssetGenImageExt on AssetGenImage {
  String get filename => basename(path);

  basename(String path) => path.split('assets/images/').last;
}
