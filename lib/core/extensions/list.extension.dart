import 'dart:math';

extension ListExt<T> on List<T> {
  T oneOff() => this[Random().nextInt(length)];
}
