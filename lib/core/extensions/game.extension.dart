import 'dart:ui';

import 'package:flame/game.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

extension GmaeExtension on Game {
  Image imageFromCache(AssetGenImage image) => images.fromCache(image.filename);
}
