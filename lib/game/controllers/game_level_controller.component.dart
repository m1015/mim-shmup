import 'dart:math';

import 'package:flame/components.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_races.enum.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/enums/levels.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/components/backgrounds/background.component.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';

class GameLevelController extends Component with HasGameRef<MiMGame> {
  late ComponentsNotifier<GamePhaseController> _phaseControllerNotifier;

  Levels _currentLevel = Levels.fromPhaseNumber(dotenv.getInt('START_PHASE', fallback: 1));
  int _totalLevelEnemies = 0;
  int _destroyedLevelEnemies = 0;
  bool _hasBeenShot = false;

  GameLevelController() : super(key: ComponentKey.named(GameConstants.levelController));

  Levels get currentLevel => _currentLevel;

  @override
  Future<void> onLoad() async {
    _phaseControllerNotifier = game.componentsNotifier<GamePhaseController>()
      ..addListener(() {
        final phaseController = _phaseControllerNotifier.single;

        final countPhase = phaseController!.countPhase;

        final currentLevel = (countPhase / 4).ceil();

        if (currentLevel != _currentLevel.number) {
          if (currentLevel <= 6) {
            _currentLevel = Levels.fromNumber(currentLevel);
            _setNewLevel();
          }
        }
      });

    return super.onLoad();
  }

  void reset() {
    _currentLevel = Levels.fromPhaseNumber(dotenv.getInt('START_PHASE', fallback: 1));
    _setNewLevel();
  }

  void addEnemyToLevel() => _totalLevelEnemies++;

  int get totalLevelEnemies => _totalLevelEnemies;

  void addDestroyedEnemy() => _destroyedLevelEnemies++;

  int get destroyedLevelEnemies => _destroyedLevelEnemies;

  void setHasBeenShot() => _hasBeenShot = true;

  bool get hasBeenShot => _hasBeenShot;

  EnemyTypes randomEnemyType() {
    List<EnemyTypes> availableEnemyTypes = currentLevel.enemyTypes;

    if (availableEnemyTypes.isEmpty) {
      availableEnemyTypes = [...EnemyTypes.values.where((type) => type.race == EnemyRaces.nairan)];
    }

    return availableEnemyTypes[Random().nextInt(availableEnemyTypes.length)];
  }

  void _setNewLevel() {
    _totalLevelEnemies = 0;
    _destroyedLevelEnemies = 0;
    _hasBeenShot = false;

    game.findByKeyName<BackgroundComponent>(GameConstants.background)?.updateBackground(_currentLevel.farback.filename);
  }
}
