import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/game/components/generators/missile_generator.component.dart';

class MissileController extends Component with HasGameRef {
  bool activated = false;

  double spawnCoolDown = GameConstants.baseMissileCoolDown;

  MissileController() : super(key: ComponentKey.named(GameConstants.missileController));

  void reset() {
    activated = false;
    spawnCoolDown = GameConstants.baseMissileCoolDown;
  }

  void addModifier(double modifier) {
    if (!activated) {
      activated = true;
    } else {
      spawnCoolDown -= modifier;
    }
    if (spawnCoolDown < GameConstants.maxMissileCoolDown) spawnCoolDown = GameConstants.maxMissileCoolDown;

    final missileGenerator = game.findByKeyName<MissileGenerator>(GameConstants.missileGenerator);
    if (missileGenerator == null) {
      game.add(
        MissileGenerator(
          key: ComponentKey.named(GameConstants.missileGenerator),
          period: spawnCoolDown,
        ),
      );
    } else {
      missileGenerator.setPeriod(spawnCoolDown);
    }
  }
}
