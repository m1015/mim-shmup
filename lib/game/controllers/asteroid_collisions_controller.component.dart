import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/game/components/session/models/asteroid_collision.model.dart';

class AsteroidCollisionsController extends TimerComponent {
  final List<AsteroidCollision> _collisions = [];

  AsteroidCollisionsController({super.period = 0.1})
      : super(key: ComponentKey.named(GameConstants.asteroidCollisionsController));

  @override
  void onTick() {
    final now = DateTime.now();
    _collisions.removeWhere((collision) => collision.creationTime.difference(now) > const Duration(milliseconds: 100));
  }

  bool addCollision(int hashCodesSum) {
    if (_collisions.where((collision) => collision.hashCodesSum == hashCodesSum).isNotEmpty) {
      return false;
    }
    _collisions.add(AsteroidCollision(DateTime.now(), hashCodesSum));
    return true;
  }
}
