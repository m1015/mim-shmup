import 'dart:async' as async;
import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';

class GameSessionController extends Component with Notifier {
  GameSessionController() : super(key: ComponentKey.named(GameConstants.gameSession)) {
    init();
  }

  /// Game session local info
  late int _scoreMultiplication;
  late int _lifePoint;
  late bool _isInvincible;
  async.Timer? _invincibilityTimer;
  late bool _shieldActivated;
  async.Timer? _shieldTimer;
  late int _score;
  late int _firePowerLevel;
  late int _multiFireLevel;
  late int _satelliteLevel;
  late int _missileFirePower;
  late bool _isDown;

  /// Getters
  int get currentLifePoint => _lifePoint;

  bool get currentInvincibleState => _isInvincible;

  bool get currentShieldState => _shieldActivated;

  bool get isPlayerDead => _lifePoint <= 0;

  int get currentScore => _score;

  int get currentFirePowerLevel => _firePowerLevel;

  int get currentScoreMultiplication => _scoreMultiplication;

  int get currentMultiFireLevel => _multiFireLevel;

  int get currentSatelliteLevel => _satelliteLevel;

  int get currentMissileFirePower => _missileFirePower;

  bool get currentMoveBoss => _isDown;

  /// fire power levels, and cancels any active timers for invincibility and shield.
  void init() {
    _lifePoint = GameConstants.baseLifePoint;
    _isInvincible = dotenv.getBool('IS_INVINCIBLE', fallback: false);
    _invincibilityTimer?.cancel();
    _shieldActivated = false;
    _shieldTimer?.cancel();
    _score = dotenv.getInt('SCORE', fallback: 0);
    _scoreMultiplication = dotenv.getInt('SCORE_MULTIPLICATION', fallback: 1);
    _firePowerLevel = dotenv.getInt('FIRE_POWER_LEVEL', fallback: GameConstants.baseFirePowerLevel);
    _multiFireLevel = dotenv.getInt('MULTI_FIRE_LEVEL', fallback: GameConstants.baseMultiFireLevel);
    _satelliteLevel = dotenv.getInt('SATELLITE_LEVEL', fallback: GameConstants.baseSatelliteLevel);
    _missileFirePower = dotenv.getInt('MISSILE_FIRE_POWER', fallback: GameConstants.baseMissileFirePower);
    _isDown = true;
  }

  void reset() {
    init();
    notifyListeners();
  }

  /// Life points
  void instantDeath() => _lifePoint = 0;

  void _activateInvincibility() {
    _isInvincible = true;
    _invincibilityTimer = async.Timer(const Duration(seconds: 1), () {
      _isInvincible = false;
      notifyListeners();
    });
  }

  void updatePosBoss() {
    _isDown = !_isDown;
  }

  void addLifeModifier(int modifier) {
    _lifePoint += modifier;

    /// Trigger invincibility
    if (modifier < 0 && !isPlayerDead) {
      _activateInvincibility();
    }

    /// Check for max life point
    if (_lifePoint > GameConstants.baseLifePoint) _lifePoint = GameConstants.baseLifePoint;

    /// Notify all listeners
    notifyListeners();
  }

  void activateShield() {
    _shieldActivated = true;
    notifyListeners();
  }

  void cancelShield() {
    _shieldTimer?.cancel();
    _shieldTimer = async.Timer(
      const Duration(milliseconds: 50),
      () {
        _shieldActivated = false;
        _activateInvincibility();
        notifyListeners();
      },
    );
  }

  /// Score
  void addScore(int score) {
    _score += score * _scoreMultiplication;
    notifyListeners();
  }

  /// Score Multiplication
  void addMultiplication(int scoreMultiplication) {
    _scoreMultiplication += scoreMultiplication;
    notifyListeners();
  }

  /// Score Multiplication
  void resetMultiplication() {
    _scoreMultiplication = 1;
    notifyListeners();
  }

  /// Fire power
  void levelUpFirePower() {
    if (_firePowerLevel < GameConstants.maxFirePowerLevel) {
      _firePowerLevel += 1;
      notifyListeners();
    }
  }

  Color get firePowerColor => switch (_firePowerLevel) {
        2 => gamePurple,
        3 => gameGreen,
        4 => gameYellow,
        5 => gameWhite,
        _ => gameBlue,
      };

  /// Multi fire
  void levelUpMultiFire() {
    if (_multiFireLevel < GameConstants.maxMultiFireLevel) {
      _multiFireLevel += 1;
      notifyListeners();
    }
  }

  /// Satellites
  void levelUpSatellite() {
    if (_satelliteLevel < GameConstants.maxSatelliteLevel) {
      _satelliteLevel += 1;
      notifyListeners();
    }
  }
}
