import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/core/utils/database/settings.service.dart';
import 'package:mim_shmup/core/utils/di.dart';
import 'package:mim_shmup/core/utils/platform.utils.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';

class EventController extends Component with HasGameReference<MiMGame>, TapCallbacks {
  Set<AxisDirection> horizontalAxis = <AxisDirection>{};
  Set<AxisDirection> verticalAxis = <AxisDirection>{};

  final SettingsService _settingsService = getIt<SettingsService>();

  late ComponentsNotifier<GamePhaseController> _phaseControllerNotifier;

  bool _canMove = false;

  EventController() : super(key: ComponentKey.named(GameConstants.eventController)) {}

  @override
  String toString() => '$horizontalAxis $verticalAxis';

  @override
  Future<void> onLoad() async {
    _phaseControllerNotifier = game.componentsNotifier()
      ..addListener(() {
        final phaseController = _phaseControllerNotifier.single;

        if (phaseController != null) {
          _canMove = phaseController.currentPhase.type != GamePhaseTypes.rewards &&
              phaseController.currentPhase.type != GamePhaseTypes.endGame;

          if (!_canMove) _reset();
        }
      });
    return super.onLoad();
  }

  @override
  void update(double dt) {
    if (!_canMove) return;

    if (_onScreenGamepad()) {
      _reset();

      /// there is a 90 degrees anti clockwise rotation of the "game view" on mobile so we have to correct it
      switch (game.findByKeyName<JoystickComponent>(GameConstants.joystick)?.direction) {
        case JoystickDirection.up:
          horizontalAxis.add(AxisDirection.right);
          break;
        case JoystickDirection.upLeft:
          verticalAxis.add(AxisDirection.up);
          horizontalAxis.add(AxisDirection.right);
          break;
        case JoystickDirection.upRight:
          verticalAxis.add(AxisDirection.down);
          horizontalAxis.add(AxisDirection.right);
          break;
        case JoystickDirection.right:
          verticalAxis.add(AxisDirection.down);
          break;
        case JoystickDirection.down:
          horizontalAxis.add(AxisDirection.left);
          break;
        case JoystickDirection.downRight:
          verticalAxis.add(AxisDirection.down);
          horizontalAxis.add(AxisDirection.left);
          break;
        case JoystickDirection.downLeft:
          verticalAxis.add(AxisDirection.up);
          horizontalAxis.add(AxisDirection.left);
          break;
        case JoystickDirection.left:
          verticalAxis.add(AxisDirection.up);
          break;
        default:
          break;
      }
    }

    super.update(dt);
  }

  void onKeyEvent(
    GameStatusCubit gameStatusCubit,
    KeyEvent event,
    Set<LogicalKeyboardKey> keysPressed,
  ) {
    if (_onScreenGamepad() || !_canMove) return;

    if (!game.isPlaying) {
      _reset();
      game.findByKeyName<PlayerSprite>(GameConstants.player)?.setFiring(false);
      return;
    }

    /// Update player position
    _updateDirection(event);

    /// Trigger action like shooting, etc
    _triggerAction(
      event,
      onShoot: () => game
          .findByKeyName<PlayerSprite>(GameConstants.player)
          ?.setFiring(event is KeyDownEvent || event is KeyRepeatEvent),
      onPause: () {
        gameStatusCubit.pause();
        game.router.pushNamed(Routes.pause);
      },
    );
  }

  @override
  void onTapDown(TapDownEvent event) {
    if (_onScreenGamepad() && event.canvasPosition.x > game.size.x / 2) {
      game.findByKeyName<PlayerSprite>(GameConstants.player)?.setFiring(true);
    }
    super.onTapDown(event);
  }

  @override
  void onTapUp(TapUpEvent event) {
    if (_onScreenGamepad() && event.canvasPosition.x > game.size.x / 2) {
      game.findByKeyName<PlayerSprite>(GameConstants.player)?.setFiring(false);
    }
    super.onTapUp(event);
  }

  @override
  bool containsLocalPoint(Vector2 point) {
    return point.x > game.size.x / 2;
  }

  Vector2 getControllerDirection() {
    Vector2 direction = Vector2.zero();

    if (horizontalAxis.isNotEmpty) {
      final AxisDirection current = horizontalAxis.last;
      direction.x = current == AxisDirection.right ? 1 : -1;
    }
    if (verticalAxis.isNotEmpty) {
      final AxisDirection current = verticalAxis.last;
      direction.y = current == AxisDirection.down ? 1 : -1;
    }

    return direction;
  }

  void _reset() {
    horizontalAxis.clear();
    verticalAxis.clear();
  }

  void _updateDirection(KeyEvent event) {
    if (event.logicalKey == LogicalKeyboardKey.arrowLeft) {
      _updateHorizontalAxis(
        horizontal: AxisDirection.left,
        event: event,
      );
    } else if (event.logicalKey == LogicalKeyboardKey.arrowRight) {
      _updateHorizontalAxis(
        horizontal: AxisDirection.right,
        event: event,
      );
    } else if (event.logicalKey == LogicalKeyboardKey.arrowUp) {
      _updateVerticalAxis(
        vertical: AxisDirection.up,
        event: event,
      );
    } else if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
      _updateVerticalAxis(
        vertical: AxisDirection.down,
        event: event,
      );
    }
  }

  void _triggerAction(
    KeyEvent event, {
    VoidCallback? onShoot,
    VoidCallback? onPause,
  }) {
    if ([
      LogicalKeyboardKey.gameButtonA,
      LogicalKeyboardKey.gameButtonB,
      LogicalKeyboardKey.gameButtonX,
      LogicalKeyboardKey.gameButtonY,
      LogicalKeyboardKey.space
    ].contains(event.logicalKey)) {
      onShoot?.call();
    }
    if (event.logicalKey == LogicalKeyboardKey.gameButtonStart || event.logicalKey == LogicalKeyboardKey.escape) {
      onPause?.call();
    }
  }

  void _updateHorizontalAxis({
    required AxisDirection horizontal,
    required KeyEvent event,
  }) {
    if (event is KeyDownEvent) {
      horizontalAxis.remove(horizontal);
      horizontalAxis.add(horizontal);
    } else if (event is KeyUpEvent) {
      horizontalAxis.remove(horizontal);
    }
  }

  void _updateVerticalAxis({
    required AxisDirection vertical,
    required KeyEvent event,
  }) {
    if (event is KeyDownEvent) {
      verticalAxis.remove(vertical);
      verticalAxis.add(vertical);
    } else if (event is KeyUpEvent) {
      verticalAxis.remove(vertical);
    }
  }

  bool _onScreenGamepad() => _settingsService.getBoolParameters(Settings.onScreenGamepad) ?? PlatformUtils.isMobile;
}
