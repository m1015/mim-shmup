import 'dart:math';

import 'package:flame/components.dart' hide Timer;
import 'package:flame/effects.dart';
import 'package:flutter/animation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/asteroid_types.enum.dart';
import 'package:mim_shmup/core/enums/enemy_pattern_types.enum.dart';
import 'package:mim_shmup/core/enums/enemy_shoot_type.enum.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/game/components/generators/asteroid_generator.component.dart';
import 'package:mim_shmup/game/components/generators/boss_generator.component.dart';
import 'package:mim_shmup/game/components/generators/enemy_generator.component.dart';
import 'package:mim_shmup/game/components/session/factories/game_phase.factory.dart';
import 'package:mim_shmup/game/components/session/models/game_phase.model.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/boss.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/game/sprites/power_up.sprite.dart';

class GamePhaseController extends Component with HasGameReference<MiMGame>, Notifier {
  final GamePhaseFactory _gamePhaseFactory = GamePhaseFactory();
  late GamePhaseModel currentPhase;

  int _countPhase = dotenv.getInt('START_PHASE', fallback: 1);

  GamePhaseController() : super(key: ComponentKey.named(GameConstants.phaseController)) {
    currentPhase = _gamePhaseFactory.nextPhase(_countPhase);
  }

  int get countPhase => _countPhase;

  @override
  void update(double dt) {
    final player = game.findByKeyName<PlayerSprite>(GameConstants.player);

    if (player != null && game.isPlaying && currentPhase.isFinished) {
      switch (currentPhase.type) {
        case GamePhaseTypes.enemies:
          _nextPhase();
        case GamePhaseTypes.asteroids:
          final enemies = game.findByKeyName(GameConstants.gameView)!.children.whereType<EnemySprite>();

          final asteroids = game.findByKeyName(GameConstants.gameView)!.children.whereType<AsteroidSprite>();
          final asteroidsAhead = asteroids.any((asteroid) => asteroid.position.x > player.position.x);
          if (enemies.isEmpty && (asteroids.isEmpty || !asteroidsAhead)) {
            _nextPhase();
          }
        case GamePhaseTypes.boss:
          final bosses = game.findByKeyName(GameConstants.gameView)!.children.whereType<BossSprite>();

          final powerUps = game.findByKeyName(GameConstants.gameView)!.children.whereType<PowerUpSprite>();

          if (bosses.isEmpty && powerUps.isEmpty) {
            _nextPhase();
            _rewardsPhase();
          }
        case GamePhaseTypes.rewards:
          game.setBackgroundSpeed(false);
          _nextPhase();
          if (currentPhase.type == GamePhaseTypes.endGame) {
            game.router.pushNamed(Routes.endGame);
          }
        case GamePhaseTypes.endGame:
          game.router.pushNamed(Routes.gameOver);
      }
    }

    super.update(dt);
  }

  void reset() {
    _countPhase = dotenv.getInt('START_PHASE', fallback: 1);
    _gamePhaseFactory.reset();
    currentPhase = _gamePhaseFactory.nextPhase(_countPhase);

    notifyListeners();
  }

  EnemyShootType getShootType(EnemyPattern pattern) {
    List<EnemyShootType> availablePatterns = [EnemyShootType.noShoot];

    if (_countPhase > 1) {
      availablePatterns.add(pattern.type.shootType);
    }

    return availablePatterns[Random().nextInt(availablePatterns.length)];
  }

  EnemyPattern getEnemyPattern(EnemyTypes type) {
    List<EnemyPatternTypes> availablePatterns = [...EnemyPatternTypes.values];

    final patternType = availablePatterns[Random().nextInt(availablePatterns.length)];

    return patternType.getPatternFromType(game, type);
  }

  AsteroidType getAsteroidType() {
    int currentPhase = countPhase;

    double smallAsteroidProbability = max(1 - currentPhase * 0.1, 0);
    double mediumAsteroidProbability = 0.5;

    double randomValue = Random().nextDouble();
    AsteroidType asteroidType;

    if (randomValue < smallAsteroidProbability) {
      asteroidType = AsteroidType.small;
    } else if (randomValue < smallAsteroidProbability + mediumAsteroidProbability) {
      asteroidType = AsteroidType.medium;
    } else {
      asteroidType = AsteroidType.big;
    }

    return asteroidType;
  }

  int getPhaseCumulatedWeight(int countPhase) => countPhase * 50;

  double getEnemyCreationCoolDown() => 3;

  double getAsteroidCreationCoolDown() => .6;

  int getMineChance() => (10000 / countPhase).ceil();

  double getMineHealth() => countPhase * 500;

  void _nextPhase() {
    if (game.findByKeyName<PlayerSprite>(GameConstants.player) == null) return;

    _countPhase++;

    currentPhase = _gamePhaseFactory.nextPhase(_countPhase);

    game.findByKeyName<EnemyGenerator>(GameConstants.enemyGenerator)!.reset();
    game.findByKeyName<AsteroidGenerator>(GameConstants.asteroidGenerator)!.reset();
    game.findByKeyName<BossGenerator>(GameConstants.bossGenerator)!.reset();

    notifyListeners();
  }

  void _rewardsPhase() {
    PlayerSprite? player = game.findByKeyName<PlayerSprite>(GameConstants.player);

    player?.add(
      MoveEffect.to(
        game.initialPlayerPosition,
        DelayedEffectController(
          CurvedEffectController(1.5, Curves.easeOut),
          delay: 1,
        ),
        onComplete: () {
          game.setBackgroundSpeed(true);
          game.router.pushNamed(Routes.rewards);
        },
      ),
    );

    game.removeWhere((component) => component is PowerUpSprite);
  }
}
