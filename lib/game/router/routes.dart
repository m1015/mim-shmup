abstract class Routes {
  static String get start => 'start';

  static String get countDown => 'countDown';

  static String get game => 'game';

  static String get pause => 'pause';

  static String get gameOver => 'gameOver';

  static String get leaderboard => 'leaderboard';

  static String get settings => 'settings';

  static String get rewards => 'rewards';

  static String get endGame => 'endGame';
}
