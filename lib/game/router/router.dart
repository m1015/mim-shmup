import 'package:flame/game.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/presentation/pages/count_down.page.dart';
import 'package:mim_shmup/presentation/pages/end_game.page.dart';
import 'package:mim_shmup/presentation/pages/game.page.dart';
import 'package:mim_shmup/presentation/pages/game_over.page.dart';
import 'package:mim_shmup/presentation/pages/leaderboard.page.dart';
import 'package:mim_shmup/presentation/pages/rewards.page.dart';
import 'package:mim_shmup/presentation/pages/settings.page.dart';
import 'package:mim_shmup/presentation/pages/pause.page.dart';
import 'package:mim_shmup/presentation/pages/start.page.dart';

RouterComponent mimRouter(MiMGame mimGame) => RouterComponent(
      initialRoute: Routes.start,
      routes: {
        Routes.start: OverlayRoute((context, game) => StartPage(game: mimGame)),
        Routes.countDown: OverlayRoute((context, game) => CountDownPage(game: mimGame)),
        Routes.game: OverlayRoute((context, game) => const GamePage()),
        Routes.pause: OverlayRoute((context, game) => PausePage(game: mimGame)),
        Routes.gameOver: OverlayRoute((context, game) => GameOverPage(game: mimGame)),
        Routes.leaderboard: OverlayRoute((context, game) => LeaderboardPage(game: mimGame)),
        Routes.settings: OverlayRoute((context, game) => SettingsPage(game: mimGame)),
        Routes.rewards: OverlayRoute((context, game) => RewardsPage(game: mimGame)),
        Routes.endGame: OverlayRoute((context, game) => EndGamePage(game: mimGame)),
      },
    );
