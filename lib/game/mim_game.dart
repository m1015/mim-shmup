import 'dart:async';
import 'dart:developer';

import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame_bloc/flame_bloc.dart';
import 'package:flutter/material.dart' hide OverlayRoute;
import 'package:flutter/services.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/core/utils/database/settings.service.dart';
import 'package:mim_shmup/core/utils/di.dart';
import 'package:mim_shmup/core/utils/platform.utils.dart';
import 'package:mim_shmup/game/components/backgrounds/background.component.dart';
import 'package:mim_shmup/game/components/generators/asteroid_generator.component.dart';
import 'package:mim_shmup/game/components/generators/boss_generator.component.dart';
import 'package:mim_shmup/game/components/generators/enemy_generator.component.dart';
import 'package:mim_shmup/game/components/generators/mine_generator.component.dart';
import 'package:mim_shmup/game/components/generators/missile_generator.component.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/components/generators/planet_generator.component.dart';
import 'package:mim_shmup/game/components/generators/power_up_generator.component.dart';
import 'package:mim_shmup/game/components/hud/hud.component.dart';
import 'package:mim_shmup/game/components/hud/hud_score.component.dart';
import 'package:mim_shmup/game/controllers/asteroid_collisions_controller.component.dart';
import 'package:mim_shmup/game/controllers/event_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/controllers/missile_controller.component.dart';
import 'package:mim_shmup/game/game_view.component.dart';
import 'package:mim_shmup/game/router/router.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/bonus_points.sprite.dart';
import 'package:mim_shmup/game/sprites/bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/death.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/mines/mine_exploding.sprite.dart';
import 'package:mim_shmup/game/sprites/mines/mine_idle.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/game/sprites/power_up.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/cubits/game_status.state.dart';

class MiMGame extends FlameGame with KeyboardEvents, HasCollisionDetection {
  MiMGame(this._gameStateCubit, this._focusNode, this._textTheme);

  late RouterComponent router;

  final GameStatusCubit _gameStateCubit;
  final FocusNode _focusNode;
  final TextTheme? _textTheme;

  bool isPlaying = false;

  late StreamSubscription<GameStatusState> _gameStatusSubscription;

  late EnemyGenerator _enemyGenerator;
  late BossGenerator _bossGenerator;
  late PowerUpGenerator _powerUpGenerator;
  late ParticleGenerator _particleGenerator;
  late AsteroidGenerator _asteroidGenerator;
  late MineGenerator _mineGenerator;

  late MissileController _missileController;
  late GameSessionController _gameSessionController;
  late GamePhaseController _gamePhaseController;
  late GameLevelController _gameLevelController;

  late EventController _eventController;
  late GameViewComponent _gameViewComponent;

  /// Height related to hud component
  double get hudHeight => size.y * 0.13;

  double get availableHeight => _gameViewSize!.y - (portraitOrientation ? 0 : hudHeight);

  /// Access to game session from game ref
  GameSessionController? get gameSession => _gameSessionController;

  /// Access to textTheme from game ref
  TextTheme? get textTheme => _textTheme;

  Vector2? _gameViewSize;
  Vector2? _initialPlayerPosition;

  @override
  Future<void> onLoad() async {
    await Flame.device.fullScreen();

    final settings = getIt<SettingsService>();

    settings.listenToBoolParameter(Settings.portraitOrientation).listen((value) => _setOrientation(value));

    _setOrientation(settings.getBoolParameters(Settings.portraitOrientation));

    await _loadGameAssets();

    _gameStatusSubscription = _gameStateCubit.stream.listen(_gameStatusListener);

    camera.viewfinder.anchor = Anchor.topLeft;

    _gameViewComponent = GameViewComponent();

    _enemyGenerator = EnemyGenerator();
    _bossGenerator = BossGenerator();
    _powerUpGenerator = PowerUpGenerator();
    _particleGenerator = ParticleGenerator();
    _asteroidGenerator = AsteroidGenerator();
    _mineGenerator = MineGenerator();

    _missileController = MissileController();
    _gameSessionController = GameSessionController();
    _gamePhaseController = GamePhaseController();
    _gameLevelController = GameLevelController();

    _eventController = EventController();

    // debugMode = true;

    router = mimRouter(this);

    await addAll([
      router,

      _gameViewComponent,

      /// Debug
      if (debugMode)
        TextComponent(
          key: ComponentKey.named(GameConstants.debug),
          text: 'Components: 0 / Time: 0 / xxx',
          position: Vector2(10, size.y - 30),
        ),

      /// Generators
      _enemyGenerator,
      _bossGenerator,
      _powerUpGenerator,
      _particleGenerator,
      _asteroidGenerator,
      _mineGenerator,

      /// Session
      _gameSessionController,
      _missileController,
      _gamePhaseController,
      _gameLevelController,
      AsteroidCollisionsController(),

      /// Inputs
      _eventController,

      FlameBlocProvider<GameStatusCubit, GameStatusState>(
        create: () => _gameStateCubit,
        children: [
          HudComponent(size: Vector2(size.x, hudHeight)),
        ],
      ),
    ]);

    settings.listenToBoolParameter(Settings.onScreenGamepad).listen((value) => _setJoystick(value));
    _setJoystick(settings.getBoolParameters(Settings.onScreenGamepad));
  }

  void _setJoystick(bool? value) {
    if (value ?? PlatformUtils.isMobile) {
      if (findByKeyName(GameConstants.joystick) == null) {
        add(JoystickComponent(
          key: ComponentKey.named(GameConstants.joystick),
          knob: CircleComponent(radius: MiMSizes.joystickKnobRadius, paint: knobPaint),
          background: CircleComponent(radius: MiMSizes.joystickBackgroundRadius, paint: backgroundPaint),
          margin: EdgeInsets.only(left: MiMSizes.joystickPadding, bottom: MiMSizes.joystickPadding),
        ));
      }
    } else {
      removeWhere((component) => component.key == ComponentKey.named(GameConstants.joystick));
    }
  }

  void _setOrientation(bool? isPortrait) {
    if (isPortrait ?? PlatformUtils.isMobile) {
      Flame.device.setPortrait();
    } else {
      Flame.device.setLandscape();
    }
  }

  @override
  void onDispose() {
    _gameStatusSubscription.cancel();
    super.onDispose();
  }

  @override
  void update(double dt) {
    super.update(dt);

    final player = findByKeyName<PlayerSprite>(GameConstants.player);

    if (_gameSessionController.isPlayerDead) {
      if (player != null) {
        player.removeFromParent();
        findByKeyName(GameConstants.gameView)!.add(
          DeathSprite(
            position: player.position,
            callback: () => gameOver(),
          ),
        );
      }
    }

    if (debugMode) {
      final debug = findByKeyName<TextComponent>(GameConstants.debug);
      if (debug != null) {
        debug.text = 'Components: ${children.length} / Phase: ${_gamePhaseController.currentPhase.type}';
      }
    }
  }

  @override
  KeyEventResult onKeyEvent(KeyEvent event, Set<LogicalKeyboardKey> keysPressed) {
    _eventController.onKeyEvent(_gameStateCubit, event, keysPressed);

    return super.onKeyEvent(event, keysPressed);
  }

  void gameOver() {
    _gameStateCubit.end();
  }

  void setBackgroundSpeed(bool speedingUp, [bool withWarp = true]) {
    findByKeyName<BackgroundComponent>(GameConstants.background)!.toggleSpeedUp(speedingUp, withWarp);

    final planetGenerator = findByKeyName<PlanetGenerator>(GameConstants.planetGenerator)!;

    for (var planet in planetGenerator.planets) {
      planet.isSpeedingUp = speedingUp;
    }
  }

  int getEnemiesReward() =>
      GameConstants.allEnemiesKilledOnLevel *
      _gameLevelController.currentLevel.number *
      _gameLevelController.currentLevel.number;

  int getUntouchableReward() =>
      GameConstants.notTouchedOnLevel *
      _gameLevelController.currentLevel.number *
      _gameLevelController.currentLevel.number;

  void _gameStatusListener(GameStatusState state) {
    log('${state.runtimeType}');

    isPlaying = state is GameStatusStarted || state is GameStatusResumed;

    final player = findByKeyName<PlayerSprite>(GameConstants.player);

    switch (state) {
      case GameStatusInitial():
      case GameStatusStarting():
        _resetGame();
        resumeEngine();
      case GameStatusStarted():
        _focusNode.requestFocus();
        _enemyGenerator.creating = true;
        _bossGenerator.creating = true;
        _asteroidGenerator.creating = true;
        _mineGenerator.creating = true;
        resumeEngine();
      case GameStatusPaused():
        _enemyGenerator.creating = false;
        _bossGenerator.creating = false;
        _asteroidGenerator.creating = false;
        _mineGenerator.creating = false;
        router.pushReplacementNamed(Routes.pause);
        pauseEngine();
      case GameStatusResumed():
        _enemyGenerator.creating = true;
        _bossGenerator.creating = true;
        _asteroidGenerator.creating = true;
        _mineGenerator.creating = true;
        resumeEngine();
      case GameStatusGameOver():
        _enemyGenerator.creating = false;
        _bossGenerator.creating = false;
        _asteroidGenerator.creating = false;
        _mineGenerator.creating = false;
        player!.setFiring(false);
        router.pushReplacementNamed(Routes.gameOver);
      case GameStatusLeaderboard():
        router.pushReplacementNamed(Routes.leaderboard);
    }
  }

  void _resetGame() {
    PlayerSprite? player = findByKeyName<PlayerSprite>(GameConstants.player);

    final shipY = initialPlayerPosition.y;

    if (player == null) {
      player = PlayerSprite(
        position: Vector2(-100, shipY),
      );
      findByKeyName(GameConstants.gameView)!.add(player);
    } else {
      player.position = Vector2(-100, shipY);
    }

    player.add(
      MoveEffect.to(
        initialPlayerPosition,
        DelayedEffectController(
          CurvedEffectController(1.5, Curves.easeOut),
          delay: 1,
        ),
        onComplete: () {
          setBackgroundSpeed(false, false);
        },
      ),
    );

    setBackgroundSpeed(true, false);

    _gameSessionController.reset();
    _missileController.reset();
    _gamePhaseController.reset();
    _bossGenerator.reset();
    _gameLevelController.reset();
    _enemyGenerator.reset();

    findByKeyName<HudScoreComponent>(GameConstants.hudScore)?.reset();

    _gameViewComponent.removeWhere((component) => component is EnemySprite);
    _gameViewComponent.removeWhere((component) => component is BulletSprite);
    _gameViewComponent.removeWhere((component) => component is AsteroidSprite);
    _gameViewComponent.removeWhere((component) => component is MissileGenerator);
    _gameViewComponent.removeWhere((component) => component is PowerUpSprite);
    _gameViewComponent.removeWhere((component) => component is BonusPointsSprite);
    _gameViewComponent.removeWhere((component) => component is MineIdleSprite);
    _gameViewComponent.removeWhere((component) => component is MineExplodingSprite);
  }

  Future<void> _loadGameAssets() => images.loadAll([
        /// Asteroids
        ...Assets.images.asteroids.values.map((value) => value.filename),

        /// Lasers
        ...Assets.images.lasers.values.map((value) => value.filename),

        /// Power ups
        ...Assets.images.powerUps.values.map((value) => value.filename),

        /// Sprites
        ...Assets.images.sprites.values.map((value) => value.filename),

        /// Bullets
        ...Assets.images.sprites.bullets.values.map((value) => value.filename),

        /// klaed destruction
        ...Assets.images.sprites.enemies.klaed.destructions.values.map((value) => value.filename),

        /// klaed engines
        ...Assets.images.sprites.enemies.klaed.engines.values.map((value) => value.filename),

        /// klaed projectiles
        ...Assets.images.sprites.enemies.klaed.projectiles.values.map((value) => value.filename),

        /// klaed shields
        ...Assets.images.sprites.enemies.klaed.shields.values.map((value) => value.filename),

        /// klaed ships
        ...Assets.images.sprites.enemies.klaed.ships.values.map((value) => value.filename),

        /// klaed weapons
        ...Assets.images.sprites.enemies.klaed.weapons.values.map((value) => value.filename),

        /// nairan destruction
        ...Assets.images.sprites.enemies.nairan.destructions.values.map((value) => value.filename),

        /// nairan engines
        ...Assets.images.sprites.enemies.nairan.engines.values.map((value) => value.filename),

        /// nairan projectiles
        ...Assets.images.sprites.enemies.nairan.projectiles.values.map((value) => value.filename),

        /// nairan shields
        ...Assets.images.sprites.enemies.nairan.shields.values.map((value) => value.filename),

        /// nairan ships
        ...Assets.images.sprites.enemies.nairan.ships.values.map((value) => value.filename),

        /// nairan weapons
        ...Assets.images.sprites.enemies.nairan.weapons.values.map((value) => value.filename),

        /// nautolan destruction
        ...Assets.images.sprites.enemies.nautolan.destructions.values.map((value) => value.filename),

        /// nautolan engines
        ...Assets.images.sprites.enemies.nautolan.engines.values.map((value) => value.filename),

        /// nautolan projectiles
        ...Assets.images.sprites.enemies.nautolan.projectiles.values.map((value) => value.filename),

        /// nautolan shields
        ...Assets.images.sprites.enemies.nautolan.shields.values.map((value) => value.filename),

        /// nautolan ships
        ...Assets.images.sprites.enemies.nautolan.ships.values.map((value) => value.filename),

        /// nautolan weapons
        ...Assets.images.sprites.enemies.nautolan.weapons.values.map((value) => value.filename),

        /// Engines
        ...Assets.images.sprites.engines.values.map((value) => value.filename),

        /// Player
        ...Assets.images.sprites.player.values.map((value) => value.filename),

        /// Weapons
        ...Assets.images.sprites.weapons.values.map((value) => value.filename),
      ]);

  Vector2 get gameViewSize {
    _gameViewSize ??= portraitOrientation ? Vector2(size.y, size.x) : size;
    return _gameViewSize!;
  }

  Vector2 get initialPlayerPosition {
    final shipX = portraitOrientation
        ? (MiMSizes.joystickBackgroundRadius + MiMSizes.joystickPadding) * 2 +
            MiMSizes.getPlayerSize(gameViewSize).x / 2
        : gameViewSize.x / 4;
    final offset = portraitOrientation ? 0 : hudHeight;
    _initialPlayerPosition ??= Vector2(shipX, ((gameViewSize.y - offset) / 2) + offset);
    return _initialPlayerPosition!;
  }

  bool get portraitOrientation =>
      getIt<SettingsService>().getBoolParameters(Settings.portraitOrientation) ?? PlatformUtils.isMobile;
}
