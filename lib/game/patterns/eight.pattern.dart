import 'dart:math';
import 'dart:ui';

import 'package:flame/effects.dart';
import 'package:flame/extensions.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/game/effects/eight.effect.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

class EightPattern extends EnemyPattern {
  late Vector2 _position;

  EightPattern({
    required MiMGame game,
    required super.type,
    super.coolDown = 0.5,
  }) : super(game: game) {
    /// Set enemy y position between 10% and 90% of available height
    /// this way the player can always shoot them
    _position = Vector2(
      game.gameViewSize.x + type.getSize(game.gameViewSize).x,
      _getNotMovingEnemyY(type),
    );
  }

  @override
  void setEnemy(EnemySprite enemy) {
    final yOffset = game.gameViewSize.y * 0.1;
    final eightCenter = Vector2(game.gameViewSize.x * 3 / 4, game.gameViewSize.y / 2 + yOffset);
    enemy.position = _position;
    enemy.add(_goToEightCenter(_position, eightCenter));
  }

  double _getNotMovingEnemyY(EnemyTypes enemy) =>
      (game.portraitOrientation ? 0 : game.hudHeight) +
      enemy.getHalfSize(game.gameViewSize).y +
      Random().nextDouble() * (game.availableHeight - 2 * enemy.getHalfSize(game.gameViewSize).y);

  Effect _goToEightCenter(Vector2 startPosition, Vector2 eightCenter) {
    final path = Path()
      ..moveTo(startPosition.x, startPosition.y)
      ..quadraticBezierTo(
        startPosition.x,
        eightCenter.y,
        eightCenter.x,
        eightCenter.y,
      );

    final effect = SequenceEffect(
      [
        MoveAlongPathEffect(
          path,
          EffectController(duration: 3.0),
          absolute: true,
        ),
        eightEffect(game.gameViewSize),
      ],
    );
    return effect;
  }
}
