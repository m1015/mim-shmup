import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flutter/animation.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

class StaticPattern extends EnemyPattern {
  StaticPattern({
    required super.game,
    required super.type,
    super.coolDown = 1,
  });

  @override
  void setEnemy(EnemySprite enemy) {
    final screenSize = game.gameViewSize;

    final rightOffset = screenSize.x * 0.75;

    // Set the initial position outside the right side of the screen
    enemy.position = Vector2(
      screenSize.x + enemy.size.x, // Start outside the screen
      screenSize.y / 2,
    );

    // Add an animation to move the enemy from the right to the desired position
    enemy.add(
      MoveEffect.to(
        Vector2(rightOffset, screenSize.y / 2),
        EffectController(
          duration: 1.0,
          curve: Curves.easeOut,
        ),
      ),
    );

    // Animation de montée et de descente
    final verticalMoveEffect = MoveEffect.by(
      Vector2(0, screenSize.y / 4),
      EffectController(
        duration: 4,
        infinite: true,
        reverseDuration: 4,
        curve: Curves.linear,
        onMax: () => game.gameSession!.updatePosBoss(),
        onMin: () => game.gameSession!.updatePosBoss(),
      ),
    );

    verticalMoveEffect.onComplete = () {
      game.gameSession!.updatePosBoss();
    };

    enemy.add(verticalMoveEffect);
  }
}
