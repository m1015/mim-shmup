import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_shoot_type.enum.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/game_view.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/boss.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

abstract class EnemyPattern {
  final EnemyTypes type;
  final double coolDown;

  late final WeakReference<MiMGame> _game;
  late EnemyShootType _shootType;
  GameViewComponent? _gameView;

  bool _finished = false;
  double _elapsedTime = 0;
  int _generatedEnemies = 0;
  GameLevelController? _levelController;

  EnemyPattern({
    required MiMGame game,
    required this.type,
    required this.coolDown,
  })  : _game = WeakReference(game),
        _elapsedTime = coolDown {
    _shootType = game.findByKeyName<GamePhaseController>(GameConstants.phaseController)!.getShootType(this);
  }

  void update(double dt) {
    if (_game.target == null) return;

    _levelController ??= _game.target!.findByKeyName(GameConstants.levelController);
    _gameView ??= _game.target!.findByKeyName(GameConstants.gameView);

    _elapsedTime += dt;

    if (_elapsedTime > coolDown) {
      if (_generatedEnemies >= type.number) {
        markFinished();
      } else {
        _elapsedTime = 0;

        final enemySprite = type.getEnemySprite();

        if (enemySprite is! BossSprite) {
          enemySprite.shootType = _shootType;
        }

        setEnemy(enemySprite);

        _gameView!.add(enemySprite);

        _levelController!.addEnemyToLevel();

        _generatedEnemies++;
      }
    }
  }

  void setEnemy(EnemySprite enemy);

  void markFinished() => _finished = true;

  MiMGame get game => _game.target!;

  bool get finished => _finished;
}
