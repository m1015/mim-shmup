import 'dart:math';

import 'package:flame/effects.dart';
import 'package:flame/extensions.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

class LinePattern extends EnemyPattern {
  late Vector2 _position;

  LinePattern({
    required MiMGame game,
    required super.type,
    super.coolDown = 0.5,
  }) : super(game: game) {
    /// Set enemy y position between 10% and 90% of available height
    /// this way the player can always shoot them
    _position = Vector2(
      game.gameViewSize.x + type.getSize(game.gameViewSize).x,
      _getNotMovingEnemyY(type),
    );
  }

  @override
  void setEnemy(EnemySprite enemy) {
    enemy.position = _position;
    enemy.add(
      MoveEffect.to(
        Vector2(
          -100,
          _position.y,
        ),
        EffectController(duration: 10),
      ),
    );
  }

  double _getNotMovingEnemyY(EnemyTypes enemy) =>
      (game.portraitOrientation ? 0 : game.hudHeight) +
      enemy.getHalfSize(game.gameViewSize).y +
      Random().nextDouble() * (game.availableHeight - 2 * enemy.getHalfSize(game.gameViewSize).y);
}
