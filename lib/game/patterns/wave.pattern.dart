import 'dart:math';

import 'package:flame/extensions.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/game/effects/wave.effect.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

class WavePattern extends EnemyPattern {
  final double amplitude;
  final int frequency;

  late Vector2 _position;

  WavePattern({
    required MiMGame game,
    required super.type,
    super.coolDown = 0.5,
    required this.amplitude,
    required this.frequency,
  }) : super(game: game) {
    /// Set enemy y position between 10% and 90% of available height
    /// this way the player can always shoot them
    _position = Vector2(
      game.gameViewSize.x + type.getSize(game.gameViewSize).x,
      _getMovingEnemyY(type, amplitude),
    );
  }

  @override
  void setEnemy(EnemySprite enemy) => enemy.add(waveEffect(game.gameViewSize, _position, amplitude, frequency));

  double _getMovingEnemyY(EnemyTypes enemy, double amplitude) {
    double amplitudeDelta = amplitude + enemy.getHalfSize(game.gameViewSize).y;
    return (game.portraitOrientation ? 0 : game.hudHeight) +
        amplitudeDelta +
        Random().nextDouble() * (game.availableHeight - 2 * amplitudeDelta);
  }
}
