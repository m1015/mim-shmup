import 'dart:math';

import 'package:flame/extensions.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/game/effects/loop.effect.dart';
import 'package:mim_shmup/game/effects/reversed_loop.effect.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

class LoopPattern extends EnemyPattern {
  final bool reversed;

  late Vector2 _position;

  LoopPattern({
    required MiMGame game,
    required super.type,
    super.coolDown = 0.5,
    required this.reversed,
  }) : super(game: game) {
    /// Set enemy y position between 10% and 90% of available height
    /// this way the player can always shoot them
    _position = Vector2(
      game.gameViewSize.x + type.getSize(game.gameViewSize).x,
      _getNotMovingEnemyY(type),
    );
  }

  @override
  void setEnemy(EnemySprite enemy) {
    if (reversed) {
      enemy.add(reversedLoopEffect(game.gameViewSize, _position));
    } else {
      enemy.add(loopEffect(game.gameViewSize, _position));
    }
  }

  double _getNotMovingEnemyY(EnemyTypes enemy) =>
      (game.portraitOrientation ? 0 : game.hudHeight) +
      enemy.getHalfSize(game.gameViewSize).y +
      Random().nextDouble() * (game.availableHeight - 2 * enemy.getHalfSize(game.gameViewSize).y);
}
