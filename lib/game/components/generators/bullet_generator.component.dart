import 'dart:math';

import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/bullet_types.enum.dart';
import 'package:mim_shmup/core/enums/enemy_shoot_type.enum.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/enums/laser_boss_missile.enum.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/bullet.sprite.dart';

class BulletGenerator extends TimerComponent with HasGameReference<MiMGame> {
  final bool bigBulletSize;
  final EnemyShootType type;
  final EnemyTypes enemyType;

  bool _firing = false;
  late WeakReference<PositionComponent> _source;
  late Vector2 _offset1;
  late Vector2 _offset2;
  late List<double> _angles;
  late Vector2 _initialBossPosition;
  int _firingMode = 0;

  BulletGenerator({
    this.bigBulletSize = false,
    required this.type,
    required this.enemyType,
    required PositionComponent source,
    super.period = 0.2,
  }) : super(repeat: true, autoStart: true) {
    _initialBossPosition = source.position.clone();

    switch (type) {
      case EnemyShootType.line:
        _setLineFiring(source);
        break;
      case EnemyShootType.tripleShoot:
        _setTripleShootFiring(source);
        break;
      case EnemyShootType.playerFocus:
        _setPlayerFocusFiring(source);
        break;
      case EnemyShootType.bigShoot:
        _setBigShootFiring(source);
        break;
      case EnemyShootType.laser:
        _setLaserFiring(source);
        break;
      case EnemyShootType.noShoot:
        // nothing to do
        break;
    }
  }

  @override
  void onTick() {
    if (_firing && _source.target != null && isMounted) {
      _shoot();
    }
    super.onTick();
  }

  void _shoot() {
    if (type == EnemyShootType.laser) {
      double bossYOffset = (_source.target!.position.y - _initialBossPosition.y);
      if (game.gameSession != null) {
        if (game.gameSession!.currentMoveBoss) {
          bossYOffset += 20;
        } else {
          bossYOffset -= 20;
        }
      }
      switch (_firingMode) {
        case 0:
          _offset1 = MiMSizes.getLaserBossPosition(
              enemyType.getSize(game.gameViewSize), enemyType.textureSize, LaserBossMissileEnum.top);
          _offset2 = MiMSizes.getLaserBossPosition(
              enemyType.getSize(game.gameViewSize), enemyType.textureSize, LaserBossMissileEnum.bottom);

          _generateParticleEffect(Vector2(_offset1.x + (game.gameViewSize.x / 2), _offset1.y + bossYOffset));
          _generateParticleEffect(Vector2(_offset2.x + (game.gameViewSize.x / 2), _offset2.y + bossYOffset));
          _angles = [0];
          break;
        case 1:
          _offset1 = MiMSizes.getLaserBossPosition(
              enemyType.getSize(game.gameViewSize), enemyType.textureSize, LaserBossMissileEnum.topMiddle);
          _offset2 = MiMSizes.getLaserBossPosition(
              enemyType.getSize(game.gameViewSize), enemyType.textureSize, LaserBossMissileEnum.bottomMiddle);

          _generateParticleEffect(Vector2(_offset1.x + (game.gameViewSize.x / 2), _offset1.y + bossYOffset));
          _generateParticleEffect(Vector2(_offset2.x + (game.gameViewSize.x / 2), _offset2.y + bossYOffset));
          _angles = [0];
          break;
      }

      game.findByKeyName(GameConstants.gameView)!.add(
            TimerComponent(
              period: 0.5,
              repeat: false,
              onTick: () {
                _fireLaser(bossYOffset);
              },
            ),
          );
      _firingMode = (_firingMode + 1) % 2;
    } else {
      game.findByKeyName(GameConstants.gameView)!.addAll(
            _angles.map(
              (angle) => BulletSprite(
                position: _source.target!.position,
                angle: angle,
                type: bigBulletSize ? BulletTypesEnum.bigBullet : BulletTypesEnum.bullet,
              ),
            ),
          );
    }
  }

  void _fireLaser(double bossYOffset) {
    game.findByKeyName(GameConstants.gameView)!.addAll(
      _angles.map(
        (angle) => BulletSprite(
          position: Vector2(_offset1.x, _offset1.y + bossYOffset),
          angle: angle,
          type: BulletTypesEnum.laser,
        ),
      ),
    );

    game.findByKeyName(GameConstants.gameView)!.addAll(
      _angles.map(
        (angle) => BulletSprite(
          position: Vector2(_offset2.x, _offset2.y + bossYOffset),
          angle: angle,
          type: BulletTypesEnum.laser,
        ),
      ),
    );
  }

  void _generateParticleEffect(Vector2 position) {
    if (game.gameSession != null) {
      game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
            color: Colors.red,
            position: position.clone(),
            lifespan: 1.5,
            convergeToCenter: true,
            count: 300,
            rnd: 200,
            radius: 2,
          );
    }
  }

  void setFiring({
    required bool firing,
    required PositionComponent source,
    required Vector2 offset1,
    required Vector2 offset2,
    required List<double> angles,
    required BulletTypesEnum bulletTypes,
    double size = 3,
  }) {
    _firing = firing;
    _source = WeakReference(source);
    _offset1 = offset1;
    _offset2 = offset2;
    _angles = angles;
    if (isMounted) _shoot();
  }

  void _setLineFiring(PositionComponent source) => setFiring(
        firing: true,
        source: source,
        offset1: Vector2(-source.size.x / 2, 0),
        offset2: Vector2(-source.size.x / 2, 0),
        angles: [(pi * 1.5)],
        bulletTypes: BulletTypesEnum.bullet,
      );

  void _setBigShootFiring(PositionComponent source) => setFiring(
        firing: true,
        source: source,
        offset1: Vector2(-source.size.x / 2, 0),
        offset2: Vector2(-source.size.x / 2, 0),
        angles: [(pi * 1.5)],
        bulletTypes: BulletTypesEnum.bigBullet,
      );

  void _setTripleShootFiring(PositionComponent source) => setFiring(
        firing: true,
        source: source,
        offset1: Vector2(-source.size.x / 2, 0),
        offset2: Vector2(-source.size.x / 2, 0),
        angles: [
          pi * 1.5,
          (pi * 1.5) + (pi / 2) * 0.05,
          (pi * 1.5) - (pi / 2) * 0.05,
        ],
        bulletTypes: BulletTypesEnum.bullet,
      );

  void _setPlayerFocusFiring(PositionComponent source) => setFiring(
        firing: true,
        source: source,
        offset1: Vector2(-source.size.x / 2, 0),
        offset2: Vector2(-source.size.x / 2, 0),
        angles: [],
        bulletTypes: BulletTypesEnum.bullet,
      );

  // New method to handle dual laser firing
  void _setLaserFiring(PositionComponent source) {
    setFiring(
      firing: true,
      source: source,
      offset1: Vector2(-source.size.x / 2, 120),
      offset2: Vector2(-source.size.x / 2, -120),
      angles: [0],
      bulletTypes: BulletTypesEnum.laser,
    );
  }

  void updateAngle(List<double> angles) => _angles = angles;
}
