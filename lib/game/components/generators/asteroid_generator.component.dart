import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/asteroid_types.enum.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';

class AsteroidGenerator extends Component with HasGameReference<MiMGame> {
  bool creating = false;
  double _elapsedTime = 0;
  double _phaseTime = 0;
  GamePhaseController? _phaseController;
  bool _generatingAsteroid = false;

  AsteroidGenerator() : super(key: ComponentKey.named(GameConstants.asteroidGenerator));

  @override
  void update(double dt) {
    if (creating && !_generatingAsteroid) {
      _phaseController ??= game.findByKeyName<GamePhaseController>(GameConstants.phaseController)!;

      if (_phaseController!.currentPhase.type == GamePhaseTypes.asteroids &&
          _phaseTime <= GameConstants.timerPhaseAsteroid) {
        _phaseTime = _phaseTime + dt;

        _elapsedTime += dt;

        if (_elapsedTime > _phaseController!.getAsteroidCreationCoolDown()) {
          _elapsedTime = 0;
          _generateAsteroid();
        }

        if (_phaseTime > GameConstants.timerPhaseAsteroid) {
          _phaseController?.currentPhase.setFinished();
        }
      }
    }

    super.update(dt);
  }

  void reset() {
    _phaseTime = 0;
  }

  void _generateAsteroid() async {
    _generatingAsteroid = true;
    AsteroidType asteroidType = _phaseController!.getAsteroidType();

    /// Set asteroid y position between 10% and 90% of available height
    /// this way the player can always shoot them
    late Vector2 position;

    do {
      position = Vector2(
        game.gameViewSize.x + asteroidType.getSize(game.gameViewSize).x,
        _getY(asteroidType),
      );
    } while (game.children.whereType<AsteroidSprite>().any((component) => component.containsPoint(position)));

    game.findByKeyName(GameConstants.gameView)!.add(AsteroidSprite(
          type: asteroidType,
          position: position,
          direction: 8 * pi / 9 + (2 * Random().nextDouble() * pi / 9),
        ));

    _generatingAsteroid = false;
  }

  double _getY(AsteroidType type) =>
      (game.portraitOrientation ? 0 : game.hudHeight) +
      type.getHalfSize(game.gameViewSize).y +
      Random().nextDouble() * (game.availableHeight - 2 * type.getHalfSize(game.gameViewSize).y);
}
