import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/missile.sprite.dart';
import 'package:mim_shmup/game/sprites/player/rocket_weapons.sprite.dart';

class MissileGenerator extends TimerComponent with HasGameRef<MiMGame>, Notifier {
  MissileGenerator({super.key, required super.period}) : super(repeat: true, autoStart: true);

  RocketWeaponSprite? rocketWeaponsSprite;

  @override
  void onTick() {
    _shoot();
    super.onTick();
  }

  late bool _playAnimation = false;
  bool get currentAnimationState => _playAnimation;

  void setPeriod(double period) => timer.limit = period;

  bool _shootFromTop = true;
  int _shootCountTop = 0;
  int _shootCountBottom = 0;

  static const int maxShootCount = 3;
  void _shoot() {
    _playAnimation = false;
    notifyListeners();
    final player = game.findByKeyName<PositionComponent>(GameConstants.player);

    if (player == null) return;

    final target = _getTarget(player);

    if (target != null) {
      final playerSize = MiMSizes.getPlayerSize(game.gameViewSize);

      final missilePosition = _shootFromTop
          ? player.position - Vector2(0, playerSize.y / (3 + (_shootCountTop * 2)))
          : player.position + Vector2(0, playerSize.y / (4 + (_shootCountBottom * 2)));

      game.findByKeyName(GameConstants.gameView)!.add(
        MissileSprite(
          target: target,
          position: missilePosition,
        ),
      );

      if (_shootFromTop) {
        _shootCountTop++;
        if (_shootCountTop >= maxShootCount) {
          _shootCountTop = 0;
        }
      } else {
        _shootCountBottom++;
        if (_shootCountBottom >= maxShootCount) {
          _shootCountBottom = 0;
        }
      }

      _shootFromTop = !_shootFromTop;

      _playAnimation = true;
      notifyListeners();
    } else {
      _playAnimation = false;
      notifyListeners();
    }
  }

  PositionComponent? _getTarget(PositionComponent player) {
    List<PositionComponent> targets = [];
    targets.addAll(gameRef.children.query<EnemySprite>());
    targets.addAll(gameRef.children.query<AsteroidSprite>());

    if (targets.isEmpty) return null;

    var nearestEnemy = targets.first;
    var nearestDistance = nearestEnemy.position.distanceTo(player.position);

    for (var enemy in targets) {
      final distance = enemy.position.distanceTo(player.position);
      if (distance < nearestDistance) {
        nearestEnemy = enemy;
        nearestDistance = distance;
      }
    }

    return nearestEnemy;
  }
}
