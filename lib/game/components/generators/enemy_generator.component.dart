import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';

class EnemyGenerator extends Component with HasGameReference<MiMGame> {
  bool creating = false;
  double _elapsedTime = 0;
  GamePhaseController? _phaseController;
  List<EnemyPattern> _patterns = [];
  EnemyTypes? _selectedEnemy;
  double _accumulatedWeight = 0;
  GameLevelController? _gameLevelController;

  EnemyGenerator() : super(key: ComponentKey.named(GameConstants.enemyGenerator));

  @override
  void update(double dt) {
    _phaseController ??= game.findByKeyName<GamePhaseController>(GameConstants.phaseController)!;

    if (_phaseController!.currentPhase.type == GamePhaseTypes.enemies) {
      _elapsedTime += dt;

      final cooldown = _phaseController!.getEnemyCreationCoolDown();

      /// Obtenir le score total d'ennemis générés depuis GamePhase
      if (!_phaseController!.currentPhase.isFinished && creating && _elapsedTime > cooldown) {
        _elapsedTime = 0;

        _selectedEnemy = _selectNextEnemy();

        if (_selectedEnemy != null) {
          /// Ajouter le pattern à la liste des patterns
          _patterns.add(_phaseController!.getEnemyPattern(_selectedEnemy!));
        }
      }
    } else {
      _accumulatedWeight = 0;
    }

    /// Mise à jour des patterns
    for (var pattern in _patterns) {
      pattern.update(dt);
    }

    /// Suppression des patterns terminés
    _patterns.removeWhere((pattern) => pattern.finished);

    super.update(dt);
  }

  void reset() {
    _elapsedTime = 0;
    _accumulatedWeight = 0;
    _patterns = [];
    _selectedEnemy = null;
  }

  /// Méthode pour sélectionner un type d'ennemi en fonction du poids
  EnemyTypes _selectNextEnemy() {
    _gameLevelController ??= game.findByKeyName<GameLevelController>(GameConstants.levelController);

    EnemyTypes selectedEnemy = _gameLevelController!.randomEnemyType();

    _accumulatedWeight += selectedEnemy.weight;

    if (_accumulatedWeight >= _phaseController!.getPhaseCumulatedWeight(_phaseController!.countPhase)) {
      _phaseController!.currentPhase.setFinished();
    }

    return selectedEnemy;
  }
}
