import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/player_bullet_types.enum.dart';
import 'package:mim_shmup/core/enums/satellite_types.enum.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/bullets/player_bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';

const double horizontalAngle = pi / 2;

class SatelliteBulletGenerator extends TimerComponent with HasGameReference<MiMGame> {
  SatelliteBulletGenerator({
    required this.playerRef,
    required this.source,
    required this.type,
  }) : super(period: 0.5, repeat: true, autoStart: true);

  final SatelliteTypesEnum type;
  final WeakReference<PlayerSprite> playerRef;
  final WeakReference<PositionComponent> source;

  @override
  void onTick() {
    if (source.target != null) {
      _shoot();
    }
    super.onTick();
  }

  int _getSatelliteLevel() {
    GameSessionController? gameSession = game.gameSession;
    return gameSession?.currentSatelliteLevel ?? 1;
  }

  Vector2 _getFirePosition() {
    final Vector2 playerPosition = playerRef.target!.position;
    final Vector2 satellitePosition =
        MiMSizes.getSatellitePosition(game.gameViewSize, SatelliteTypesEnum.bottom);
    final Vector2 satelliteSize = MiMSizes.getSatelliteSize(game.gameViewSize);

    return switch (type) {
      SatelliteTypesEnum.bottom =>
        playerPosition + Vector2(-satelliteSize.x / 2, satellitePosition.y / 2),
      SatelliteTypesEnum.top =>
        playerPosition - Vector2(satelliteSize.x / 2, satellitePosition.y / 2),
    };
  }

  double _getAngle(int level) {
    const double baseAngle = pi / 45;
    return switch (type) {
      SatelliteTypesEnum.bottom => baseAngle * (level - 1),
      SatelliteTypesEnum.top => -baseAngle * (level - 1),
    };
  }

  void _shoot() {
    final int level = _getSatelliteLevel();
    game.findByKeyName(GameConstants.gameView)!.addAll([
      PlayerBulletSprite(
        type: PlayerBulletTypesEnum.satellite,
        position: _getFirePosition(),
        angle: horizontalAngle,
      ),
      if (level >= 2)
        PlayerBulletSprite(
          type: PlayerBulletTypesEnum.satellite,
          position: _getFirePosition(),
          angle: horizontalAngle + _getAngle(2),
        ),
      if (level >= GameConstants.maxSatelliteLevel)
        PlayerBulletSprite(
          type: PlayerBulletTypesEnum.satellite,
          position: _getFirePosition(),
          angle: horizontalAngle + _getAngle(GameConstants.maxSatelliteLevel),
        ),
    ]);
  }
}
