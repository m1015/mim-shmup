import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/core/enums/power_ups.enum.dart';
import 'package:mim_shmup/core/enums/power_ups_types.enum.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/power_up.sprite.dart';

class PowerUpGenerator extends Component with HasGameRef<MiMGame> {
  double _elapsedTime = 0;
  GamePhaseController? _phaseController;
  bool shouldGenerate = false;

  PowerUpGenerator() : super(key: ComponentKey.named(GameConstants.powerUpGenerator));

  @override
  void update(double dt) {
    _elapsedTime += dt;
    super.update(dt);
  }

  void generatePowerUp(Vector2 position, [PowerUpsTypes type = PowerUpsTypes.inGame]) {
    _phaseController ??= game.findByKeyName<GamePhaseController>(GameConstants.phaseController)!;

    if (_phaseController!.currentPhase.type == GamePhaseTypes.enemies) {
      shouldGenerate = _elapsedTime > 10 && Random().nextDouble() > 0.5;
    } else if (_phaseController!.currentPhase.type == GamePhaseTypes.asteroids) {
      shouldGenerate = _elapsedTime > 10 && Random().nextDouble() > 0.3;
    } else {
      shouldGenerate = false;
    }

    if (shouldGenerate || type == PowerUpsTypes.bonusPhase) {
      _elapsedTime = 0;

      var powerUp = getPowerUp(type);

      powerUp ??= getPowerUp(PowerUpsTypes.inGame);

      game.findByKeyName(GameConstants.gameView)!.add(PowerUpSprite(position: position, type: powerUp!));
    }
  }

  PowerUps? getPowerUp(PowerUpsTypes type) {
    final gameSession = game.gameSession!;

    final power = gameSession.currentFirePowerLevel;
    final multiFire = gameSession.currentMultiFireLevel;
    final satellite = gameSession.currentSatelliteLevel;

    /// we filter the power ups not to have one whose level have been maxed
    final availablePowerUps = _availablePowerUps(
      type: type,
      life: gameSession.currentLifePoint,
      hasShield: gameSession.currentShieldState,
      power: power,
      multiFire: multiFire,
      satellite: satellite,
      countPhase: _phaseController!.countPhase,
    );

    /// we sum all the weights of the available power ups
    final totalWeight = availablePowerUps.fold(0.0, (total, powerUp) => total + powerUp.weight.toDouble());

    /// we pick a value in the range of the sum
    final pickedValue = (Random().nextDouble() * totalWeight).round();

    int reachedValue = 0;

    PowerUps? result;

    /// we add the weights of the available power ups until we reach the picked value
    for (final powerUp in availablePowerUps) {
      reachedValue += powerUp.weight;
      if (reachedValue >= pickedValue) {
        result = powerUp;
        break;
      }
    }

    return result;
  }

  List<PowerUps> _availablePowerUps({
    required PowerUpsTypes type,
    required int life,
    required bool hasShield,
    required int power,
    required int multiFire,
    required int satellite,
    required int countPhase,
  }) {
    final availablePowerUps = [...PowerUps.values.where((powerUp) => powerUp.type == type)];

    if (life >= GameConstants.baseLifePoint) availablePowerUps.remove(PowerUps.life);
    if (hasShield) availablePowerUps.remove(PowerUps.shield);
    if (power >= GameConstants.maxFirePowerLevel) availablePowerUps.remove(PowerUps.power);
    if (multiFire >= GameConstants.maxMultiFireLevel) availablePowerUps.remove(PowerUps.multiFire);
    if (satellite >= GameConstants.maxSatelliteLevel) {
      availablePowerUps.remove(PowerUps.satellite);
    }
    if (countPhase < 5) {
      availablePowerUps.remove(PowerUps.nuke);
    }
    return availablePowerUps;
  }
}
