import 'dart:math';
import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/particles.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';

class ParticleGenerator extends Component with HasGameRef {
  final _random = Random();

  ParticleGenerator() : super(key: ComponentKey.named(GameConstants.particleGenerator));

  void generateParticle({
    required Color color,
    required Vector2 position,
    int count = 50,
    double lifespan = 0.3,
    double rnd = 500,
    double radius = 2,
    Vector2? acceleration,
    Vector2? speed,
    bool convergeToCenter = false, // New parameter to control convergence
  }) {
    if (convergeToCenter) {
      final particleComponent = ParticleSystemComponent(
        anchor: Anchor.center,
        particle: Particle.generate(
          count: count,
          lifespan: lifespan,
          generator: (_) {
            final startPosition = position + _getRandomVector(rnd);
            final direction = (position - startPosition).normalized();

            // Speed is calculated towards the center
            final adjustedSpeed = speed ?? direction * rnd;
            final adjustedAcceleration = acceleration ?? Vector2.zero();

            return AcceleratedParticle(
              acceleration: adjustedAcceleration,
              speed: adjustedSpeed,
              position: startPosition,
              child: CircleParticle(
                radius: radius,
                paint: Paint()..color = color,
              ),
            );
          },
        ),
      );
      game.findByKeyName(GameConstants.gameView)!.add(particleComponent);
    } else {
      final particleComponent = ParticleSystemComponent(
        anchor: Anchor.center,
        particle: Particle.generate(
          count: count,
          lifespan: lifespan,
          generator: (_) => AcceleratedParticle(
            acceleration: acceleration ?? _getRandomVector(rnd),
            speed: speed ?? _getRandomVector(rnd),
            position: position.clone(),
            child: CircleParticle(
              radius: radius,
              paint: Paint()..color = color,
            ),
          ),
        ),
      );
      game.findByKeyName(GameConstants.gameView)!.add(particleComponent);
    }
  }

  Vector2 _getRandomVector(double rnd) {
    // Random vector generation for starting points
    return (Vector2.random(_random) - Vector2.random(_random)) * rnd;
  }
}
