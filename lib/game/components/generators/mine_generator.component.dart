import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/mines/mine_idle.sprite.dart';

class MineGenerator extends Component with HasGameReference<MiMGame> {
  late Vector2 mineSize;
  GamePhaseController? _phaseController;
  bool creating = false;

  MineGenerator() : super(key: ComponentKey.named(GameConstants.mineGenerator));

  @override
  Future<void> onLoad() async {
    mineSize = MiMSizes.getMineIdleTextureSize(game.gameViewSize);
    return super.onLoad();
  }

  @override
  void update(double dt) {
    if (creating) {
      _phaseController ??= game.findByKeyName<GamePhaseController>(GameConstants.phaseController)!;

      if (_phaseController!.currentPhase.type == GamePhaseTypes.enemies ||
          _phaseController!.currentPhase.type == GamePhaseTypes.asteroids) {
        final number = Random().nextInt(_phaseController!.getMineChance());
        final chance = number == 11;

        if (chance) {
          _generateMine();
        }
      }
    }

    super.update(dt);
  }

  void _generateMine() async {
    Vector2 position = Vector2(game.gameViewSize.x + mineSize.x, _getY());

    game.findByKeyName(GameConstants.gameView)!.add(MineIdleSprite(position: position));
  }

  double _getY() =>
      (game.portraitOrientation ? 0 : game.hudHeight) +
      mineSize.y / 2 +
      Random().nextDouble() * (game.availableHeight - 2 * mineSize.y / 2);
}
