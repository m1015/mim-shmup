import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/planet.sprite.dart';

class PlanetGenerator extends Component with HasGameRef<MiMGame> {
  final List<PlanetSprite> planets = [];

  double _time = 0;
  double _timer = GameConstants.timerSpawnPlanet;

  GameLevelController? _levelController;

  PlanetGenerator() : super(key: ComponentKey.named(GameConstants.planetGenerator));

  @override
  void update(double dt) {
    _time += dt;

    if (_time > _timer) {
      if (planets.isEmpty) {
        _generatePlanet();
      }
    }

    super.update(dt);
  }

  void _removePlanet() {
    planets.removeLast();
    _timer = _time + GameConstants.timerSpawnPlanet;
  }

  void _generatePlanet() {
    _levelController ??= game.findByKeyName(GameConstants.levelController);

    final planetAssets = _levelController!.currentLevel.planets.map((asset) => asset.filename).toList();

    final randomAssetIndex = Random().nextInt(planetAssets.length);
    final randomAsset = planetAssets[randomAssetIndex];
    final randomSize =
        MiMSizes.minPlanetSize + Random().nextDouble() * (MiMSizes.maxPlanetSize - MiMSizes.minPlanetSize);

    final size = Vector2(randomSize, randomSize);
    final position = Vector2(game.gameViewSize.x + size.x, game.gameViewSize.y / 2);

    final planet = PlanetSprite(
      removePlanet: _removePlanet,
      position: position,
      size: size,
      asset: randomAsset,
      baseSpeed: GameConstants.basePlanetSpeed + Random().nextInt(GameConstants.basePlanetSpeed.toInt()),
    );

    planets.add(planet);
    add(planet);
  }
}
