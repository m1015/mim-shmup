import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_shoot_type.enum.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/patterns/eight.pattern.dart';
import 'package:mim_shmup/game/patterns/enemy.pattern.dart';
import 'package:mim_shmup/game/patterns/static.pattern.dart';

class BossGenerator extends Component with HasGameReference<MiMGame> {
  bool _creating = false;
  EnemyPattern? _pattern;

  late ComponentsNotifier<GamePhaseController> _phaseControllerNotifier;

  BossGenerator() : super(key: ComponentKey.named(GameConstants.bossGenerator));

  set creating(bool value) {
    _creating = value;
    _startBossGeneration();
  }

  @override
  void onLoad() async {
    _phaseControllerNotifier = game.componentsNotifier<GamePhaseController>()
      ..addListener(() {
        _startBossGeneration();
      });

    return super.onLoad();
  }

  void _startBossGeneration() {
    final phaseController = _phaseControllerNotifier.single;

    if (phaseController!.currentPhase.type == GamePhaseTypes.boss) {
      if (_creating) {
        _creating = false;
        final type = [
          ...EnemyTypes.values.where((type) => type.isBoss),
        ][min((phaseController.countPhase ~/ 3) - 1, 5)];
        if (type.shootType == EnemyShootType.laser) {
          _pattern = StaticPattern(game: game, type: type);
        } else {
          _pattern = EightPattern(game: game, type: type);
        }
      }
    }
  }

  @override
  void update(double dt) {
    if (_pattern?.finished ?? false) {
      _pattern = null;
      _phaseControllerNotifier.single?.currentPhase.setFinished();
    }

    _pattern?.update(dt);

    super.update(dt);
  }

  void reset() {
    _creating = true;
  }
}
