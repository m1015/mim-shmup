import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/player_bullet_types.enum.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/bullets/player_bullet.sprite.dart';

const double horizontalAngle = pi / 2;
const double angleFactor = 0.05;

class PlayerBulletGenerator extends TimerComponent with HasGameReference<MiMGame> {
  PlayerBulletGenerator({
    super.period = 0.2,
  }) : super(repeat: true, autoStart: true);

  bool _firing = false;
  late WeakReference<PositionComponent> _source;

  @override
  void onTick() {
    if (_firing && _source.target != null) {
      _shoot();
    }
    super.onTick();
  }

  List<PlayerBulletSprite> _getBulletPerLevel(int level) {
    List<PlayerBulletSprite> bullets = [];
    if (level >= 0) {
      Vector2 bulletPosition = MiMSizes.getMainBulletPosition(game.gameViewSize, 0);
      bullets.addAll([
        PlayerBulletSprite(
          type: PlayerBulletTypesEnum.main,
          position: _source.target!.position + Vector2(bulletPosition.x, -bulletPosition.y),
          angle: horizontalAngle,
        ),
        PlayerBulletSprite(
          type: PlayerBulletTypesEnum.main,
          position: _source.target!.position + bulletPosition,
          angle: horizontalAngle,
        ),
      ]);
    }
    if (level >= 1) {
      bullets.add(
        PlayerBulletSprite(
          type: PlayerBulletTypesEnum.main,
          position: _source.target!.position + MiMSizes.getMainBulletPosition(game.gameViewSize, 1),
          angle: horizontalAngle,
        ),
      );
    }
    if (level >= GameConstants.maxMultiFireLevel) {
      Vector2 bulletPosition =
          MiMSizes.getMainBulletPosition(game.gameViewSize, GameConstants.maxMultiFireLevel);
      bullets.addAll([
        PlayerBulletSprite(
          type: PlayerBulletTypesEnum.main,
          position: _source.target!.position + Vector2(bulletPosition.x, -bulletPosition.y),
          angle: horizontalAngle,
        ),
        PlayerBulletSprite(
          type: PlayerBulletTypesEnum.main,
          position: _source.target!.position + bulletPosition,
          angle: horizontalAngle,
        ),
      ]);
    }
    return bullets;
  }

  void _shoot() {
    final int multiFireLevel = game.gameSession!.currentMultiFireLevel;

    game.findByKeyName(GameConstants.gameView)!.addAll(_getBulletPerLevel(multiFireLevel));
  }

  void setFiring({
    required bool firing,
    required PositionComponent source,
  }) {
    _firing = firing;
    _source = WeakReference(source);
    _shoot();
  }
}
