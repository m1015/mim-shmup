import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/mines/mine_idle.sprite.dart';

/// in seconds
const duration = 2;

class Nuke extends CircleComponent with HasGameReference<MiMGame>, CollisionCallbacks {
  Nuke({super.anchor = Anchor.center});

  double _elapsedTime = 0;

  final CircleHitbox _hitBox = CircleHitbox(radius: 0);

  @override
  Future<void> onLoad() {
    add(_hitBox);
    game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
          color: Colors.yellow,
          position: position.clone(),
          count: 200,
          lifespan: 2,
          radius: 1,
        );
    game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
          color: Colors.orange,
          position: position.clone(),
          count: 200,
          lifespan: 2,
          radius: 1,
        );
    game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
          color: Colors.red,
          position: position.clone(),
          count: 200,
          lifespan: 2,
          radius: 1,
        );
    return super.onLoad();
  }

  @override
  void update(double dt) {
    _elapsedTime += dt;
    radius = (_elapsedTime * 100 / duration) * game.gameViewSize.x / 100;
    opacity = min(_elapsedTime / duration, 1);
    _hitBox.radius = radius;
    if (_elapsedTime > duration) removeFromParent();
    super.update(dt);
  }

  @override
  void onCollisionStart(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollisionStart(intersectionPoints, other);
    if (other is EnemySprite) {
      other.death();
    }
    if (other is AsteroidSprite) {
      other.death();
    }
    if (other is BulletSprite) {
      other.removeFromParent();
    }
    if (other is MineIdleSprite) {
      other.death();
    }
  }
}
