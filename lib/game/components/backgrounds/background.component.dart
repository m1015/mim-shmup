import 'dart:async';

import 'package:flame/components.dart';
import 'package:flame/parallax.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/components/backgrounds/warp.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

/// in seconds
const speedUpDuration = 2;

class BackgroundComponent extends Component with HasGameRef<MiMGame> {
  late ParallaxComponent _background;
  bool isSpeedingUp = false;
  double _elapsedTime = 0;

  final Vector2 _baseVelocity = Vector2(20, 0);
  final WarpComponent _warpComponent = WarpComponent();

  BackgroundComponent() : super(key: ComponentKey.named(GameConstants.background));

  @override
  Future<void> onLoad() async {
    _background = await ParallaxComponent.load(
      size: game.gameViewSize,
      [
        ParallaxImageData(Assets.images.backgrounds.level1.farback.filename),
        ParallaxImageData(Assets.images.backgrounds.stars1.filename),
        ParallaxImageData(Assets.images.backgrounds.stars2.filename),
      ],
      repeat: ImageRepeat.repeat,
      baseVelocity: _baseVelocity,
      velocityMultiplierDelta: Vector2(2, 1),
    );

    add(_background);
    add(_warpComponent);

    return super.onLoad();
  }

  @override
  void onRemove() {
    remove(_background);

    super.onRemove();
  }

  @override
  void update(double dt) {
    _elapsedTime += dt;
    if (isSpeedingUp) {
      if (_background.parallax!.baseVelocity.x < 100) {
        _baseVelocity.x += _elapsedTime / speedUpDuration;
        _background.parallax!.baseVelocity = _baseVelocity;
      } else {
        _elapsedTime = 0;
      }
    } else {
      if (_background.parallax!.baseVelocity.x > 20) {
        _baseVelocity.x -= _elapsedTime / speedUpDuration;
        _background.parallax!.baseVelocity = _baseVelocity;
      } else {
        _elapsedTime = 0;
      }
    }

    super.update(dt);
  }

  void toggleSpeedUp(bool speedingUp, bool withWarp) {
    isSpeedingUp = speedingUp;
    if (withWarp) _warpComponent.setSpeedingUp(speedingUp);
  }

  void updateBackground(String farback) async {
    remove(_background);
    remove(_warpComponent);

    _background = await ParallaxComponent.load(
      size: game.gameViewSize,
      [
        ParallaxImageData(farback),
        ParallaxImageData(Assets.images.backgrounds.stars1.filename),
        ParallaxImageData(Assets.images.backgrounds.stars2.filename),
      ],
      repeat: ImageRepeat.repeat,
      baseVelocity: _baseVelocity,
      velocityMultiplierDelta: Vector2(2, 1),
    );

    add(_background);
    add(_warpComponent);
  }
}
