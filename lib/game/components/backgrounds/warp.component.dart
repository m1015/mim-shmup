import 'dart:math';
import 'dart:async' as dart_async;

import 'package:flame/components.dart';
import 'package:mim_shmup/game/mim_game.dart';

/// in seconds
const duration = 2.0;

class WarpComponent extends RectangleComponent with HasGameRef<MiMGame> {
  double _elapsedTime = duration;
  bool _isSpeedingUp = false;
  double _opacity = 0;
  dart_async.Timer? _transitionTimer;

  WarpComponent()
      : super(
          position: Vector2.zero(),
          anchor: Anchor.topLeft,
        );

  @override
  Future<void> onLoad() async {
    size = game.gameViewSize;
    return super.onLoad();
  }

  @override
  void update(double dt) {
    _elapsedTime += dt;

    if (_isSpeedingUp) {
      _opacity = min(_elapsedTime / duration, 1);
    } else {
      _opacity = max(1 - _elapsedTime / duration, 0);
    }

    if (_opacity >= 0 && _opacity <= 1) {
      opacity = _opacity;
    }

    return super.update(dt);
  }

  void setSpeedingUp(bool isSpeedingUp) {
    if (_transitionTimer != null) {
      _transitionTimer!.cancel();
    }

    _transitionTimer = dart_async.Timer(Duration(seconds: isSpeedingUp ? 1 : 0), () {
      _opacity = isSpeedingUp ? 0 : 1;
      _elapsedTime = 0;
      _isSpeedingUp = isSpeedingUp;
      _transitionTimer = null;
    });
  }
}
