import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/game/components/session/models/game_phase.model.dart';

class GamePhaseFactory {
  GamePhaseModel? _currentPhase;

  GamePhaseModel nextPhase(int countPhase) {
    /// Determine the phase based on the countPhase value
    if (countPhase == 25) {
      /// the end of the game
      _currentPhase = GamePhaseModel(GamePhaseTypes.endGame);
    } else if (countPhase % 4 == 1) {
      /// Phase 1, 5, 9, ... should be enemies
      _currentPhase = GamePhaseModel(GamePhaseTypes.enemies);
    } else if (countPhase % 4 == 2) {
      /// Phase 2, 6, 10, ... should be asteroids
      _currentPhase = GamePhaseModel(GamePhaseTypes.asteroids);
    } else if (countPhase % 4 == 3) {
      /// Phase 3, 7, 11, ... should be a boss
      _currentPhase = GamePhaseModel(GamePhaseTypes.boss);
    } else if (countPhase % 4 == 0) {
      /// Phase 4, 8, 12, ... should be the rewards screen
      _currentPhase = GamePhaseModel(GamePhaseTypes.rewards);
    }

    return _currentPhase!;
  }

  void reset() {
    _currentPhase = null;
  }
}
