class AsteroidCollision {
  final DateTime creationTime;
  final int hashCodesSum;

  AsteroidCollision(this.creationTime, this.hashCodesSum);
}
