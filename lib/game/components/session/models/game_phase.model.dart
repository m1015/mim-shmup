import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';

class GamePhaseModel {
  final GamePhaseTypes type;

  bool _inProgress = true;

  GamePhaseModel(this.type);

  void setFinished() => _inProgress = false;

  bool get isFinished => !_inProgress;
}
