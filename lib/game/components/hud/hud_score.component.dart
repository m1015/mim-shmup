import 'dart:math';

import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/game/components/hud/hud_score_mulitplier.component.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/fonts.gen.dart';

class HudScoreComponent extends TextComponent with HasGameReference<MiMGame> {
  HudScoreComponent({
    required super.position,
  }) : super(anchor: Anchor.center, key: ComponentKey.named(GameConstants.hudScore));

  /// Game dynamic values
  late ComponentsNotifier<GameSessionController> _sessionNotifier;
  int multiplier = 1;

  NumberFormat get _scoreFormat => NumberFormat("00000000000");

  TextPaint get _scorePainter => TextPaint(
        style: TextStyle(
          fontFamily: FontFamily.valorax,
          fontSize: game.textTheme?.displaySmall?.fontSize,
          color: halloweenOrange,
        ),
      );

  int _currentScore = 0;
  int _displayedScore = 0;

  @override
  Future<void> onLoad() async {
    text = _scoreFormat.format(0);
    textRenderer = _scorePainter;

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          _currentScore = session.currentScore;
        }
      });

    add(HudScoreMultiplierComponent(position: Vector2(size.x * 0.95, size.y)));

    return super.onLoad();
  }

  @override
  void update(double dt) {
    /// this aims at increasing the score progressively
    if (_displayedScore < _currentScore) {
      /// we try to have a maximum 10000 points added every given time
      final value = min(_currentScore - _displayedScore, 10000);

      /// we take a percentage of the computed value
      _displayedScore += (value * dt * 5).floor();

      /// if the difference between the current display and the real score is minimal, we reach it right away
      if (_currentScore - _displayedScore < 20) _displayedScore = _currentScore;

      text = _scoreFormat.format(_displayedScore);
    }
    super.update(dt);
  }

  void reset() {
    _currentScore = 0;
    _displayedScore = 0;

    text = _scoreFormat.format(_displayedScore);
  }

  void setFinalScore() {
    _displayedScore = _currentScore;
  }
}
