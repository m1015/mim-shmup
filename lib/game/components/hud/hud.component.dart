import 'dart:async';

import 'package:flame/components.dart';
import 'package:flame_bloc/flame_bloc.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/enums/weapon_types.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/core/utils/path.helper.dart';
import 'package:mim_shmup/game/components/hud/hud_score.component.dart';
import 'package:mim_shmup/game/components/hud/hud_upgrade.component.dart';
import 'package:mim_shmup/game/components/hud/pause_button.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/gen/assets.gen.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/cubits/game_status.state.dart';

class HudComponent extends PositionComponent
    with HasGameReference<MiMGame>, FlameBlocListenable<GameStatusCubit, GameStatusState> {
  HudComponent({super.size}) : super(priority: 1);

  /// Size getters
  Vector2 get hudUpgradeSize => Vector2(size.x / 4, size.y * 0.4);
  double get hudPadding => (size.y - 2 * hudUpgradeSize.y) / 3;

  @override
  Future<void> onLoad() async {
    addAll([
      HudUpgradeComponent(
        type: PlayerUpgradeTypes.power,
        size: hudUpgradeSize,
        position: Vector2(hudPadding * 3, hudPadding),
      ),
      HudUpgradeComponent(
        type: PlayerUpgradeTypes.satellite,
        size: hudUpgradeSize,
        position: Vector2(hudPadding * 3, hudPadding * 2 + hudUpgradeSize.y),
      ),
      HudUpgradeComponent(
        type: PlayerUpgradeTypes.multiFire,
        size: hudUpgradeSize,
        position: Vector2(size.x - hudPadding * 3, hudPadding),
      ),
      PauseButtonComponent(
        sprite: await Sprite.load(Assets.images.ui.pause.filename),
        onPressed: () {
          bloc.pause();
          game.router.pushNamed(Routes.pause);
        },
        size: Vector2(hudUpgradeSize.y, hudUpgradeSize.y),
        position: Vector2(size.x - hudPadding * 3, hudPadding * 2 + hudUpgradeSize.y),
        anchor: Anchor.topRight,
      ),
      HudScoreComponent(position: size / 2),
    ]);
    return super.onLoad();
  }

  @override
  void render(Canvas canvas) {
    final Size canvasSize = Size(size.x, size.y);

    /// Draw component shape
    final paint = Paint()
      ..color = black.withOpacity(0.6)
      ..style = PaintingStyle.fill;
    canvas.drawPath(PathHelper.getTopComponentPath(canvasSize), paint);
  }
}
