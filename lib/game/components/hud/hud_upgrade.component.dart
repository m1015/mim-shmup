import 'dart:async';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/weapon_types.enum.dart';
import 'package:mim_shmup/game/components/hud/hud_upgrade.sprite.dart';
import 'package:mim_shmup/game/components/hud/hud_upgrade_level.sprite.dart';
import 'package:mim_shmup/game/mim_game.dart';

class HudUpgradeComponent extends PositionComponent with HasGameReference<MiMGame> {
  HudUpgradeComponent({
    required this.type,
    required super.size,
    required super.position,
  }) : super(anchor: type.isLeft ? Anchor.topLeft : Anchor.topRight);

  final PlayerUpgradeTypes type;

  Vector2 get upgradeSize => Vector2(size.y, size.y);
  Vector2 get assetPosition => type.isLeft ? Vector2.zero() : Vector2(size.x, 0);

  Vector2 _getLevelPosition(int index) => type.isLeft
      ? Vector2(upgradeSize.x, upgradeSize.y / 2) +
          Vector2(index * MiMSizes.getUpgradeLevelSize(upgradeSize).x, 0)
      : Vector2(size.x, upgradeSize.y / 2) -
          Vector2(upgradeSize.x, 0) -
          Vector2(index * MiMSizes.getUpgradeLevelSize(upgradeSize).x, 0);

  @override
  FutureOr<void> onLoad() {
    addAll([
      HudUpgradeSprite(
        fileName: type.hudAsset,
        position: assetPosition,
        size: upgradeSize,
        anchor: type.isLeft ? Anchor.topLeft : Anchor.topRight,
      ),
      ...List<Component>.generate(
        type.maxLevel,
        (int index) => HudUpgradeLevelSprite(
          type: type,
          levelToActivate: index + 1,
          position: _getLevelPosition(index),
          size: MiMSizes.getUpgradeLevelSize(size),
          anchor: type.isLeft ? Anchor.centerLeft : Anchor.centerRight,
        ),
      ),
    ]);
    return super.onLoad();
  }
}
