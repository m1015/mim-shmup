import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/fonts.gen.dart';

class HudScoreMultiplierComponent extends TextComponent with HasGameReference<MiMGame> {
  HudScoreMultiplierComponent({
    required super.position,
  }) : super(anchor: Anchor.centerLeft);

  /// Game dynamic values
  late ComponentsNotifier<GameSessionController> _sessionNotifier;

  @override
  Future<void> onLoad() async {
    textRenderer = TextPaint(
      style: TextStyle(
        fontFamily: FontFamily.swipeRace,
        fontSize: game.textTheme?.headlineMedium?.fontSize,
        color: turkishRed,
      ),
    );

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          text = session.currentScoreMultiplication > 1
              ? 'x${session.currentScoreMultiplication}'
              : '';
        }
      });

    return super.onLoad();
  }
}
