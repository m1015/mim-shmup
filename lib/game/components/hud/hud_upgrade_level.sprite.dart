import 'package:flame/components.dart';
import 'package:flame/sprite.dart';
import 'package:mim_shmup/core/enums/weapon_types.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class HudUpgradeLevelSprite extends SpriteAnimationComponent with HasGameReference<MiMGame> {
  HudUpgradeLevelSprite({
    required this.type,
    this.levelToActivate = 1,
    required super.position,
    required super.size,
    super.anchor = Anchor.centerLeft,
  });

  final PlayerUpgradeTypes type;
  final int levelToActivate;

  late SpriteSheet _spriteSheet;
  late ComponentsNotifier<GameSessionController> _sessionNotifier;
  bool _activated = false;

  @override
  Future<void> onLoad() async {
    /// Create sprite sheet
    _spriteSheet = SpriteSheet.fromColumnsAndRows(
      image: await game.images.load(Assets.images.sprites.hud.levelFilling.filename),
      rows: 1,
      columns: 5,
    );

    /// Create animation from sprite sheet
    /// From index to another index (excluded)
    animation = _spriteSheet.createAnimation(
      row: 0,
      stepTime: 0.1,
      from: 0,
      to: 1,
      loop: false,
    );

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          if (!_activated && type.currentLevel(session) >= levelToActivate) {
            _activated = true;
            animation = _spriteSheet.createAnimation(
              row: 0,
              stepTime: 0.1,
              from: 0,
              to: 5,
              loop: false,
            );
          } else if (_activated && type.currentLevel(session) < levelToActivate) {
            _activated = false;
            animation = _spriteSheet.createAnimation(
              row: 0,
              stepTime: 0.1,
              from: 0,
              to: 1,
              loop: false,
            );
          }
        }
      });
  }
}
