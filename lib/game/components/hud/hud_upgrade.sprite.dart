import 'package:flame/components.dart';

class HudUpgradeSprite extends SpriteComponent with HasGameRef {
  HudUpgradeSprite({
    required this.fileName,
    required super.position,
    required super.size,
    super.anchor = Anchor.topLeft,
  });

  final String fileName;

  @override
  Future<void> onLoad() async {
    sprite = await Sprite.load(fileName);
  }
}
