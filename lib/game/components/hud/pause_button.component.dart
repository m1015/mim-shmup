import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

class PauseButtonComponent extends ButtonComponent {
  final Sprite sprite;

  ///Sprite and position of our button
  PauseButtonComponent({
    required this.sprite,
    required VoidCallback onPressed,
    required super.position,
    required super.size,
    super.anchor = Anchor.topLeft,
  }) : super(
          onPressed: onPressed,
          button: SpriteComponent(
            sprite: sprite,
            size: size,
          ),
          buttonDown: SpriteComponent(
            size: size,
          ),
        );
}
