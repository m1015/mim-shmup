import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/effects.dart';

MoveEffect reversedLoopEffect(Vector2 screenSize, Vector2 startPosition) {
  /// we compute the points of the loop
  final pointA = Vector2(3 * screenSize.x / 4, screenSize.y * 0.2);
  final pointB = Vector2(screenSize.x / 2, screenSize.y / 2);
  final pointC = Vector2(3 * screenSize.x / 4, screenSize.y - 50);
  final pointD = Vector2(screenSize.x - 50, screenSize.y / 2);
  final pointE = Vector2(-50, screenSize.y * 0.2);

  /// we compute the path from the points and add a bezier transformation between them for smoother movements
  final path = Path()
    ..moveTo(startPosition.x, startPosition.y)
    ..quadraticBezierTo(
      startPosition.x,
      pointA.y,
      pointA.x,
      pointA.y,
    )
    ..quadraticBezierTo(
      pointB.x,
      pointA.y,
      pointB.x,
      pointB.y,
    )
    ..quadraticBezierTo(
      pointB.x,
      pointC.y,
      pointC.x,
      pointC.y,
    )
    ..quadraticBezierTo(
      pointD.x,
      pointC.y,
      pointD.x,
      pointD.y,
    )
    ..quadraticBezierTo(
      pointD.x,
      pointE.y,
      pointE.x,
      pointE.y,
    );

  final effect = MoveAlongPathEffect(
    path,
    EffectController(duration: 12.0),
    absolute: true,
  );
  return effect;
}
