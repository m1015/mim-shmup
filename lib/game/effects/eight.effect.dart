import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/effects.dart';

MoveEffect eightEffect(Vector2 screenSize) {
  final yOffset = screenSize.y * 0.1;
  final yPadding = screenSize.y * 0.2;

  /// we compute the points of the eight
  final pointA = Vector2(3 * screenSize.x / 4, screenSize.y / 2 + yOffset);
  final pointB = Vector2(4 * screenSize.x / 6, 3 * screenSize.y / 4 + yOffset);
  final pointC = Vector2(3 * screenSize.x / 4, screenSize.y - yPadding + yOffset);
  final pointD = Vector2(5 * screenSize.x / 6, 3 * screenSize.y / 4 + yOffset);
  final pointE = Vector2(3 * screenSize.x / 4, screenSize.y / 2 + yOffset);
  final pointF = Vector2(4 * screenSize.x / 6, screenSize.y / 4 + yOffset);
  final pointG = Vector2(3 * screenSize.x / 4, yPadding);
  final pointH = Vector2(5 * screenSize.x / 6, screenSize.y / 4 + yOffset);

  /// we compute the path from the points and add a bezier transformation between them for smoother movements
  final path = Path()
    ..moveTo(pointA.x, pointA.y)
    ..quadraticBezierTo(
      pointB.x,
      pointA.y,
      pointB.x,
      pointB.y,
    )
    ..quadraticBezierTo(
      pointB.x,
      pointC.y,
      pointC.x,
      pointC.y,
    )
    ..quadraticBezierTo(
      pointD.x,
      pointC.y,
      pointD.x,
      pointD.y,
    )
    ..quadraticBezierTo(
      pointD.x,
      pointE.y,
      pointE.x,
      pointE.y,
    )
    ..quadraticBezierTo(
      pointF.x,
      pointE.y,
      pointF.x,
      pointF.y,
    )
    ..quadraticBezierTo(
      pointF.x,
      pointG.y,
      pointG.x,
      pointG.y,
    )
    ..quadraticBezierTo(
      pointH.x,
      pointG.y,
      pointH.x,
      pointH.y,
    )
    ..quadraticBezierTo(
      pointH.x,
      pointA.y,
      pointA.x,
      pointA.y,
    );

  final effect = MoveAlongPathEffect(
    path,
    InfiniteEffectController(EffectController(duration: 12.0)),
    absolute: true,
  );
  return effect;
}
