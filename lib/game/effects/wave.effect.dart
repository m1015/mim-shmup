import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/effects.dart';

MoveEffect waveEffect(Vector2 screenSize, Vector2 startPosition, double amplitude, int frequency) {
  final List<(Vector2, bool)> vectors = [];
  /// phase varies between -1, 0 and 1. It is used to compute the y values of the path
  int phase = 0;
  /// up means the next computed point will be upper than the previous one
  bool up = false;
  /// this is used to compute the control point of the bezier transformation
  bool rightWay = true;

  /// we generate the points of the path
  for (var index = frequency; index >= 0; index--) {
    vectors.add((Vector2(index * screenSize.x / frequency, startPosition.y + (phase * amplitude)), rightWay));
    rightWay = !rightWay;
    if (up) {
      phase++;
    } else {
      phase--;
    }
    if (phase > 1) {
      phase = 0;
      up = false;
    } else if (phase < -1) {
      phase = 0;
      up = true;
    }
  }

  /// we compute the first bezier transformation, between the start point and the first computed point
  final path = Path()..moveTo(startPosition.x, startPosition.y);

  path.quadraticBezierTo(
    vectors[0].$1.x,
    startPosition.y,
    vectors[0].$1.x,
    vectors[0].$1.y,
  );

  /// then we iterate through the computed points
  for (var index = 0; index < vectors.length; index++) {
    final vector = vectors[index];
    final previous = index == 0 ? startPosition : vectors[index - 1].$1;
    final vector2 = vector.$1;
    final same = vector.$2;

    final control = same
        ? Vector2(
            vector2.x,
            previous.y,
          )
        : Vector2(
            previous.x,
            vector2.y,
          );

    path.quadraticBezierTo(
      control.x,
      control.y,
      vector2.x,
      vector2.y,
    );
  }

  /// we add one last point outside of the screen or the enemy won't disappear
  final lastVector = vectors.last.$1;

  path.quadraticBezierTo(
    lastVector.x,
    lastVector.y,
    -100,
    lastVector.y,
  );

  final effect = MoveAlongPathEffect(
    path,
    EffectController(duration: 12.0),
    absolute: true,
  );

  return effect;
}
