import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class DeathSprite<T extends FlameGame> extends SpriteAnimationComponent with HasGameReference<MiMGame> {
  final void Function() callback;

  DeathSprite({
    super.position,
    required this.callback,
  }) : super(
          anchor: Anchor.center,
          removeOnFinish: true,
        );

  @mustCallSuper
  @override
  Future<void> onLoad() async {
    final textureSize = Vector2(57, 57);
    final spriteHeight = game.gameViewSize.y * textureSize.y / GameConstants.baseScreenHeight;
    final spriteWidth = spriteHeight * textureSize.x / textureSize.y;

    size = Vector2(spriteWidth, spriteHeight);

    animation = await game.loadSpriteAnimation(
      Assets.images.sprites.death.filename,
      SpriteAnimationData.sequenced(
        amount: 12,
        textureSize: textureSize,
        stepTime: 0.15,
        loop: false,
      ),
    );

    animationTicker!.onComplete = callback;
  }
}
