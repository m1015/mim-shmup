import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flame/sprite.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/player_bullet_types.enum.dart';
import 'package:mim_shmup/core/extensions/game.extension.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

class PlayerBulletSprite extends SpriteAnimationComponent
    with HasGameRef<MiMGame>, CollisionCallbacks {
  PlayerBulletSprite({
    this.type = PlayerBulletTypesEnum.main,
    required super.position,
    super.angle,
  }) : super(anchor: Anchor.center);

  late final Vector2 velocity;
  final Vector2 deltaPosition = Vector2.zero();
  final PlayerBulletTypesEnum type;

  @override
  Future<void> onLoad() async {
    size = type.getSize(game.gameViewSize);
    final spriteSheet = SpriteSheet.fromColumnsAndRows(
      image: game.imageFromCache(type.sprite),
      rows: 1,
      columns: type.columns,
    );
    animation = spriteSheet.createAnimation(
      row: 0,
      stepTime: 0.15,
      from: 0,
      to: type.columns,
      loop: true,
    );

    velocity = Vector2(0, -1)
      ..rotate(angle)
      ..scale(GameConstants.bulletsSpeed * game.gameViewSize.y / GameConstants.baseScreenHeight);

    addAll([
      RectangleHitbox(size: size),
      ColorEffect(
        game.gameSession!.firePowerColor,
        InfiniteEffectController(
          SequenceEffectController(
            [
              CurvedEffectController(1, Curves.easeOut),
              ReverseCurvedEffectController(1, Curves.easeOut),
            ],
          ),
        ),
        opacityFrom: 0.05,
        opacityTo: 0.2,
      )
    ]);

    game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
          color: Colors.grey.withOpacity(0.7),
          position: Vector2(
            position.x + (Random().nextDouble() * size.x) - (size.x / 2),
            position.y + (Random().nextDouble() * size.y) - (size.y / 2),
          ),
          count: 10,
          radius: 1,
          rnd: 10,
        );
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    if (other is EnemySprite || other is AsteroidSprite) {
      removeFromParent();
    }
  }

  @override
  void update(double dt) {
    super.update(dt);
    deltaPosition
      ..setFrom(velocity)
      ..scale(dt);
    position += deltaPosition;

    if (position.y < 0 || position.x > game.gameViewSize.x || position.x + size.x < 0) {
      removeFromParent();
    }
  }
}
