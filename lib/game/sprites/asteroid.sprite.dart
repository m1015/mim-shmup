import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/asteroid_types.enum.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/controllers/asteroid_collisions_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/bullets/player_bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/explosion.sprite.dart';
import 'package:mim_shmup/game/sprites/missile.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';

class AsteroidSprite extends SpriteComponent with HasGameRef<MiMGame>, CollisionCallbacks {
  final AsteroidType type;
  late final double speed;
  double direction;
  Vector2 velocity = Vector2.zero();
  double health = 0;
  double spinSpeed = 0.25;
  bool _alive = true;

  AsteroidSprite({
    required this.type,
    required this.direction,
    super.angle,
    super.position,
  }) : super(anchor: Anchor.center);

  @override
  Future<void> onLoad() async {
    sprite = Sprite(game.images.fromCache(type.sprite));

    size = type.getSize(game.gameViewSize);
    health = type.health;

    spinSpeed = -0.5 + Random().nextDouble();

    speed = (GameConstants.baseAsteroidSpeed * game.gameViewSize.y / GameConstants.baseScreenHeight) +
        Random().nextDouble() * GameConstants.variableAsteroidSpeed;

    final opposite = speed * tan(direction);

    velocity = Vector2(-speed, opposite);

    final radius = min(size.x, size.y) / 2;

    add(CircleHitbox(radius: radius));

    super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    /// Set the new position
    position += velocity * dt;

    /// make the thing rotate over time
    angle += spinSpeed * dt;
    angle %= 2 * pi;

    if (position.x + size.x < 0 || position.y - size.y > game.gameViewSize.y || position.y + size.y < 0) {
      removeFromParent();
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    if (!_alive) return;

    final GameSessionController? gameSession = game.gameSession;

    if (other is BulletSprite || other is PlayerBulletSprite) {
      int power = 1;

      if (gameSession != null) {
        power = gameSession.currentFirePowerLevel;
        game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
              color: gameSession.firePowerColor,
              position: position.clone(),
            );
      }

      health = health - power;
      if (health <= 0) {
        death();
      }
    } else if (other is MissileSprite) {
      if (gameSession != null) {
        other.removeFromParent();
        health = health - gameSession.currentMissileFirePower;
        if (health <= 0) {
          death();
        }
      }
    } else if (other is EnemySprite) {
      death();
      if (!other.type.isBoss) other.health -= health / 10;
    } else if (other is PlayerSprite && (gameSession?.currentShieldState ?? false)) {
      death();
    } else if (other is AsteroidSprite) {
      final hashCodesSum = hashCode + other.hashCode;

      final collisionsController =
          game.findByKeyName<AsteroidCollisionsController>(GameConstants.asteroidCollisionsController);

      if (collisionsController != null && !collisionsController.addCollision(hashCodesSum)) {
        final otherVelocity = other.velocity.clone();
        other.velocity.setFrom(velocity.clone());
        velocity.setFrom(otherVelocity);

        final otherSpinSpeed = other.spinSpeed;
        other.spinSpeed = spinSpeed;
        spinSpeed = otherSpinSpeed;
      }
    }
  }

  void death() {
    if (!_alive) return;

    _alive = false;

    game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
          color: const Color(0xff9a6f3b),
          position: position.clone(),
          count: 30,
          lifespan: 0.8,
          rnd: 250,
          radius: 3,
        );

    game.findByKeyName(GameConstants.gameView)!.add(ExplosionSprite(position: position));
    removeFromParent();
    game.gameSession!.addScore(type.score);

    final next = type.next();

    if (next != null) {
      game.findByKeyName(GameConstants.gameView)!.addAll([
        AsteroidSprite(
          type: next,
          position: position.clone(),
          direction: 2 * pi / 3 + (Random().nextDouble() * pi / 2),
        ),
        AsteroidSprite(
          type: next,
          position: position.clone(),
          direction: 5 * pi / 6 + (Random().nextDouble() * pi / 3),
        ),
      ]);
    }
  }
}
