import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/explosion.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class MissileSprite extends SpriteAnimationComponent with HasGameRef<MiMGame>, CollisionCallbacks {
  late final Vector2 velocity;
  final Vector2 deltaPosition = Vector2.zero();
  final PositionComponent? target;
  bool isMoving = false;

  MissileSprite({
    required super.position,
    required this.target,
  }) : super(
          anchor: Anchor.center,
        );

  @override
  Future<void> onLoad() async {
    size = MiMSizes.getMissileSize(game.gameViewSize);
    playing = false;
    // Charger l'animation du missile
    animation = await gameRef.loadSpriteAnimation(
      Assets.images.sprites.bullets.missileSprite.filename,
      SpriteAnimationData.sequenced(
        amount: 3,
        textureSize: size,
        stepTime: 0.15,
        loop: true,
      ),
    );

    // Définir la vélocité horizontale
    velocity = Vector2(1, 0)
      ..scale(
        GameConstants.missilesSpeed * game.gameViewSize.y / GameConstants.baseScreenHeight,
      );

    // Ajouter un TimerComponent pour le délai de 2 secondes
    addAll([
      TimerComponent(
        period: GameConstants.maxMissileCoolDown,
        repeat: false,
        autoStart: true,
        onTick: () {
          isMoving = true;
        },
      ),
      ColorEffect(
        turkishRed.withOpacity(0.3),
        InfiniteEffectController(
          SequenceEffectController(
            [
              CurvedEffectController(1, Curves.easeOut),
              ReverseCurvedEffectController(1, Curves.easeOut),
            ],
          ),
        ),
        opacityFrom: 0,
        opacityTo: 0.5,
      )
    ]);
  }

  @override
  void onMount() {
    super.onMount();

    // Ajouter un hitbox circulaire
    final shape = CircleHitbox.relative(
      1,
      parentSize: size,
      position: size / 2,
      anchor: Anchor.center,
    );
    add(shape);
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    if (other is EnemySprite || other is AsteroidSprite) {
      game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
            color: Colors.orange,
            position: position.clone(),
          );
      game.findByKeyName(GameConstants.gameView)!.add(ExplosionSprite(position: position));
      removeFromParent();
    }
  }

  @override
  Future<void> update(double dt) async {
    super.update(dt);

    if (isMoving) {
      game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
            color: Colors.orange.withOpacity(0.7),
            position: Vector2(
              position.x + (Random().nextDouble() * size.x) - (size.x / 2),
              position.y + (Random().nextDouble() * size.y) - (size.y / 2),
            ),
            count: 1,
            radius: 1,
            rnd: 1,
          );
      playing = true;
      deltaPosition
        ..setFrom(velocity)
        ..scale(dt);
      position += deltaPosition;
    }

    // Supprimer le missile s'il sort de l'écran
    if (position.x < 0 || position.x > game.gameViewSize.x || position.y < 0 || position.y > game.gameViewSize.y) {
      removeFromParent();
    }
  }
}
