import 'dart:math';

import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_shoot_type.enum.dart';
import 'package:mim_shmup/core/enums/power_ups_types.enum.dart';
import 'package:mim_shmup/game/components/generators/bullet_generator.component.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/components/generators/power_up_generator.component.dart';
import 'package:mim_shmup/game/components/nuke.component.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';

class BossSprite extends EnemySprite {
  int _phase = 1;
  bool _isDead = false;

  BossSprite({
    super.priority,
    super.key,
    required super.type,
    super.shootType,
  });

  @override
  void update(double dt) {
    super.update(dt);

    if (health < type.health / 2) {
      game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
            color: Colors.white.withOpacity(0.3),
            position: Vector2(
              position.x + (Random().nextDouble() * size.x) - (size.x / 2),
              position.y + (Random().nextDouble() * size.y) - (size.y / 2),
            ),
            lifespan: 1,
            count: 1,
            radius: 4,
            rnd: 1,
          );
    }

    if (health < type.health / 3 && _phase == 2) {
      _phase = 3;

      final bulletCreator2 = BulletGenerator(
        bigBulletSize: true,
        period: 0.3,
        type: EnemyShootType.tripleShoot,
        source: this,
        enemyType: type,
      );

      add(bulletCreator2);
      bulletCreators.add(bulletCreator2);
    } else if (health > type.health / 3 && health < type.health * 2 / 3 && _phase == 1) {
      _phase = 2;

      final bulletCreator2 = BulletGenerator(
        bigBulletSize: true,
        period: 0.3,
        type: EnemyShootType.bigShoot,
        source: this,
        enemyType: type,
      );

      add(bulletCreator2);
      bulletCreators.add(bulletCreator2);
    }
  }

  @override
  void death() {
    if (!_isDead) {
      _isDead = true;
      super.death();
      game.findByKeyName(GameConstants.gameView)!.add(Nuke()..position = position);
      game
          .findByKeyName<PowerUpGenerator>(GameConstants.powerUpGenerator)
          ?.generatePowerUp(position, PowerUpsTypes.bonusPhase);
    }
  }
}
