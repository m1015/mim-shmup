import 'package:flame/components.dart';
import 'package:mim_shmup/game/mim_game.dart';

class PlanetSprite extends SpriteComponent with HasGameReference<MiMGame> {
  final String asset;
  late final Vector2 velocity;
  final double baseSpeed;
  final Function() removePlanet;
  bool isSpeedingUp = false;
  double speed = 0;

  PlanetSprite({
    super.position,
    super.size,
    super.priority,
    super.key,
    required this.asset,
    required this.baseSpeed,
    required this.removePlanet,
  }) : super(anchor: Anchor.center) {
    speed = baseSpeed;
  }

  @override
  Future<void> onLoad() async {
    sprite = await game.loadSprite(
      asset,
    );

    super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    if (isSpeedingUp) {
      if (speed < baseSpeed * 5) {
        speed += 1;
      }
    } else {
      if (speed > baseSpeed) {
        speed -= 0.5;
      }
    }

    moveHorizontally(dt);
  }

  void moveHorizontally(double dt) {
    /// Calculate the new x position based on speed and time elapsed
    double newX = position.x - speed * dt;

    /// Set the new position
    position = Vector2(newX, position.y);

    if (position.x < -size.x) {
      removeFromParent();
      removePlanet();
    }
  }
}
