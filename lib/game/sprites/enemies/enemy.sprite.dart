import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_shoot_type.enum.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/components/generators/bullet_generator.component.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/components/generators/power_up_generator.component.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/bonus_points.sprite.dart';
import 'package:mim_shmup/game/sprites/bullets/player_bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy_engine_power.sprite.dart';
import 'package:mim_shmup/game/sprites/explosion.sprite.dart';
import 'package:mim_shmup/game/sprites/missile.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';

class EnemySprite extends SpriteAnimationComponent with HasGameRef<MiMGame>, CollisionCallbacks {
  final EnemyTypes type;
  EnemyShootType shootType;

  late double health;
  List<BulletGenerator> bulletCreators = [];

  PlayerSprite? _player;
  bool _alive = true;
  static int _enemiesKilled = 0;

  EnemySprite({
    super.priority,
    super.key,
    required this.type,
    this.shootType = EnemyShootType.noShoot,
  }) : super(anchor: Anchor.center);

  @override
  Future<void> onLoad() async {
    health = type.health;

    size = type.getSize(game.gameViewSize);

    // Adjust the hitbox position to be centered within the sprite
    add(RectangleHitbox(
      size: size * .75,
      position: size * 0.15, // This offsets the hitbox to be centered
    ));

    add(EnemyEnginePowerSprite(size: size, type: type));

    _player = game.findByKeyName<PlayerSprite>(GameConstants.player);

    if (shootType == EnemyShootType.noShoot) {
      animation = await game.loadSpriteAnimation(
        type.getShipSprite().filename,
        SpriteAnimationData.variable(
          amount: 1,
          textureSize: type.textureSize,
          stepTimes: List.filled(1, 0.15),
        ),
      );
    } else {
      var (sprite, number) = type.getFiringShipSprite();
      animation = await game.loadSpriteAnimation(
        sprite.filename,
        SpriteAnimationData.variable(
          amount: number,
          amountPerRow: 1,
          textureSize: type.textureSize,
          stepTimes: List.filled(number, 0.15),
        ),
      );

      final bulletCreator = BulletGenerator(
        bigBulletSize: shootType == EnemyShootType.bigShoot,
        period: shootType == EnemyShootType.bigShoot ? 3.0 : 2.0,
        type: shootType,
        enemyType: type,
        source: this,
      );

      add(bulletCreator);
      bulletCreators.add(bulletCreator);
    }

    super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    if (shootType == EnemyShootType.playerFocus && _player != null) {
      final angle = atan2(_player!.position.x - position.x, position.y - _player!.position.y);
      bulletCreators.first.updateAngle([angle]);
    }

    if (position.x + (size.x / 2) < 0) {
      removeFromParent();
      game.gameSession!.resetMultiplication();
      _enemiesKilled = 0;
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);
    final GameSessionController? gameSession = game.gameSession;

    if (other is PlayerBulletSprite) {
      if (gameSession != null) {
        game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
              color: gameSession.firePowerColor,
              position: position.clone(),
            );

        health = health - gameSession.currentFirePowerLevel;
      }
    } else if (other is MissileSprite) {
      other.removeFromParent();
      if (gameSession != null) {
        health = health - gameSession.currentMissileFirePower;
      }
    } else if (other is PlayerSprite && (gameSession?.currentShieldState ?? false) && !type.isBoss) {
      health = 0;
    }

    if (health <= 0 && _alive) {
      game.findByKeyName<PowerUpGenerator>(GameConstants.powerUpGenerator)?.generatePowerUp(position);
      death();
    }
  }

  void death() {
    if (!_alive) return;

    _alive = false;

    final gameView = game.findByKeyName(GameConstants.gameView)!;

    removeFromParent();
    gameView.add(ExplosionSprite(position: position, type: type));
    game.gameSession!.addScore(type.score);

    _enemiesKilled++;
    if (_enemiesKilled % 5 == 0) {
      game.gameSession!.addMultiplication(1);
    }

    game.findByKeyName<GameLevelController>(GameConstants.levelController)!.addDestroyedEnemy();

    if (_player == null) return;

    final rand = Random();

    for (var rank = 0; rank < GameConstants.bonusPointsNumber; rank++) {
      final xSize = (size.x / 2).toInt();
      final ySize = (size.y / 2).toInt();
      final x = position.x + rand.nextInt(xSize) - xSize / 2;
      final y = position.y + rand.nextInt(ySize) - ySize / 2;

      gameView.add(
        BonusPointsSprite(
          target: _player!,
          score: type.score ~/ GameConstants.bonusPointsNumber,
          position: Vector2(x, y),
        ),
      );
    }
  }
}
