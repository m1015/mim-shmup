import 'package:flame/components.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';

class EnemyEnginePowerSprite extends SpriteAnimationComponent with HasGameRef<MiMGame> {
  final EnemyTypes type;

  EnemyEnginePowerSprite({
    super.key,
    super.size,
    required this.type,
  });

  @override
  Future<void> onLoad() async {
    var (sprite, number) = type.getEngineSprite();

    animation = await game.loadSpriteAnimation(
      sprite.filename,
      SpriteAnimationData.variable(
        amount: number,
        amountPerRow: 1,
        textureSize: type.textureSize,
        stepTimes: List.filled(number, 0.15),
      ),
    );

    super.onLoad();
  }
}
