import 'package:flame/components.dart';
import 'package:flame/sprite.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/satellite_types.enum.dart';
import 'package:mim_shmup/core/extensions/game.extension.dart';
import 'package:mim_shmup/game/components/generators/satellite_bullet_generator.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class SatelliteSprite extends SpriteAnimationComponent with HasGameReference<MiMGame> {
  SatelliteSprite({
    required this.playerRef,
    super.priority,
    super.key,
    required this.type,
  }) : super(anchor: Anchor.center);

  final WeakReference<PlayerSprite> playerRef;
  final SatelliteTypesEnum type;

  @override
  Future<void> onLoad() async {
    size = MiMSizes.getSatelliteSize(game.gameViewSize);
    position = MiMSizes.getSatellitePosition(game.gameViewSize, type);

    final spriteSheet = SpriteSheet.fromColumnsAndRows(
      image: game.imageFromCache(Assets.images.sprites.player.satellite),
      rows: 1,
      columns: 4,
    );
    animation = spriteSheet.createAnimation(
      row: 0,
      stepTime: 0.15,
      from: 0,
      to: 4,
      loop: true,
    );

    add(SatelliteBulletGenerator(
      playerRef: playerRef,
      source: WeakReference(this),
      type: type,
    ));

    super.onLoad();
  }
}
