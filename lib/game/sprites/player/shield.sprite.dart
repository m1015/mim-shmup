import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class ShieldSprite extends SpriteAnimationComponent with HasGameRef<MiMGame> {
  ShieldSprite({
    super.key,
    super.size,
  });

  late ComponentsNotifier<GameSessionController> _sessionNotifier;

  @override
  Future<void> onLoad() async {
    animation = await game.loadSpriteAnimation(
      Assets.images.sprites.player.shipShield.filename,
      SpriteAnimationData.variable(
        amount: 10,
        textureSize: MiMSizes.playerTextureSize,
        stepTimes: List.filled(10, 0.1),
      ),
    );

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          if (!session.currentShieldState) {
            removeFromParent();
          }
        }
      });

    super.onLoad();
  }
}
