import 'package:flame/components.dart';
import 'package:mim_shmup/core/extensions/game.extension.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class MainWeaponSprite extends SpriteComponent with HasGameRef<MiMGame> {
  MainWeaponSprite({
    super.key,
    super.size,
  });

  late ComponentsNotifier<GameSessionController> _sessionNotifier;

  @override
  Future<void> onLoad() async {
    sprite = Sprite(game.imageFromCache(Assets.images.sprites.weapons.mainWeapon));

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          opacity = session.currentInvincibleState ? 0.5 : 1.0;
        }
      });

    super.onLoad();
  }
}
