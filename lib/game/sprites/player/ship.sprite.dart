import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/extensions/game.extension.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class ShipSprite extends SpriteComponent with HasGameRef<MiMGame> {
  ShipSprite({
    super.key,
    super.size,
  });

  late ComponentsNotifier<GameSessionController> _sessionNotifier;

  @override
  Future<void> onLoad() async {
    sprite = Sprite(game.imageFromCache(Assets.images.sprites.player.shipFullHealth));

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          sprite = switch (session.currentLifePoint) {
            GameConstants.baseLifePoint =>
              Sprite(game.imageFromCache(Assets.images.sprites.player.shipFullHealth)),
            2 => Sprite(game.imageFromCache(Assets.images.sprites.player.shipSlightDamage)),
            1 => Sprite(game.imageFromCache(Assets.images.sprites.player.shipDamaged)),
            _ => Sprite(game.imageFromCache(Assets.images.sprites.player.shipVeryDamaged)),
          };
          opacity = session.currentInvincibleState ? 0.5 : 1.0;
        }
      });
    super.onLoad();
  }
}
