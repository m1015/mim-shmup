import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class EnginePowerSprite extends SpriteAnimationComponent with HasGameRef<MiMGame> {
  EnginePowerSprite({
    super.key,
    super.size,
  });

  late ComponentsNotifier<GameSessionController> _sessionNotifier;

  @override
  Future<void> onLoad() async {
    animation = await game.loadSpriteAnimation(
      Assets.images.sprites.engines.engineSuperchargedEffect.filename,
      SpriteAnimationData.variable(
        amount: 4,
        textureSize: MiMSizes.playerTextureSize,
        stepTimes: List.filled(4, 0.15),
      ),
    );

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          opacity = session.currentInvincibleState ? 0.5 : 1.0;
        }
      });
    super.onLoad();
  }
}
