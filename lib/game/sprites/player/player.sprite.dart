import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/rendering.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/power_ups.enum.dart';
import 'package:mim_shmup/core/enums/satellite_types.enum.dart';
import 'package:mim_shmup/core/extensions/game.extension.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/components/generators/player_bullet_generator.component.dart';
import 'package:mim_shmup/game/components/nuke.component.dart';
import 'package:mim_shmup/game/controllers/event_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/controllers/missile_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/life_bar.sprite.dart';
import 'package:mim_shmup/game/sprites/player/engine_power.sprite.dart';
import 'package:mim_shmup/game/sprites/player/main_weapon.sprite.dart';
import 'package:mim_shmup/game/sprites/player/rocket_weapons.sprite.dart';
import 'package:mim_shmup/game/sprites/player/shield.sprite.dart';
import 'package:mim_shmup/game/sprites/player/ship.sprite.dart';
import 'package:mim_shmup/game/sprites/power_up.sprite.dart';
import 'package:mim_shmup/game/sprites/satellite.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class PlayerSprite extends SpriteComponent with HasGameReference<MiMGame>, CollisionCallbacks {
  PlayerSprite({
    super.position,
    super.priority,
  }) : super(key: ComponentKey.named(GameConstants.player), anchor: Anchor.center);

  PlayerBulletGenerator? _bulletCreator;

  bool _satelliteAdded = false;
  bool _isInvincible = false;
  bool _shieldActivated = false;

  late Vector2 _hitBoxSize;
  late ComponentsNotifier<GameSessionController> _sessionNotifier;
  late Rotate3DDecorator _decorator;

  @override
  Future<void> onLoad() async {
    size = MiMSizes.getPlayerSize(game.gameViewSize);
    _hitBoxSize = MiMSizes.getPlayerHitBoxSize(game.gameViewSize);
    sprite = Sprite(game.imageFromCache(Assets.images.sprites.engines.engineSupercharged));

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null) {
          _isInvincible = session.currentInvincibleState;
          opacity = _isInvincible ? 0.5 : 1.0;

          if (session.currentSatelliteLevel >= 1 && !_satelliteAdded) {
            _satelliteAdded = true;
            _addSatellites();
          }
          if (session.currentShieldState && !_shieldActivated) {
            _shieldActivated = true;
            add(ShieldSprite(size: size));
          } else if (!session.currentShieldState && _shieldActivated) {
            _shieldActivated = false;
          }
        }
      });

    /// Adding custom hit box
    add(PolygonHitbox.relative(
      _buildRelativeHitBox(),
      anchor: Anchor.center,
      position: size / 2,
      parentSize: size,
    ));
    add(RocketWeaponSprite(size: size));

    /// Adding weapons, ship and engine power sprites
    add(MainWeaponSprite(size: size));
    add(ShipSprite(size: size));
    add(EnginePowerSprite(size: size));

    /// Adding life bar component
    add(LifeBarSprite(
      position: Vector2(
        -size.x * 0.2,
        (size.y - _hitBoxSize.y) / 2,
      ),
    ));

    /// Adding rotation effect when going up/down
    _decorator = Rotate3DDecorator(
      center: Vector2.all(55),
      perspective: 0.002,
    );
    decorator.addLast(_decorator);

    super.onLoad();
  }

  @override
  void update(double dt) {
    Vector2 direction =
        game.findByKeyName<EventController>(GameConstants.eventController)!.getControllerDirection().normalized();

    _decorator.angleX = direction.y * 0.4;

    _move(dt, direction);

    super.update(dt);
  }

  /// This methods return relative position of points needed to create a polygonal hit box
  List<Vector2> _buildRelativeHitBox() {
    Vector2 ratio = Vector2(_hitBoxSize.x / size.x, _hitBoxSize.y / size.y);
    return [
      // Top left
      Vector2(-ratio.x, ratio.y),
      // Top left after wing
      Vector2(0, ratio.y),
      // Head of the ship
      Vector2(ratio.x, 0),
      // Bottom left before wing
      Vector2(0, -ratio.y),
      // Bottom left
      Vector2(-ratio.x, -ratio.y),
    ];
  }

  void _addSatellites() {
    addAll([
      SatelliteSprite(
        key: ComponentKey.named(GameConstants.satelliteTop),
        type: SatelliteTypesEnum.top,
        playerRef: WeakReference(this),
      ),
      SatelliteSprite(
        key: ComponentKey.named(GameConstants.satelliteBottom),
        type: SatelliteTypesEnum.bottom,
        playerRef: WeakReference(this),
      )
    ]);
  }

  void _move(double dt, Vector2 direction) {
    /// Get position displacement based on event, speed and time elapsed
    final displacement =
        direction * (GameConstants.basePlayerSpeed * game.gameViewSize.y / GameConstants.baseScreenHeight) * dt;

    /// Calculate the new x y
    double newX = position.x + displacement.x;
    double newY = position.y + displacement.y;

    /// Get half size to match player hit box
    Vector2 playerHalfSize = size / 2;

    final portraitOrientation = game.portraitOrientation;

    final xMinOffset = portraitOrientation ? (MiMSizes.joystickBackgroundRadius + MiMSizes.joystickPadding) * 2 : 0;
    final xMaxOffset = game.gameViewSize.x - (portraitOrientation ? game.hudHeight : 0);
    final yOffset = portraitOrientation ? 0 : game.hudHeight;

    /// Avoid player to get out off game screen
    if (game.isPlaying) {
      if (newX - playerHalfSize.x < xMinOffset) newX = playerHalfSize.x + xMinOffset;
      if (newX + playerHalfSize.x > xMaxOffset) newX = xMaxOffset - playerHalfSize.x;
      if (newY - playerHalfSize.y < yOffset) newY = yOffset + playerHalfSize.y;
      if (newY + playerHalfSize.y > game.gameViewSize.y) newY = game.gameViewSize.y - playerHalfSize.y;
    }

    /// Set the new position
    position = Vector2(newX, newY);
  }

  void setFiring(bool firing) {
    if (!firing && _bulletCreator != null) {
      remove(_bulletCreator!);
      _bulletCreator = null;
    }
    if (firing && _bulletCreator == null) {
      _bulletCreator = PlayerBulletGenerator();
      add(_bulletCreator!);

      _bulletCreator!.setFiring(
        firing: firing,
        source: this,
      );
    }
  }

  @override
  void onCollisionStart(Set<Vector2> intersectionPoints, PositionComponent other) {
    final particleGenerator = game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!;

    if (other is PowerUpSprite) {
      other.removeFromParent();
      switch (other.type) {
        case PowerUps.power:
          game.gameSession!.levelUpFirePower();
          break;
        case PowerUps.nuke:
          game.findByKeyName(GameConstants.gameView)!.add(Nuke()..position = position);
          break;
        case PowerUps.multiFire:
          game.gameSession!.levelUpMultiFire();
          break;
        case PowerUps.satellite:
          game.gameSession!.levelUpSatellite();
          break;
        case PowerUps.life:
          game.gameSession!.addLifeModifier(1);
          break;
        case PowerUps.shield:
          game.gameSession!.activateShield();
          break;
        case PowerUps.missile:
          game.findByKeyName<MissileController>(GameConstants.missileController)!.addModifier(0.2);
          break;
      }
    } else if (_isInvincible) {
      return;
    } else if (other is BulletSprite) {
      if (game.gameSession!.currentShieldState) {
        game.gameSession!.cancelShield();
      } else {
        particleGenerator.generateParticle(
          color: Colors.orange.shade100,
          position: position.clone(),
        );

        game.gameSession!.addLifeModifier(-1);

        game.findByKeyName<GameLevelController>(GameConstants.levelController)!.setHasBeenShot();

        game.gameSession!.resetMultiplication();
      }
    } else if (other is EnemySprite || other is AsteroidSprite) {
      if (game.gameSession!.currentShieldState) {
        game.gameSession!.cancelShield();
      } else {
        game.gameSession!.instantDeath();
      }
    }
    super.onCollisionStart(intersectionPoints, other);
  }
}
