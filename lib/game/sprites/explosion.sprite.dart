import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/enemy_types.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class ExplosionSprite<T extends FlameGame> extends SpriteAnimationComponent with HasGameReference<MiMGame> {
  final EnemyTypes? type;

  ExplosionSprite({
    super.position,
    this.type,
  }) : super(
          anchor: Anchor.center,
          removeOnFinish: true,
        );

  @mustCallSuper
  @override
  Future<void> onLoad() async {
    if (type == null) {
      final textureSize = Vector2(34, 35);
      final spriteHeight = game.gameViewSize.y * textureSize.y / GameConstants.baseScreenHeight;
      final spriteWidth = spriteHeight * textureSize.x / textureSize.y;

      size = Vector2(spriteWidth, spriteHeight);

      animation = await game.loadSpriteAnimation(
        Assets.images.sprites.explosion.filename,
        SpriteAnimationData.sequenced(
          amount: 7,
          textureSize: textureSize,
          stepTime: 0.12,
          loop: false,
        ),
      );
    } else {
      size = type!.getSize(game.gameViewSize);

      var (sprite, number) = type!.getExplosionSprite();

      animation = await game.loadSpriteAnimation(
        sprite.filename,
        SpriteAnimationData.sequenced(
          amount: number,
          amountPerRow: 1,
          textureSize: type!.textureSize,
          stepTime: 0.12,
          loop: false,
        ),
      );
    }
  }
}
