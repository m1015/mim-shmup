import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class BonusPointsSprite extends SpriteComponent with HasGameRef<MiMGame>, CollisionCallbacks {
  final PositionComponent target;
  final int score;

  Vector2 velocity = Vector2(-GameConstants.bonusPointsSpeed, 0);
  final deltaPosition = Vector2.zero();
  Vector2 direction = Vector2(1, 0);

  BonusPointsSprite({
    required this.target,
    required this.score,
    required super.position,
  }) : super(anchor: Anchor.center);

  @override
  Future<void> onLoad() async {
    size = MiMSizes.getBonusPointTextureSize(game.gameViewSize);

    sprite = Sprite(await gameRef.images.load(Assets.images.sprites.bonusPoints.filename));

    /// Adding a circular hitbox with radius as 1 times
    ///  the smallest dimension of this components size.
    final shape = CircleHitbox.relative(
      1,
      parentSize: size,
      position: size / 2,
      anchor: Anchor.center,
    );
    add(shape);

    return super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    if (target.isMounted && target.position.distanceTo(position) < GameConstants.minBonusPointsDistance) {
      velocity = Vector2(GameConstants.basePlayerSpeed, 0);

      direction = target.position - position;
      direction.normalize();
    }

    position += direction * velocity.x * dt;

    if (position.x + (size.x / 2) < 0) {
      removeFromParent();
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    if (other is PlayerSprite) {
      game.gameSession!.addScore(score);
      removeFromParent();
    }
  }
}
