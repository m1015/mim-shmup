import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/power_ups.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/mim_game.dart';

class PowerUpSprite extends SpriteAnimationComponent with HasGameRef<MiMGame> {
  final PowerUps type;

  PowerUpSprite({
    required super.position,
    required this.type,
  }) : super(anchor: Anchor.center);

  @override
  Future<void> onLoad() async {
    add(RectangleHitbox());

    if (!type.animated) {
      addAll(
        [
          ScaleEffect.to(
            Vector2.all(1.2),
            InfiniteEffectController(
              SequenceEffectController(
                [
                  CurvedEffectController(1, Curves.easeOut),
                  ReverseCurvedEffectController(1, Curves.easeOut),
                ],
              ),
            ),
          ),
          ColorEffect(
            type.color,
            InfiniteEffectController(
              SequenceEffectController(
                [
                  CurvedEffectController(1, Curves.easeOut),
                  ReverseCurvedEffectController(1, Curves.easeOut),
                ],
              ),
            ),
            opacityFrom: 0,
            opacityTo: 0.5,
          ),
        ],
      );
    }

    size = type.getSize(game.gameViewSize);

    var (sprite, number) = type.sprite;

    animation = await game.loadSpriteAnimation(
      sprite.filename,
      SpriteAnimationData.variable(
        amount: number,
        textureSize: type.textureSize,
        stepTimes: List.filled(number, 0.15),
      ),
    );
  }

  @override
  void update(double dt) {
    /// Calculate the new x position based on speed and time elapsed
    double newX = position.x - (GameConstants.powerUpSpeed * game.gameViewSize.y / GameConstants.baseScreenHeight) * dt;

    /// Set the new position
    position = Vector2(newX, position.y);

    /// Check if the enemy is out of the screen, and if so, remove it
    if (position.x < -size.x) {
      game.findByKeyName(GameConstants.gameView)!.remove(this);
    }

    super.update(dt);
  }

  @override
  void onRemove() {
    if (type == PowerUps.nuke) return;

    final particleGenerator = game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!;

    particleGenerator.generateParticle(
      color: type.color,
      position: position.clone(),
      count: 30,
      lifespan: 0.4,
      rnd: 250,
      radius: 3,
    );

    super.onRemove();
  }
}
