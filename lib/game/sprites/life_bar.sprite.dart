import 'package:flame/components.dart';
import 'package:flame/sprite.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class LifeBarSprite extends SpriteAnimationComponent with HasGameReference<MiMGame> {
  LifeBarSprite({
    super.position,
  });

  late SpriteSheet _spriteSheet;
  late ComponentsNotifier<GameSessionController> _sessionNotifier;
  int _lifePoint = GameConstants.baseLifePoint;

  @override
  Future<void> onLoad() async {
    size = MiMSizes.getLifeBarSize(game.gameViewSize);

    /// Create sprite sheet
    _spriteSheet = SpriteSheet.fromColumnsAndRows(
      image: await game.images.load(Assets.images.sprites.lifeBar.filename),
      rows: 2,
      columns: 10,
    );

    /// Create animation from sprite sheet
    /// From index to another index (excluded)
    animation = _spriteSheet.createAnimation(
      row: 0,
      stepTime: 0.2,
      from: 0,
      to: 1,
      loop: false,
    );

    /// Update sprite animation according to session notifier
    _sessionNotifier = game.componentsNotifier<GameSessionController>()
      ..addListener(() {
        final session = _sessionNotifier.single;
        if (session != null && session.currentLifePoint != _lifePoint) {
          _updateAnimation(
            decrease: session.currentLifePoint < _lifePoint,
            newLifePoint: session.currentLifePoint,
          );
        }
      });
  }

  int _getSpriteFromIndex(bool decrease, int newLifePoint) {
    switch (newLifePoint) {
      case 2:
        return decrease ? 0 : 3;
      case 1:
        return decrease ? 3 : 0;
      case 0:
        return decrease ? 6 : 0;
      default:
        return decrease ? 0 : 6;
    }
  }

  int _getSpriteToIndex(bool decrease, int newLifePoint) {
    switch (newLifePoint) {
      case 2:
        return decrease ? 4 : 7;
      case 1:
        return decrease ? 7 : 4;
      case 0:
        return decrease ? 10 : 1;
      default:
        return decrease ? 1 : 10;
    }
  }

  void _updateAnimation({
    bool decrease = false,
    required int newLifePoint,
  }) {
    animation = _spriteSheet.createAnimation(
      row: decrease ? 0 : 1,
      stepTime: 0.2,
      from: _getSpriteFromIndex(decrease, newLifePoint),
      to: _getSpriteToIndex(decrease, newLifePoint),
      loop: false,
    );
    _lifePoint = newLifePoint;
  }
}
