import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/enums/bullet_types.enum.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/boss.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class BulletSprite extends SpriteAnimationComponent with HasGameRef<MiMGame>, CollisionCallbacks {
  late final Vector2 velocity;
  final Vector2 deltaPosition = Vector2.zero();
  final BulletTypesEnum type;

  double _lifetime = 0;
  static const double laserLifetime = 2.0;

  double initialYOffset = 0;
  late double previousBossY;

  BulletSprite({
    this.type = BulletTypesEnum.bullet,
    required super.position,
    super.angle,
  }) : super(anchor: Anchor.center) {
    initialYOffset = 0;
    previousBossY = 0;
  }

  @override
  Future<void> onLoad() async {
    late String asset;

    if (type == BulletTypesEnum.satellite) {
      asset = Assets.images.lasers.laserSatellite.filename;
      animation = await gameRef.loadSpriteAnimation(
        asset,
        SpriteAnimationData.sequenced(
          amount: 1,
          stepTime: 0.1,
          textureSize: MiMSizes.laserSatelliteTextureSize,
        ),
      );
    } else if (type == BulletTypesEnum.laser) {
      asset = Assets.images.sprites.enemies.klaed.projectiles.ray.filename;
      animation = await gameRef.loadSpriteAnimation(
        asset,
        SpriteAnimationData.sequenced(
          amount: 4,
          amountPerRow: 1,
          textureSize: MiMSizes.klaedLaserTextureSize,
          stepTime: 0.20,
          loop: true,
        ),
      );
    } else {
      asset = Assets.images.lasers.laserRed.filename;
      animation = await gameRef.loadSpriteAnimation(
        asset,
        SpriteAnimationData.sequenced(
          amount: 1,
          stepTime: 1,
          textureSize: MiMSizes.laserRedTextureSize,
        ),
      );
      add(ColorEffect(
        turkishRed.withOpacity(0.5),
        EffectController(duration: 0.5, reverseDuration: 0.5, infinite: true),
      ));
    }

    size = type.getSize(game.gameViewSize);

    if (type.fromSatellite) {
      add(RotateEffect.by(2 * pi, EffectController(duration: 0.7, infinite: true)));
    }

    if (type == BulletTypesEnum.laser) {
      velocity = Vector2.zero();
    } else {
      velocity = Vector2(0, -1)
        ..rotate(angle)
        ..scale(GameConstants.bulletsSpeed * game.gameViewSize.y / GameConstants.baseScreenHeight);
    }
  }

  @override
  void onMount() {
    super.onMount();

    final shape = type == BulletTypesEnum.laser
        ? RectangleHitbox()
        : CircleHitbox.relative(
            1,
            parentSize: size,
            position: size / 2,
            anchor: Anchor.center,
          );

    add(shape);

    final boss = gameRef.findByKeyName<BossSprite>(GameConstants.boss);
    if (boss != null) {
      initialYOffset = position.y - boss.position.y;
      previousBossY = boss.position.y;
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    if (other is PlayerSprite || other is AsteroidSprite) {
      if (type != BulletTypesEnum.laser) {
        removeFromParent();
      }
    }
  }

  @override
  void update(double dt) {
    super.update(dt);

    final bossComponent = gameRef.findByKeyName<BossSprite>(GameConstants.boss);

    if (bossComponent != null && type == BulletTypesEnum.laser) {
      double bossYDifference = bossComponent.position.y - previousBossY;

      position.y += bossYDifference;

      previousBossY = bossComponent.position.y;

      _lifetime += dt;
      if (_lifetime >= laserLifetime) {
        removeFromParent();
      }
    } else if (type != BulletTypesEnum.laser) {
      deltaPosition
        ..setFrom(velocity)
        ..scale(dt);
      position += deltaPosition;
    }

    if (position.y < 0 || position.x > game.gameViewSize.x || position.x + size.x < 0) {
      removeFromParent();
    }
  }
}
