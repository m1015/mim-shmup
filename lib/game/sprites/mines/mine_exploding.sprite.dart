import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/asteroid.sprite.dart';
import 'package:mim_shmup/game/sprites/bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/enemies/enemy.sprite.dart';
import 'package:mim_shmup/game/sprites/mines/mine_idle.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class MineExplodingSprite extends SpriteAnimationComponent with HasGameRef<MiMGame>, CollisionCallbacks {
  Vector2 velocity = Vector2(-GameConstants.mineSpeed, 0);
  final deltaPosition = Vector2.zero();
  Vector2 direction = Vector2(1, 0);

  bool _hasTouched = false;

  MineExplodingSprite({
    super.position,
    super.priority,
  }) : super(anchor: Anchor.center);

  @override
  Future<void> onLoad() async {
    size = MiMSizes.getMineExplodingTextureSize(game.gameViewSize);

    animation = await game.loadSpriteAnimation(
      Assets.images.sprites.mine.mineExploding.filename,
      SpriteAnimationData.variable(
        amount: 9,
        textureSize: MiMSizes.mineExplodingTextureSize,
        stepTimes: List.filled(9, 0.2),
        loop: false,
      ),
    );

    animationTicker!.onComplete = () => removeFromParent();

    final shape = CircleHitbox.relative(
      1,
      parentSize: size,
      position: size / 2,
      anchor: Anchor.center,
    );

    add(shape);

    return super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    position += direction * velocity.x * dt;

    if (position.x + (size.x / 2) < 0) {
      removeFromParent();
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    if (other is PlayerSprite && !_hasTouched) {
      _hasTouched = true;
      if (game.gameSession!.currentShieldState) {
        game.gameSession!.cancelShield();
      } else {
        game.gameSession!.addLifeModifier(-2);

        game.findByKeyName<GameLevelController>(GameConstants.levelController)!.setHasBeenShot();

        game.gameSession!.resetMultiplication();
      }
    }
    if (other is EnemySprite) {
      other.death();
    }
    if (other is AsteroidSprite) {
      other.death();
    }
    if (other is BulletSprite) {
      other.removeFromParent();
    }
    if (other is MineIdleSprite) {
      other.death();
    }
  }
}
