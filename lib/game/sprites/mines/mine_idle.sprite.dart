import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/asset_gen_image.extension.dart';
import 'package:mim_shmup/game/components/generators/particle_generator.component.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_session_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/bullets/player_bullet.sprite.dart';
import 'package:mim_shmup/game/sprites/mines/mine_exploding.sprite.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';
import 'package:mim_shmup/gen/assets.gen.dart';

class MineIdleSprite extends SpriteAnimationComponent with HasGameRef<MiMGame>, CollisionCallbacks {
  Vector2 velocity = Vector2(-GameConstants.mineSpeed, 0);
  final deltaPosition = Vector2.zero();
  Vector2 direction = Vector2(1, 0);
  bool _alive = true;

  late double health;

  PositionComponent? target;
  DateTime? triggerTime;

  MineIdleSprite({
    super.position,
    super.priority,
  }) : super(anchor: Anchor.center);

  @override
  Future<void> onLoad() async {
    size = MiMSizes.getMineIdleTextureSize(game.gameViewSize);

    animation = await game.loadSpriteAnimation(
      Assets.images.sprites.mine.mineIdle.filename,
      SpriteAnimationData.variable(
        amount: 10,
        textureSize: MiMSizes.mineIdleTextureSize,
        stepTimes: List.filled(10, 0.2),
      ),
    );

    health = game.findByKeyName<GamePhaseController>(GameConstants.phaseController)!.getMineHealth();

    final shape = CircleHitbox.relative(
      1,
      parentSize: size,
      position: size / 2,
      anchor: Anchor.center,
    );

    add(shape);

    return super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);

    target ??= game.findByKeyName<PlayerSprite>(GameConstants.player);

    if (target != null &&
        target!.isMounted &&
        target!.position.distanceTo(position) < GameConstants.minMineDistance &&
        triggerTime == null) {
      triggerTime = DateTime.now();
      animation!.stepTime = 0.05;
    }

    if (triggerTime != null && DateTime.now().difference(triggerTime!) > const Duration(seconds: 2)) {
      death();
    }

    position += direction * velocity.x * dt;

    if (position.x + (size.x / 2) < 0) {
      removeFromParent();
    }
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollision(intersectionPoints, other);

    final GameSessionController? gameSession = game.gameSession;

    if (other is PlayerSprite) {
      death();
    } else if (other is PlayerBulletSprite) {
      if (gameSession != null) {
        game.findByKeyName<ParticleGenerator>(GameConstants.particleGenerator)!.generateParticle(
              color: gameSession.firePowerColor,
              position: position.clone(),
            );

        health = health - gameSession.currentFirePowerLevel;

        if (health < 0 && _alive) {
          death();
        }
      }
      other.removeFromParent();
    }
  }

  void death() {
    if (!_alive) return;

    _alive = false;

    game.findByKeyName(GameConstants.gameView)!.add(MineExplodingSprite(position: position));
    removeFromParent();
  }
}
