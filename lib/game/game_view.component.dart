import 'dart:math';

import 'package:flame/components.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/utils/database/settings.service.dart';
import 'package:mim_shmup/core/utils/di.dart';
import 'package:mim_shmup/core/utils/platform.utils.dart';
import 'package:mim_shmup/game/components/backgrounds/background.component.dart';
import 'package:mim_shmup/game/components/generators/planet_generator.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/sprites/player/player.sprite.dart';

class GameViewComponent extends PositionComponent with HasGameRef<MiMGame> {
  GameViewComponent() : super(key: ComponentKey.named(GameConstants.gameView));

  @override
  Future<void> onLoad() async {
    final settings = getIt<SettingsService>();

    settings.listenToBoolParameter(Settings.portraitOrientation).listen((value) => _setOrientation(value));

    _setOrientation(settings.getBoolParameters(Settings.portraitOrientation));

    size = game.gameViewSize;

    add(BackgroundComponent());
    add(PlanetGenerator());
    add(PlayerSprite(
      position: game.initialPlayerPosition,
    ));
  }

  void _setOrientation(bool? value) {
    final portrait = value ?? PlatformUtils.isMobile;
    anchor = portrait ? Anchor.topRight : Anchor.topLeft;
    angle = portrait ? 3 * pi / 2 : 0;
  }
}
