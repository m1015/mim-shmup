/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// Directory path: assets/images/asteroids
  $AssetsImagesAsteroidsGen get asteroids => const $AssetsImagesAsteroidsGen();

  /// Directory path: assets/images/backgrounds
  $AssetsImagesBackgroundsGen get backgrounds =>
      const $AssetsImagesBackgroundsGen();

  /// Directory path: assets/images/hud
  $AssetsImagesHudGen get hud => const $AssetsImagesHudGen();

  /// Directory path: assets/images/lasers
  $AssetsImagesLasersGen get lasers => const $AssetsImagesLasersGen();

  /// Directory path: assets/images/power_ups
  $AssetsImagesPowerUpsGen get powerUps => const $AssetsImagesPowerUpsGen();

  /// Directory path: assets/images/sprites
  $AssetsImagesSpritesGen get sprites => const $AssetsImagesSpritesGen();

  /// Directory path: assets/images/ui
  $AssetsImagesUiGen get ui => const $AssetsImagesUiGen();
}

class $AssetsImagesAsteroidsGen {
  const $AssetsImagesAsteroidsGen();

  /// File path: assets/images/asteroids/meteorbig1.png
  AssetGenImage get meteorbig1 =>
      const AssetGenImage('assets/images/asteroids/meteorbig1.png');

  /// File path: assets/images/asteroids/meteorbig2.png
  AssetGenImage get meteorbig2 =>
      const AssetGenImage('assets/images/asteroids/meteorbig2.png');

  /// File path: assets/images/asteroids/meteorbig3.png
  AssetGenImage get meteorbig3 =>
      const AssetGenImage('assets/images/asteroids/meteorbig3.png');

  /// File path: assets/images/asteroids/meteorbig4.png
  AssetGenImage get meteorbig4 =>
      const AssetGenImage('assets/images/asteroids/meteorbig4.png');

  /// File path: assets/images/asteroids/meteormed1.png
  AssetGenImage get meteormed1 =>
      const AssetGenImage('assets/images/asteroids/meteormed1.png');

  /// File path: assets/images/asteroids/meteormed2.png
  AssetGenImage get meteormed2 =>
      const AssetGenImage('assets/images/asteroids/meteormed2.png');

  /// File path: assets/images/asteroids/meteormed3.png
  AssetGenImage get meteormed3 =>
      const AssetGenImage('assets/images/asteroids/meteormed3.png');

  /// File path: assets/images/asteroids/meteormed4.png
  AssetGenImage get meteormed4 =>
      const AssetGenImage('assets/images/asteroids/meteormed4.png');

  /// File path: assets/images/asteroids/meteorsm1.png
  AssetGenImage get meteorsm1 =>
      const AssetGenImage('assets/images/asteroids/meteorsm1.png');

  /// File path: assets/images/asteroids/meteorsm2.png
  AssetGenImage get meteorsm2 =>
      const AssetGenImage('assets/images/asteroids/meteorsm2.png');

  /// File path: assets/images/asteroids/meteorsm3.png
  AssetGenImage get meteorsm3 =>
      const AssetGenImage('assets/images/asteroids/meteorsm3.png');

  /// File path: assets/images/asteroids/meteorsm4.png
  AssetGenImage get meteorsm4 =>
      const AssetGenImage('assets/images/asteroids/meteorsm4.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        meteorbig1,
        meteorbig2,
        meteorbig3,
        meteorbig4,
        meteormed1,
        meteormed2,
        meteormed3,
        meteormed4,
        meteorsm1,
        meteorsm2,
        meteorsm3,
        meteorsm4
      ];
}

class $AssetsImagesBackgroundsGen {
  const $AssetsImagesBackgroundsGen();

  /// File path: assets/images/backgrounds/farback.png
  AssetGenImage get farback =>
      const AssetGenImage('assets/images/backgrounds/farback.png');

  /// Directory path: assets/images/backgrounds/level1
  $AssetsImagesBackgroundsLevel1Gen get level1 =>
      const $AssetsImagesBackgroundsLevel1Gen();

  /// Directory path: assets/images/backgrounds/level2
  $AssetsImagesBackgroundsLevel2Gen get level2 =>
      const $AssetsImagesBackgroundsLevel2Gen();

  /// Directory path: assets/images/backgrounds/level3
  $AssetsImagesBackgroundsLevel3Gen get level3 =>
      const $AssetsImagesBackgroundsLevel3Gen();

  /// Directory path: assets/images/backgrounds/level4
  $AssetsImagesBackgroundsLevel4Gen get level4 =>
      const $AssetsImagesBackgroundsLevel4Gen();

  /// Directory path: assets/images/backgrounds/level5
  $AssetsImagesBackgroundsLevel5Gen get level5 =>
      const $AssetsImagesBackgroundsLevel5Gen();

  /// Directory path: assets/images/backgrounds/level6
  $AssetsImagesBackgroundsLevel6Gen get level6 =>
      const $AssetsImagesBackgroundsLevel6Gen();

  /// Directory path: assets/images/backgrounds/planets
  $AssetsImagesBackgroundsPlanetsGen get planets =>
      const $AssetsImagesBackgroundsPlanetsGen();

  /// File path: assets/images/backgrounds/stars1.png
  AssetGenImage get stars1 =>
      const AssetGenImage('assets/images/backgrounds/stars1.png');

  /// File path: assets/images/backgrounds/stars2.png
  AssetGenImage get stars2 =>
      const AssetGenImage('assets/images/backgrounds/stars2.png');

  /// File path: assets/images/backgrounds/void.background.png
  AssetGenImage get voidBackground =>
      const AssetGenImage('assets/images/backgrounds/void.background.png');

  /// List of all assets
  List<AssetGenImage> get values => [farback, stars1, stars2, voidBackground];
}

class $AssetsImagesHudGen {
  const $AssetsImagesHudGen();

  /// File path: assets/images/hud/hud_item_empty.png
  AssetGenImage get hudItemEmpty =>
      const AssetGenImage('assets/images/hud/hud_item_empty.png');

  /// File path: assets/images/hud/hud_main_weapon.png
  AssetGenImage get hudMainWeapon =>
      const AssetGenImage('assets/images/hud/hud_main_weapon.png');

  /// File path: assets/images/hud/hud_multifire.png
  AssetGenImage get hudMultifire =>
      const AssetGenImage('assets/images/hud/hud_multifire.png');

  /// File path: assets/images/hud/hud_satellite.png
  AssetGenImage get hudSatellite =>
      const AssetGenImage('assets/images/hud/hud_satellite.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [hudItemEmpty, hudMainWeapon, hudMultifire, hudSatellite];
}

class $AssetsImagesLasersGen {
  const $AssetsImagesLasersGen();

  /// File path: assets/images/lasers/laserBlue.png
  AssetGenImage get laserBlue =>
      const AssetGenImage('assets/images/lasers/laserBlue.png');

  /// File path: assets/images/lasers/laserGreen.png
  AssetGenImage get laserGreen =>
      const AssetGenImage('assets/images/lasers/laserGreen.png');

  /// File path: assets/images/lasers/laserRed.png
  AssetGenImage get laserRed =>
      const AssetGenImage('assets/images/lasers/laserRed.png');

  /// File path: assets/images/lasers/laserSatellite.png
  AssetGenImage get laserSatellite =>
      const AssetGenImage('assets/images/lasers/laserSatellite.png');

  /// File path: assets/images/lasers/laserViolet.png
  AssetGenImage get laserViolet =>
      const AssetGenImage('assets/images/lasers/laserViolet.png');

  /// File path: assets/images/lasers/laserWhite.png
  AssetGenImage get laserWhite =>
      const AssetGenImage('assets/images/lasers/laserWhite.png');

  /// File path: assets/images/lasers/laserYellow.png
  AssetGenImage get laserYellow =>
      const AssetGenImage('assets/images/lasers/laserYellow.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        laserBlue,
        laserGreen,
        laserRed,
        laserSatellite,
        laserViolet,
        laserWhite,
        laserYellow
      ];
}

class $AssetsImagesPowerUpsGen {
  const $AssetsImagesPowerUpsGen();

  /// File path: assets/images/power_ups/heal_up.png
  AssetGenImage get healUp =>
      const AssetGenImage('assets/images/power_ups/heal_up.png');

  /// File path: assets/images/power_ups/powerup_missile.png
  AssetGenImage get powerupMissile =>
      const AssetGenImage('assets/images/power_ups/powerup_missile.png');

  /// File path: assets/images/power_ups/powerup_multi_fire.png
  AssetGenImage get powerupMultiFire =>
      const AssetGenImage('assets/images/power_ups/powerup_multi_fire.png');

  /// File path: assets/images/power_ups/powerup_nuke.png
  AssetGenImage get powerupNuke =>
      const AssetGenImage('assets/images/power_ups/powerup_nuke.png');

  /// File path: assets/images/power_ups/powerup_power.png
  AssetGenImage get powerupPower =>
      const AssetGenImage('assets/images/power_ups/powerup_power.png');

  /// File path: assets/images/power_ups/powerup_satelite.png
  AssetGenImage get powerupSatelite =>
      const AssetGenImage('assets/images/power_ups/powerup_satelite.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        healUp,
        powerupMissile,
        powerupMultiFire,
        powerupNuke,
        powerupPower,
        powerupSatelite
      ];
}

class $AssetsImagesSpritesGen {
  const $AssetsImagesSpritesGen();

  /// File path: assets/images/sprites/bonus_points.png
  AssetGenImage get bonusPoints =>
      const AssetGenImage('assets/images/sprites/bonus_points.png');

  /// Directory path: assets/images/sprites/bullets
  $AssetsImagesSpritesBulletsGen get bullets =>
      const $AssetsImagesSpritesBulletsGen();

  /// File path: assets/images/sprites/death.png
  AssetGenImage get death =>
      const AssetGenImage('assets/images/sprites/death.png');

  /// Directory path: assets/images/sprites/enemies
  $AssetsImagesSpritesEnemiesGen get enemies =>
      const $AssetsImagesSpritesEnemiesGen();

  /// Directory path: assets/images/sprites/engines
  $AssetsImagesSpritesEnginesGen get engines =>
      const $AssetsImagesSpritesEnginesGen();

  /// File path: assets/images/sprites/explosion.png
  AssetGenImage get explosion =>
      const AssetGenImage('assets/images/sprites/explosion.png');

  /// Directory path: assets/images/sprites/hud
  $AssetsImagesSpritesHudGen get hud => const $AssetsImagesSpritesHudGen();

  /// File path: assets/images/sprites/life_bar.png
  AssetGenImage get lifeBar =>
      const AssetGenImage('assets/images/sprites/life_bar.png');

  /// Directory path: assets/images/sprites/mine
  $AssetsImagesSpritesMineGen get mine => const $AssetsImagesSpritesMineGen();

  /// Directory path: assets/images/sprites/player
  $AssetsImagesSpritesPlayerGen get player =>
      const $AssetsImagesSpritesPlayerGen();

  /// Directory path: assets/images/sprites/weapons
  $AssetsImagesSpritesWeaponsGen get weapons =>
      const $AssetsImagesSpritesWeaponsGen();

  /// List of all assets
  List<AssetGenImage> get values => [bonusPoints, death, explosion, lifeBar];
}

class $AssetsImagesUiGen {
  const $AssetsImagesUiGen();

  /// File path: assets/images/ui/pause.png
  AssetGenImage get pause => const AssetGenImage('assets/images/ui/pause.png');

  /// List of all assets
  List<AssetGenImage> get values => [pause];
}

class $AssetsImagesBackgroundsLevel1Gen {
  const $AssetsImagesBackgroundsLevel1Gen();

  /// File path: assets/images/backgrounds/level1/farback.png
  AssetGenImage get farback =>
      const AssetGenImage('assets/images/backgrounds/level1/farback.png');

  /// List of all assets
  List<AssetGenImage> get values => [farback];
}

class $AssetsImagesBackgroundsLevel2Gen {
  const $AssetsImagesBackgroundsLevel2Gen();

  /// File path: assets/images/backgrounds/level2/farback.png
  AssetGenImage get farback =>
      const AssetGenImage('assets/images/backgrounds/level2/farback.png');

  /// List of all assets
  List<AssetGenImage> get values => [farback];
}

class $AssetsImagesBackgroundsLevel3Gen {
  const $AssetsImagesBackgroundsLevel3Gen();

  /// File path: assets/images/backgrounds/level3/farback.png
  AssetGenImage get farback =>
      const AssetGenImage('assets/images/backgrounds/level3/farback.png');

  /// List of all assets
  List<AssetGenImage> get values => [farback];
}

class $AssetsImagesBackgroundsLevel4Gen {
  const $AssetsImagesBackgroundsLevel4Gen();

  /// File path: assets/images/backgrounds/level4/farback.png
  AssetGenImage get farback =>
      const AssetGenImage('assets/images/backgrounds/level4/farback.png');

  /// List of all assets
  List<AssetGenImage> get values => [farback];
}

class $AssetsImagesBackgroundsLevel5Gen {
  const $AssetsImagesBackgroundsLevel5Gen();

  /// File path: assets/images/backgrounds/level5/farback.png
  AssetGenImage get farback =>
      const AssetGenImage('assets/images/backgrounds/level5/farback.png');

  /// List of all assets
  List<AssetGenImage> get values => [farback];
}

class $AssetsImagesBackgroundsLevel6Gen {
  const $AssetsImagesBackgroundsLevel6Gen();

  /// File path: assets/images/backgrounds/level6/farback.png
  AssetGenImage get farback =>
      const AssetGenImage('assets/images/backgrounds/level6/farback.png');

  /// List of all assets
  List<AssetGenImage> get values => [farback];
}

class $AssetsImagesBackgroundsPlanetsGen {
  const $AssetsImagesBackgroundsPlanetsGen();

  /// File path: assets/images/backgrounds/planets/planet1.png
  AssetGenImage get planet1 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet1.png');

  /// File path: assets/images/backgrounds/planets/planet10.png
  AssetGenImage get planet10 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet10.png');

  /// File path: assets/images/backgrounds/planets/planet11.png
  AssetGenImage get planet11 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet11.png');

  /// File path: assets/images/backgrounds/planets/planet12.png
  AssetGenImage get planet12 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet12.png');

  /// File path: assets/images/backgrounds/planets/planet13.png
  AssetGenImage get planet13 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet13.png');

  /// File path: assets/images/backgrounds/planets/planet14.png
  AssetGenImage get planet14 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet14.png');

  /// File path: assets/images/backgrounds/planets/planet15.png
  AssetGenImage get planet15 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet15.png');

  /// File path: assets/images/backgrounds/planets/planet16.png
  AssetGenImage get planet16 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet16.png');

  /// File path: assets/images/backgrounds/planets/planet17.png
  AssetGenImage get planet17 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet17.png');

  /// File path: assets/images/backgrounds/planets/planet18.png
  AssetGenImage get planet18 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet18.png');

  /// File path: assets/images/backgrounds/planets/planet19.png
  AssetGenImage get planet19 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet19.png');

  /// File path: assets/images/backgrounds/planets/planet2.png
  AssetGenImage get planet2 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet2.png');

  /// File path: assets/images/backgrounds/planets/planet20.png
  AssetGenImage get planet20 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet20.png');

  /// File path: assets/images/backgrounds/planets/planet3.png
  AssetGenImage get planet3 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet3.png');

  /// File path: assets/images/backgrounds/planets/planet4.png
  AssetGenImage get planet4 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet4.png');

  /// File path: assets/images/backgrounds/planets/planet5.png
  AssetGenImage get planet5 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet5.png');

  /// File path: assets/images/backgrounds/planets/planet6.png
  AssetGenImage get planet6 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet6.png');

  /// File path: assets/images/backgrounds/planets/planet7.png
  AssetGenImage get planet7 =>
      const AssetGenImage('assets/images/backgrounds/planets/planet7.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        planet1,
        planet10,
        planet11,
        planet12,
        planet13,
        planet14,
        planet15,
        planet16,
        planet17,
        planet18,
        planet19,
        planet2,
        planet20,
        planet3,
        planet4,
        planet5,
        planet6,
        planet7
      ];
}

class $AssetsImagesSpritesBulletsGen {
  const $AssetsImagesSpritesBulletsGen();

  /// File path: assets/images/sprites/bullets/main.bullet.png
  AssetGenImage get mainBullet =>
      const AssetGenImage('assets/images/sprites/bullets/main.bullet.png');

  /// File path: assets/images/sprites/bullets/missile.sprite.png
  AssetGenImage get missileSprite =>
      const AssetGenImage('assets/images/sprites/bullets/missile.sprite.png');

  /// File path: assets/images/sprites/bullets/satellite.bullet.png
  AssetGenImage get satelliteBullet =>
      const AssetGenImage('assets/images/sprites/bullets/satellite.bullet.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [mainBullet, missileSprite, satelliteBullet];
}

class $AssetsImagesSpritesEnemiesGen {
  const $AssetsImagesSpritesEnemiesGen();

  /// Directory path: assets/images/sprites/enemies/klaed
  $AssetsImagesSpritesEnemiesKlaedGen get klaed =>
      const $AssetsImagesSpritesEnemiesKlaedGen();

  /// Directory path: assets/images/sprites/enemies/nairan
  $AssetsImagesSpritesEnemiesNairanGen get nairan =>
      const $AssetsImagesSpritesEnemiesNairanGen();

  /// Directory path: assets/images/sprites/enemies/nautolan
  $AssetsImagesSpritesEnemiesNautolanGen get nautolan =>
      const $AssetsImagesSpritesEnemiesNautolanGen();
}

class $AssetsImagesSpritesEnginesGen {
  const $AssetsImagesSpritesEnginesGen();

  /// File path: assets/images/sprites/engines/engine_supercharged.png
  AssetGenImage get engineSupercharged => const AssetGenImage(
      'assets/images/sprites/engines/engine_supercharged.png');

  /// File path: assets/images/sprites/engines/engine_supercharged_effect.png
  AssetGenImage get engineSuperchargedEffect => const AssetGenImage(
      'assets/images/sprites/engines/engine_supercharged_effect.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [engineSupercharged, engineSuperchargedEffect];
}

class $AssetsImagesSpritesHudGen {
  const $AssetsImagesSpritesHudGen();

  /// File path: assets/images/sprites/hud/level_filling.png
  AssetGenImage get levelFilling =>
      const AssetGenImage('assets/images/sprites/hud/level_filling.png');

  /// List of all assets
  List<AssetGenImage> get values => [levelFilling];
}

class $AssetsImagesSpritesMineGen {
  const $AssetsImagesSpritesMineGen();

  /// File path: assets/images/sprites/mine/mine_exploding.png
  AssetGenImage get mineExploding =>
      const AssetGenImage('assets/images/sprites/mine/mine_exploding.png');

  /// File path: assets/images/sprites/mine/mine_idle.png
  AssetGenImage get mineIdle =>
      const AssetGenImage('assets/images/sprites/mine/mine_idle.png');

  /// List of all assets
  List<AssetGenImage> get values => [mineExploding, mineIdle];
}

class $AssetsImagesSpritesPlayerGen {
  const $AssetsImagesSpritesPlayerGen();

  /// File path: assets/images/sprites/player/satellite.png
  AssetGenImage get satellite =>
      const AssetGenImage('assets/images/sprites/player/satellite.png');

  /// File path: assets/images/sprites/player/ship_damaged.png
  AssetGenImage get shipDamaged =>
      const AssetGenImage('assets/images/sprites/player/ship_damaged.png');

  /// File path: assets/images/sprites/player/ship_full_health.png
  AssetGenImage get shipFullHealth =>
      const AssetGenImage('assets/images/sprites/player/ship_full_health.png');

  /// File path: assets/images/sprites/player/ship_shield.png
  AssetGenImage get shipShield =>
      const AssetGenImage('assets/images/sprites/player/ship_shield.png');

  /// File path: assets/images/sprites/player/ship_slight_damage.png
  AssetGenImage get shipSlightDamage => const AssetGenImage(
      'assets/images/sprites/player/ship_slight_damage.png');

  /// File path: assets/images/sprites/player/ship_very_damaged.png
  AssetGenImage get shipVeryDamaged =>
      const AssetGenImage('assets/images/sprites/player/ship_very_damaged.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        satellite,
        shipDamaged,
        shipFullHealth,
        shipShield,
        shipSlightDamage,
        shipVeryDamaged
      ];
}

class $AssetsImagesSpritesWeaponsGen {
  const $AssetsImagesSpritesWeaponsGen();

  /// File path: assets/images/sprites/weapons/main.weapon.png
  AssetGenImage get mainWeapon =>
      const AssetGenImage('assets/images/sprites/weapons/main.weapon.png');

  /// File path: assets/images/sprites/weapons/weapon.missile.sprite.png
  AssetGenImage get weaponMissileSprite => const AssetGenImage(
      'assets/images/sprites/weapons/weapon.missile.sprite.png');

  /// List of all assets
  List<AssetGenImage> get values => [mainWeapon, weaponMissileSprite];
}

class $AssetsImagesSpritesEnemiesKlaedGen {
  const $AssetsImagesSpritesEnemiesKlaedGen();

  /// Directory path: assets/images/sprites/enemies/klaed/destructions
  $AssetsImagesSpritesEnemiesKlaedDestructionsGen get destructions =>
      const $AssetsImagesSpritesEnemiesKlaedDestructionsGen();

  /// Directory path: assets/images/sprites/enemies/klaed/engines
  $AssetsImagesSpritesEnemiesKlaedEnginesGen get engines =>
      const $AssetsImagesSpritesEnemiesKlaedEnginesGen();

  /// Directory path: assets/images/sprites/enemies/klaed/projectiles
  $AssetsImagesSpritesEnemiesKlaedProjectilesGen get projectiles =>
      const $AssetsImagesSpritesEnemiesKlaedProjectilesGen();

  /// Directory path: assets/images/sprites/enemies/klaed/shields
  $AssetsImagesSpritesEnemiesKlaedShieldsGen get shields =>
      const $AssetsImagesSpritesEnemiesKlaedShieldsGen();

  /// Directory path: assets/images/sprites/enemies/klaed/ships
  $AssetsImagesSpritesEnemiesKlaedShipsGen get ships =>
      const $AssetsImagesSpritesEnemiesKlaedShipsGen();

  /// Directory path: assets/images/sprites/enemies/klaed/weapons
  $AssetsImagesSpritesEnemiesKlaedWeaponsGen get weapons =>
      const $AssetsImagesSpritesEnemiesKlaedWeaponsGen();
}

class $AssetsImagesSpritesEnemiesNairanGen {
  const $AssetsImagesSpritesEnemiesNairanGen();

  /// Directory path: assets/images/sprites/enemies/nairan/destructions
  $AssetsImagesSpritesEnemiesNairanDestructionsGen get destructions =>
      const $AssetsImagesSpritesEnemiesNairanDestructionsGen();

  /// Directory path: assets/images/sprites/enemies/nairan/engines
  $AssetsImagesSpritesEnemiesNairanEnginesGen get engines =>
      const $AssetsImagesSpritesEnemiesNairanEnginesGen();

  /// Directory path: assets/images/sprites/enemies/nairan/projectiles
  $AssetsImagesSpritesEnemiesNairanProjectilesGen get projectiles =>
      const $AssetsImagesSpritesEnemiesNairanProjectilesGen();

  /// Directory path: assets/images/sprites/enemies/nairan/shields
  $AssetsImagesSpritesEnemiesNairanShieldsGen get shields =>
      const $AssetsImagesSpritesEnemiesNairanShieldsGen();

  /// Directory path: assets/images/sprites/enemies/nairan/ships
  $AssetsImagesSpritesEnemiesNairanShipsGen get ships =>
      const $AssetsImagesSpritesEnemiesNairanShipsGen();

  /// Directory path: assets/images/sprites/enemies/nairan/weapons
  $AssetsImagesSpritesEnemiesNairanWeaponsGen get weapons =>
      const $AssetsImagesSpritesEnemiesNairanWeaponsGen();
}

class $AssetsImagesSpritesEnemiesNautolanGen {
  const $AssetsImagesSpritesEnemiesNautolanGen();

  /// Directory path: assets/images/sprites/enemies/nautolan/destructions
  $AssetsImagesSpritesEnemiesNautolanDestructionsGen get destructions =>
      const $AssetsImagesSpritesEnemiesNautolanDestructionsGen();

  /// Directory path: assets/images/sprites/enemies/nautolan/engines
  $AssetsImagesSpritesEnemiesNautolanEnginesGen get engines =>
      const $AssetsImagesSpritesEnemiesNautolanEnginesGen();

  /// Directory path: assets/images/sprites/enemies/nautolan/projectiles
  $AssetsImagesSpritesEnemiesNautolanProjectilesGen get projectiles =>
      const $AssetsImagesSpritesEnemiesNautolanProjectilesGen();

  /// Directory path: assets/images/sprites/enemies/nautolan/shields
  $AssetsImagesSpritesEnemiesNautolanShieldsGen get shields =>
      const $AssetsImagesSpritesEnemiesNautolanShieldsGen();

  /// Directory path: assets/images/sprites/enemies/nautolan/ships
  $AssetsImagesSpritesEnemiesNautolanShipsGen get ships =>
      const $AssetsImagesSpritesEnemiesNautolanShipsGen();

  /// Directory path: assets/images/sprites/enemies/nautolan/weapons
  $AssetsImagesSpritesEnemiesNautolanWeaponsGen get weapons =>
      const $AssetsImagesSpritesEnemiesNautolanWeaponsGen();
}

class $AssetsImagesSpritesEnemiesKlaedDestructionsGen {
  const $AssetsImagesSpritesEnemiesKlaedDestructionsGen();

  /// File path: assets/images/sprites/enemies/klaed/destructions/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/klaed/destructions/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/bomber.png');

  /// File path: assets/images/sprites/enemies/klaed/destructions/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/dreadnought.png');

  /// File path: assets/images/sprites/enemies/klaed/destructions/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/fighter.png');

  /// File path: assets/images/sprites/enemies/klaed/destructions/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/frigate.png');

  /// File path: assets/images/sprites/enemies/klaed/destructions/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/scout.png');

  /// File path: assets/images/sprites/enemies/klaed/destructions/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/support.png');

  /// File path: assets/images/sprites/enemies/klaed/destructions/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/destructions/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesKlaedEnginesGen {
  const $AssetsImagesSpritesEnemiesKlaedEnginesGen();

  /// File path: assets/images/sprites/enemies/klaed/engines/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/klaed/engines/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/bomber.png');

  /// File path: assets/images/sprites/enemies/klaed/engines/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/dreadnought.png');

  /// File path: assets/images/sprites/enemies/klaed/engines/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/fighter.png');

  /// File path: assets/images/sprites/enemies/klaed/engines/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/frigate.png');

  /// File path: assets/images/sprites/enemies/klaed/engines/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/scout.png');

  /// File path: assets/images/sprites/enemies/klaed/engines/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/support.png');

  /// File path: assets/images/sprites/enemies/klaed/engines/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/engines/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesKlaedProjectilesGen {
  const $AssetsImagesSpritesEnemiesKlaedProjectilesGen();

  /// File path: assets/images/sprites/enemies/klaed/projectiles/big_bullet.png
  AssetGenImage get bigBullet => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/projectiles/big_bullet.png');

  /// File path: assets/images/sprites/enemies/klaed/projectiles/bullet.png
  AssetGenImage get bullet => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/projectiles/bullet.png');

  /// File path: assets/images/sprites/enemies/klaed/projectiles/ray.png
  AssetGenImage get ray => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/projectiles/ray.png');

  /// File path: assets/images/sprites/enemies/klaed/projectiles/torpedo.png
  AssetGenImage get torpedo => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/projectiles/torpedo.png');

  /// File path: assets/images/sprites/enemies/klaed/projectiles/wave.png
  AssetGenImage get wave => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/projectiles/wave.png');

  /// List of all assets
  List<AssetGenImage> get values => [bigBullet, bullet, ray, torpedo, wave];
}

class $AssetsImagesSpritesEnemiesKlaedShieldsGen {
  const $AssetsImagesSpritesEnemiesKlaedShieldsGen();

  /// File path: assets/images/sprites/enemies/klaed/shields/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/shields/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/klaed/shields/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/shields/bomber.png');

  /// File path: assets/images/sprites/enemies/klaed/shields/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/shields/dreadnought.png');

  /// File path: assets/images/sprites/enemies/klaed/shields/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/shields/fighter.png');

  /// File path: assets/images/sprites/enemies/klaed/shields/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/shields/frigate.png');

  /// File path: assets/images/sprites/enemies/klaed/shields/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/shields/scout.png');

  /// File path: assets/images/sprites/enemies/klaed/shields/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/shields/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesKlaedShipsGen {
  const $AssetsImagesSpritesEnemiesKlaedShipsGen();

  /// File path: assets/images/sprites/enemies/klaed/ships/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/klaed/ships/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/bomber.png');

  /// File path: assets/images/sprites/enemies/klaed/ships/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/dreadnought.png');

  /// File path: assets/images/sprites/enemies/klaed/ships/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/fighter.png');

  /// File path: assets/images/sprites/enemies/klaed/ships/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/frigate.png');

  /// File path: assets/images/sprites/enemies/klaed/ships/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/scout.png');

  /// File path: assets/images/sprites/enemies/klaed/ships/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/support.png');

  /// File path: assets/images/sprites/enemies/klaed/ships/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/ships/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesKlaedWeaponsGen {
  const $AssetsImagesSpritesEnemiesKlaedWeaponsGen();

  /// File path: assets/images/sprites/enemies/klaed/weapons/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/weapons/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/klaed/weapons/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/weapons/dreadnought.png');

  /// File path: assets/images/sprites/enemies/klaed/weapons/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/weapons/fighter.png');

  /// File path: assets/images/sprites/enemies/klaed/weapons/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/weapons/frigate.png');

  /// File path: assets/images/sprites/enemies/klaed/weapons/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/weapons/scout.png');

  /// File path: assets/images/sprites/enemies/klaed/weapons/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/klaed/weapons/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [battlecruiser, dreadnought, fighter, frigate, scout, torpedoShip];
}

class $AssetsImagesSpritesEnemiesNairanDestructionsGen {
  const $AssetsImagesSpritesEnemiesNairanDestructionsGen();

  /// File path: assets/images/sprites/enemies/nairan/destructions/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nairan/destructions/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/bomber.png');

  /// File path: assets/images/sprites/enemies/nairan/destructions/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nairan/destructions/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/fighter.png');

  /// File path: assets/images/sprites/enemies/nairan/destructions/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/frigate.png');

  /// File path: assets/images/sprites/enemies/nairan/destructions/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/scout.png');

  /// File path: assets/images/sprites/enemies/nairan/destructions/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/support.png');

  /// File path: assets/images/sprites/enemies/nairan/destructions/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/destructions/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesNairanEnginesGen {
  const $AssetsImagesSpritesEnemiesNairanEnginesGen();

  /// File path: assets/images/sprites/enemies/nairan/engines/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nairan/engines/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/bomber.png');

  /// File path: assets/images/sprites/enemies/nairan/engines/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nairan/engines/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/fighter.png');

  /// File path: assets/images/sprites/enemies/nairan/engines/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/frigate.png');

  /// File path: assets/images/sprites/enemies/nairan/engines/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/scout.png');

  /// File path: assets/images/sprites/enemies/nairan/engines/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/support.png');

  /// File path: assets/images/sprites/enemies/nairan/engines/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/engines/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesNairanProjectilesGen {
  const $AssetsImagesSpritesEnemiesNairanProjectilesGen();

  /// File path: assets/images/sprites/enemies/nairan/projectiles/bolt.png
  AssetGenImage get bolt => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/projectiles/bolt.png');

  /// File path: assets/images/sprites/enemies/nairan/projectiles/ray.png
  AssetGenImage get ray => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/projectiles/ray.png');

  /// File path: assets/images/sprites/enemies/nairan/projectiles/rocket.png
  AssetGenImage get rocket => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/projectiles/rocket.png');

  /// File path: assets/images/sprites/enemies/nairan/projectiles/torpedo.png
  AssetGenImage get torpedo => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/projectiles/torpedo.png');

  /// List of all assets
  List<AssetGenImage> get values => [bolt, ray, rocket, torpedo];
}

class $AssetsImagesSpritesEnemiesNairanShieldsGen {
  const $AssetsImagesSpritesEnemiesNairanShieldsGen();

  /// File path: assets/images/sprites/enemies/nairan/shields/Nairan - Battlecruiser - Shield.png
  AssetGenImage get nairanBattlecruiserShield => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/shields/Nairan - Battlecruiser - Shield.png');

  /// File path: assets/images/sprites/enemies/nairan/shields/Nairan - Bomber - Shield.png
  AssetGenImage get nairanBomberShield => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/shields/Nairan - Bomber - Shield.png');

  /// File path: assets/images/sprites/enemies/nairan/shields/Nairan - Dreadnought - Shield.png
  AssetGenImage get nairanDreadnoughtShield => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/shields/Nairan - Dreadnought - Shield.png');

  /// File path: assets/images/sprites/enemies/nairan/shields/Nairan - Fighter - Shield.png
  AssetGenImage get nairanFighterShield => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/shields/Nairan - Fighter - Shield.png');

  /// File path: assets/images/sprites/enemies/nairan/shields/Nairan - Frigate - Shield.png
  AssetGenImage get nairanFrigateShield => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/shields/Nairan - Frigate - Shield.png');

  /// File path: assets/images/sprites/enemies/nairan/shields/Nairan - Scout - Shield.png
  AssetGenImage get nairanScoutShield => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/shields/Nairan - Scout - Shield.png');

  /// File path: assets/images/sprites/enemies/nairan/shields/Nairan - Torpedo Ship - Shield.png
  AssetGenImage get nairanTorpedoShipShield => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/shields/Nairan - Torpedo Ship - Shield.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        nairanBattlecruiserShield,
        nairanBomberShield,
        nairanDreadnoughtShield,
        nairanFighterShield,
        nairanFrigateShield,
        nairanScoutShield,
        nairanTorpedoShipShield
      ];
}

class $AssetsImagesSpritesEnemiesNairanShipsGen {
  const $AssetsImagesSpritesEnemiesNairanShipsGen();

  /// File path: assets/images/sprites/enemies/nairan/ships/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nairan/ships/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/bomber.png');

  /// File path: assets/images/sprites/enemies/nairan/ships/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nairan/ships/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/fighter.png');

  /// File path: assets/images/sprites/enemies/nairan/ships/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/frigate.png');

  /// File path: assets/images/sprites/enemies/nairan/ships/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/scout.png');

  /// File path: assets/images/sprites/enemies/nairan/ships/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/support.png');

  /// File path: assets/images/sprites/enemies/nairan/ships/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/ships/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesNairanWeaponsGen {
  const $AssetsImagesSpritesEnemiesNairanWeaponsGen();

  /// File path: assets/images/sprites/enemies/nairan/weapons/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/weapons/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nairan/weapons/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/weapons/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nairan/weapons/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/weapons/fighter.png');

  /// File path: assets/images/sprites/enemies/nairan/weapons/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/weapons/frigate.png');

  /// File path: assets/images/sprites/enemies/nairan/weapons/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/weapons/scout.png');

  /// File path: assets/images/sprites/enemies/nairan/weapons/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nairan/weapons/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [battlecruiser, dreadnought, fighter, frigate, scout, torpedoShip];
}

class $AssetsImagesSpritesEnemiesNautolanDestructionsGen {
  const $AssetsImagesSpritesEnemiesNautolanDestructionsGen();

  /// File path: assets/images/sprites/enemies/nautolan/destructions/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nautolan/destructions/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/bomber.png');

  /// File path: assets/images/sprites/enemies/nautolan/destructions/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nautolan/destructions/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/fighter.png');

  /// File path: assets/images/sprites/enemies/nautolan/destructions/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/frigate.png');

  /// File path: assets/images/sprites/enemies/nautolan/destructions/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/scout.png');

  /// File path: assets/images/sprites/enemies/nautolan/destructions/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/support.png');

  /// File path: assets/images/sprites/enemies/nautolan/destructions/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/destructions/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesNautolanEnginesGen {
  const $AssetsImagesSpritesEnemiesNautolanEnginesGen();

  /// File path: assets/images/sprites/enemies/nautolan/engines/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nautolan/engines/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/bomber.png');

  /// File path: assets/images/sprites/enemies/nautolan/engines/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nautolan/engines/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/fighter.png');

  /// File path: assets/images/sprites/enemies/nautolan/engines/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/frigate.png');

  /// File path: assets/images/sprites/enemies/nautolan/engines/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/scout.png');

  /// File path: assets/images/sprites/enemies/nautolan/engines/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/support.png');

  /// File path: assets/images/sprites/enemies/nautolan/engines/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/engines/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesNautolanProjectilesGen {
  const $AssetsImagesSpritesEnemiesNautolanProjectilesGen();

  /// File path: assets/images/sprites/enemies/nautolan/projectiles/bomb.png
  AssetGenImage get bomb => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/projectiles/bomb.png');

  /// File path: assets/images/sprites/enemies/nautolan/projectiles/bullet.png
  AssetGenImage get bullet => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/projectiles/bullet.png');

  /// File path: assets/images/sprites/enemies/nautolan/projectiles/ray.png
  AssetGenImage get ray => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/projectiles/ray.png');

  /// File path: assets/images/sprites/enemies/nautolan/projectiles/rocket.png
  AssetGenImage get rocket => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/projectiles/rocket.png');

  /// File path: assets/images/sprites/enemies/nautolan/projectiles/spinning_bullet.png
  AssetGenImage get spinningBullet => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/projectiles/spinning_bullet.png');

  /// File path: assets/images/sprites/enemies/nautolan/projectiles/wave.png
  AssetGenImage get wave => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/projectiles/wave.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [bomb, bullet, ray, rocket, spinningBullet, wave];
}

class $AssetsImagesSpritesEnemiesNautolanShieldsGen {
  const $AssetsImagesSpritesEnemiesNautolanShieldsGen();

  /// File path: assets/images/sprites/enemies/nautolan/shields/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/shields/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nautolan/shields/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/shields/bomber.png');

  /// File path: assets/images/sprites/enemies/nautolan/shields/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/shields/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nautolan/shields/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/shields/fighter.png');

  /// File path: assets/images/sprites/enemies/nautolan/shields/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/shields/frigate.png');

  /// File path: assets/images/sprites/enemies/nautolan/shields/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/shields/scout.png');

  /// File path: assets/images/sprites/enemies/nautolan/shields/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/shields/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesNautolanShipsGen {
  const $AssetsImagesSpritesEnemiesNautolanShipsGen();

  /// File path: assets/images/sprites/enemies/nautolan/ships/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nautolan/ships/bomber.png
  AssetGenImage get bomber => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/bomber.png');

  /// File path: assets/images/sprites/enemies/nautolan/ships/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nautolan/ships/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/fighter.png');

  /// File path: assets/images/sprites/enemies/nautolan/ships/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/frigate.png');

  /// File path: assets/images/sprites/enemies/nautolan/ships/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/scout.png');

  /// File path: assets/images/sprites/enemies/nautolan/ships/support.png
  AssetGenImage get support => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/support.png');

  /// File path: assets/images/sprites/enemies/nautolan/ships/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/ships/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        battlecruiser,
        bomber,
        dreadnought,
        fighter,
        frigate,
        scout,
        support,
        torpedoShip
      ];
}

class $AssetsImagesSpritesEnemiesNautolanWeaponsGen {
  const $AssetsImagesSpritesEnemiesNautolanWeaponsGen();

  /// File path: assets/images/sprites/enemies/nautolan/weapons/battlecruiser.png
  AssetGenImage get battlecruiser => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/weapons/battlecruiser.png');

  /// File path: assets/images/sprites/enemies/nautolan/weapons/dreadnought.png
  AssetGenImage get dreadnought => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/weapons/dreadnought.png');

  /// File path: assets/images/sprites/enemies/nautolan/weapons/fighter.png
  AssetGenImage get fighter => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/weapons/fighter.png');

  /// File path: assets/images/sprites/enemies/nautolan/weapons/frigate.png
  AssetGenImage get frigate => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/weapons/frigate.png');

  /// File path: assets/images/sprites/enemies/nautolan/weapons/scout.png
  AssetGenImage get scout => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/weapons/scout.png');

  /// File path: assets/images/sprites/enemies/nautolan/weapons/torpedo_ship.png
  AssetGenImage get torpedoShip => const AssetGenImage(
      'assets/images/sprites/enemies/nautolan/weapons/torpedo_ship.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [battlecruiser, dreadnought, fighter, frigate, scout, torpedoShip];
}

class Assets {
  Assets._();

  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(
    this._assetName, {
    this.size,
    this.flavors = const {},
  });

  final String _assetName;

  final Size? size;
  final Set<String> flavors;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
