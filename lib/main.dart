import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/theme/game_theme.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/widgets/game_container.dart';

import 'core/utils/di.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await getDependencies();

  await dotenv.load();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => GameStatusCubit(),
        ),
      ],
      child: const App(),
    ),
  );
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(GameConstants.baseScreenWidth, GameConstants.baseScreenHeight),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (_, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: GameTheme.gameTheme,
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          home: const GameContainer(),
        );
      },
    );
  }
}
