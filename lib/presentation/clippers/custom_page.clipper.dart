import 'package:flutter/material.dart';
import 'package:mim_shmup/core/utils/path.helper.dart';

class CustomPageClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) => PathHelper.getCustomPagePath(size);

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
