import 'package:flutter/material.dart';
import 'package:mim_shmup/core/utils/path.helper.dart';

class CustomButtonClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) => PathHelper.getCustomButtonPath(size);

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
