import 'package:flutter/material.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/presentation/clippers/custom_page.clipper.dart';
import 'package:mim_shmup/presentation/painters/custom_page.painter.dart';

class ClipperPageWidget extends StatelessWidget {
  const ClipperPageWidget({
    super.key,
    required this.child,
  });

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: CustomPagePainter(color: context.colorScheme.primary),
      child: ClipPath(
        clipper: CustomPageClipper(),
        child: child,
      ),
    );
  }
}
