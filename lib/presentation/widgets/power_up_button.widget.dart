import 'package:flutter/material.dart';
import 'package:mim_shmup/core/enums/power_ups.enum.dart';
import 'package:mim_shmup/presentation/widgets/custom_icon_button.widget.dart';

class PowerUpButton extends StatefulWidget {
  final PowerUps powerUp;
  final VoidCallback? onPressed;
  final bool autofocus;
  final Function(PowerUps) onFocusChanged;

  const PowerUpButton({
    super.key,
    required this.powerUp,
    this.onPressed,
    required this.autofocus,
    required this.onFocusChanged,
  });

  @override
  State<PowerUpButton> createState() => _PowerUpButtonState();
}

class _PowerUpButtonState extends State<PowerUpButton> {
  final _focusNode = FocusNode();

  @override
  void didUpdateWidget(covariant PowerUpButton oldWidget) {
    if (widget.autofocus) {
      _focusNode.requestFocus();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
      onFocusChange: (hasFocus) {
        if (hasFocus) {
          _focusNode.requestFocus();
          widget.onFocusChanged(widget.powerUp);
        }
      },
      child: CustomIconButtonWidget(
        focusNode: _focusNode,
        icon: widget.powerUp.asset,
        size: MediaQuery.of(context).size.height / 8,
        onPressed: widget.onPressed,
      ),
    );
  }
}
