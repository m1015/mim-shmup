import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/presentation/clippers/custom_button.clipper.dart';
import 'package:mim_shmup/presentation/painters/custom_button.painter.dart';

class CustomIconButtonWidget extends StatelessWidget {
  const CustomIconButtonWidget({
    super.key,
    required this.icon,
    required this.size,
    this.onPressed,
    this.focusNode,
  });

  final String icon;
  final double size;
  final VoidCallback? onPressed;
  final FocusNode? focusNode;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: CustomButtonPainter(color: context.colorScheme.surfaceVariant),
      child: ClipPath(
        clipper: CustomButtonClipper(),
        child: ElevatedButton(
          style: ButtonStyle(
            overlayColor: WidgetStateProperty.resolveWith<Color>(
              (Set<WidgetState> states) {
                if (states.contains(WidgetState.focused)) {
                  return focusedButton;
                }
                return unFocusedButton;
              },
            ),
          ),
          focusNode: focusNode,
          onPressed: onPressed,
          child: Padding(
            padding: EdgeInsets.all(size / 8),
            child: Image.asset(
              icon,
              width: size,
              height: size,
              fit: BoxFit.fill,
            ),
          ),
        ),
      ),
    );
  }
}
