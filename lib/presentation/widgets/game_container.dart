import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/enums/game_phase_types.enum.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/cubits/game_status.state.dart';

class GameContainer extends StatefulWidget {
  const GameContainer({super.key});

  @override
  State<GameContainer> createState() => _GameContainerState();
}

class _GameContainerState extends State<GameContainer> with WidgetsBindingObserver {
  GameWidget? _gameWidget;

  late MiMGame _game;

  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);

    WidgetsFlutterBinding.ensureInitialized().addPostFrameCallback((_) {
      /// This workaround prevent double onLoad call from game widget
      _game = MiMGame(
        context.read<GameStatusCubit>(),
        _focusNode,
        context.textTheme,
      );
      setState(() {
        _gameWidget = GameWidget(
          focusNode: _focusNode,
          game: _game,
        );
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _focusNode.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState appState) {
    final gameStatus = context.read<GameStatusCubit>();

    final isPlaying = (gameStatus.state is GameStatusStarted || gameStatus.state is GameStatusResumed) &&
        _game.findByKeyName<GamePhaseController>(GameConstants.phaseController)?.currentPhase.type !=
            GamePhaseTypes.rewards;

    switch (appState) {
      case AppLifecycleState.resumed:

        /// do nothing
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
      case AppLifecycleState.hidden:
        if (isPlaying) {
          context.read<GameStatusCubit>().pause();
          _game.router.pushNamed(Routes.pause);
        }
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<GameStatusCubit, GameStatusState>(
      listener: (context, state) {},
      child: Scaffold(
        body: Stack(
          children: [
            _gameWidget ?? const SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
