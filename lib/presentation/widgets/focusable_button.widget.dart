import 'package:flutter/material.dart';
import 'package:mim_shmup/presentation/widgets/custom_button.widget.dart';

class FocusableButton extends StatefulWidget {
  final Key buttonKey;
  final VoidCallback onPressed;
  final bool focused;
  final Function(Key) onFocusChanged;
  final String label;
  final ButtonSize? size;

  const FocusableButton({
    super.key,
    required this.buttonKey,
    required this.onPressed,
    required this.focused,
    required this.onFocusChanged,
    required this.label,
    this.size,
  });

  @override
  State<FocusableButton> createState() => _FocusableButtonState();
}

class _FocusableButtonState extends State<FocusableButton> {
  final _focusNode = FocusNode();

  @override
  void didUpdateWidget(covariant FocusableButton oldWidget) {
    if (widget.focused) {
      _focusNode.requestFocus();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
      onFocusChange: (hasFocus) {
        if (hasFocus) {
          _focusNode.requestFocus();
          widget.onFocusChanged(widget.buttonKey);
        }
      },
      child: CustomButtonWidget(
        key: widget.buttonKey,
        focusNode: _focusNode,
        label: widget.label,
        onPressed: widget.onPressed,
        size: widget.size ?? ButtonSize.normal,
      ),
    );
  }
}
