import 'package:flutter/material.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/presentation/widgets/clipper_page.widget.dart';

enum PageStatus { opening, opened }

class CustomAnimatedPage extends StatefulWidget {
  const CustomAnimatedPage({
    super.key,
    required this.child,
    this.controller,
    this.widthFactor,
    this.heightFactor,
    this.statusListener,
  });

  final Widget? child;
  final AnimationController? controller;
  final double? widthFactor;
  final double? heightFactor;
  final Function(PageStatus)? statusListener;

  @override
  State<CustomAnimatedPage> createState() => _CustomAnimatedPageState();
}

class _CustomAnimatedPageState extends State<CustomAnimatedPage> with SingleTickerProviderStateMixin {
  late Animation<double> _openAnimation;
  late AnimationController _openController;
  bool _showContent = false;

  @override
  void initState() {
    /// Init and run animation
    _setupAndStartAnimation();

    widget.statusListener?.call(PageStatus.opening);

    super.initState();
  }

  @override
  void dispose() {
    _openAnimation.removeListener(_openAnimationListener);
    if (widget.controller == null) _openController.dispose();
    super.dispose();
  }

  void _openAnimationListener() {
    if (_openAnimation.value > 0.7 && !_showContent) {
      setState(() {
        _showContent = true;
      });
    } else if (_openAnimation.value < 1.0 && _showContent) {
      setState(() {
        _showContent = false;
      });
    }
  }

  Future<void> _setupAndStartAnimation() async {
    _openController = widget.controller ??
        AnimationController(
          vsync: this,
          duration: const Duration(seconds: 2),
        );

    final Tween<double> offsetTween = Tween<double>(
      begin: 0.0,
      end: 1.0,
    );

    _openController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        widget.statusListener?.call(PageStatus.opened);
      }
    });

    /// Setting of the animation
    _openAnimation = offsetTween.animate(
      CurvedAnimation(
        parent: _openController,
        curve: Curves.linearToEaseOut,
      ),
    )..addListener(_openAnimationListener);

    if (mounted) {
      _openController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.colorScheme.background.withOpacity(0.5),
      body: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
        return Center(
          child: FadeTransition(
            opacity: _openAnimation,
            child: ClipperPageWidget(
              child: AnimatedBuilder(
                animation: _openAnimation,
                builder: (_, child) => ClipRect(
                  child: Align(
                    alignment: Alignment.center,
                    heightFactor: _openAnimation.value,
                    widthFactor: _openAnimation.value,
                    child: child,
                  ),
                ),
                child: Container(
                  color: context.colorScheme.surface.withOpacity(0.9),
                  width: constraints.maxWidth * (widget.widthFactor ?? 0.8),
                  height: constraints.maxHeight * (widget.heightFactor ?? 0.6),
                  child: AnimatedOpacity(
                    duration: const Duration(seconds: 1),
                    opacity: _showContent ? 1.0 : 0.0,
                    child: widget.child,
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
