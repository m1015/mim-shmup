import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/presentation/clippers/custom_button.clipper.dart';
import 'package:mim_shmup/presentation/painters/custom_button.painter.dart';

enum ButtonSize { normal, small }

class CustomButtonWidget extends StatelessWidget {
  const CustomButtonWidget({
    super.key,
    required this.label,
    required this.onPressed,
    this.size = ButtonSize.normal,
    this.focusNode,
  });

  final String label;
  final VoidCallback onPressed;
  final ButtonSize size;
  final FocusNode? focusNode;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: CustomButtonPainter(color: context.colorScheme.surfaceContainerHighest),
      child: ClipPath(
        clipper: CustomButtonClipper(),
        child: ElevatedButton(
          style: ButtonStyle(
            overlayColor: WidgetStateProperty.resolveWith<Color>(
              (Set<WidgetState> states) {
                if (states.contains(WidgetState.focused)) {
                  return focusedButton;
                }
                return unFocusedButton;
              },
            ),
          ),
          focusNode: focusNode,
          onPressed: onPressed,
          child: Text(
            label,
            style: size == ButtonSize.normal ? context.textTheme.displaySmall : context.textTheme.headlineSmall,
          ),
        ),
      ),
    );
  }
}
