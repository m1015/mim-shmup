import 'package:flutter/material.dart';
import 'package:mim_shmup/core/utils/path.helper.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButtonPainter extends CustomPainter {
  CustomButtonPainter({required this.color});

  final Color color;

  @override
  void paint(Canvas canvas, Size size) {
    final Path path = PathHelper.getCustomButtonPath(size);

    Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0.sp
      ..color = color;

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
