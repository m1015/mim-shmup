import 'package:flutter/material.dart';
import 'package:mim_shmup/core/utils/path.helper.dart';

class CustomPagePainter extends CustomPainter {
  CustomPagePainter({required this.color});

  final Color color;

  @override
  void paint(Canvas canvas, Size size) {
    final Path path = PathHelper.getCustomPagePath(size);

    Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5.0
      ..color = color;

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
