import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_shmup/presentation/cubits/game_status.state.dart';

class GameStatusCubit extends Cubit<GameStatusState> {
  GameStatusCubit() : super(GameStatusInitial());

  void reset() => emit(GameStatusInitial());

  void starting() => emit(GameStatusStarting());

  void start() => emit(GameStatusStarted());

  void pause() => emit(GameStatusPaused());

  void resume() => emit(GameStatusResumed());

  void end() => emit(GameStatusGameOver());
}
