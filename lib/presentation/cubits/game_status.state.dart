sealed class GameStatusState {}

final class GameStatusInitial extends GameStatusState {}

final class GameStatusStarting extends GameStatusState {}

final class GameStatusStarted extends GameStatusState {}

final class GameStatusPaused extends GameStatusState {}

final class GameStatusResumed extends GameStatusState {}

final class GameStatusGameOver extends GameStatusState {}

final class GameStatusLeaderboard extends GameStatusState {}
