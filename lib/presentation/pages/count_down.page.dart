import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/cubits/game_status.state.dart';

class CountDownPage extends StatefulWidget {
  final MiMGame game;

  const CountDownPage({
    super.key,
    required this.game,
  });

  @override
  State<CountDownPage> createState() => _CountDownPageState();
}

class _CountDownPageState extends State<CountDownPage> with SingleTickerProviderStateMixin {
  late Animation<double> _counterAnimation;
  late AnimationController _counterController;
  int _counter = 3;

  @override
  void initState() {
    /// Init and run animation
    _setupAndStartAnimation();
    super.initState();
  }

  @override
  void dispose() {
    _counterAnimation.removeStatusListener(_openAnimationListener);
    _counterController.dispose();
    super.dispose();
  }

  void _openAnimationListener(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      /// Update counter
      if (_counter > 1) {
        setState(() {
          _counter -= 1;
        });
        _counterController
          ..reset()
          ..forward();
      } else {
        if (context.read<GameStatusCubit>().state is GameStatusPaused) {
          /// Resume game
          context.read<GameStatusCubit>().resume();
          widget.game.router.pop();
        } else {
          /// Start game
          context.read<GameStatusCubit>().start();
          widget.game.router.pushReplacementNamed(Routes.game);
        }
      }
    }
  }

  Future<void> _setupAndStartAnimation() async {
    _counterController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );

    final Tween<double> offsetTween = Tween<double>(
      begin: 1.0,
      end: 0.0,
    );

    // Setting of the animation
    _counterAnimation = offsetTween.animate(
      CurvedAnimation(
        parent: _counterController,
        curve: Curves.linearToEaseOut,
      ),
    )..addStatusListener(_openAnimationListener);

    if (mounted) {
      _counterController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: FadeTransition(
          opacity: _counterAnimation,
          child: AnimatedBuilder(
            animation: _counterAnimation,
            builder: (BuildContext context, Widget? child) {
              return Text(
                _counter.toString(),
                style: context.textTheme.displayLarge?.copyWith(
                  fontSize: context.textTheme.displayLarge!.fontSize! * 2 +
                      context.textTheme.displayLarge!.fontSize! * _counterAnimation.value,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
