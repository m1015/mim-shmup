import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/core/utils/database/high_scores.service.dart';
import 'package:mim_shmup/core/utils/di.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/utils/upper_case_text.formatter.dart';
import 'package:mim_shmup/presentation/widgets/custom_button.widget.dart';
import 'package:mim_shmup/presentation/widgets/focusable_button.widget.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class EndGamePage extends StatefulWidget {
  final MiMGame game;

  const EndGamePage({super.key, required this.game});

  @override
  State<EndGamePage> createState() => _EndGamePageState();
}

class _EndGamePageState extends State<EndGamePage> with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  final FocusNode _focusNode = FocusNode();
  final Key _pinKey = UniqueKey();
  final Key _backKey = UniqueKey();

  late int _currentScore;
  late HighScoresService _highScores;

  late AnimationController _animationsController;
  late Animation<double> _youWonAnimation;
  late Animation<double> _congratsAnimation;

  int _counting = 3;
  String _playerName = '';
  Key? _currentFocus;
  Timer? _countDown;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);

    _highScores = getIt<HighScoresService>();

    _setupAnimations();

    _animationsController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _countDown = Timer.periodic(
          const Duration(seconds: 1),
          (Timer timer) {
            if (_counting == 0) {
              setState(() => timer.cancel());
            } else {
              setState(() => _counting--);
            }
          },
        );
      }
    });

    _focusNode.onKeyEvent = (FocusNode node, KeyEvent event) {
      if (_currentFocus == _pinKey) {
        if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
          setState(() {
            _currentFocus = _backKey;
          });
          return KeyEventResult.ignored;
        } else if (event.logicalKey == LogicalKeyboardKey.arrowUp) {
          setState(() {
            _currentFocus = _backKey;
          });
          return KeyEventResult.ignored;
        }
      } else {
        setState(() {
          _currentFocus = _pinKey;
        });
      }
      return KeyEventResult.ignored;
    };

    _animationsController.forward();

    super.initState();
  }

  @override
  void didChangeDependencies() {
    _currentScore = widget.game.gameSession?.currentScore ?? 0;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _animationsController.dispose();
    _countDown?.cancel();

    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final textSize = context.textTheme.displayMedium!.fontSize!;

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AnimatedBuilder(
            animation: _youWonAnimation,
            builder: (context, child) {
              return Text(
                context.trad.endGameYouWon,
                style: context.textTheme.displayMedium!.copyWith(fontSize: textSize * _youWonAnimation.value),
              );
            },
          ),
          SizedBox(height: MiMSizes.large),
          AnimatedBuilder(
            animation: _congratsAnimation,
            builder: (context, child) {
              return Text(
                context.trad.endGameCongrats,
                style: context.textTheme.displayMedium!.copyWith(fontSize: textSize * _congratsAnimation.value),
              );
            },
          ),
          SizedBox(height: MiMSizes.large),
          PinCodeTextField(
            key: _pinKey,
            appContext: context,
            showCursor: true,
            cursorColor: halloweenOrange,
            autoFocus: true,
            focusNode: _focusNode,
            mainAxisAlignment: MainAxisAlignment.center,
            length: 3,
            keyboardType: TextInputType.name,
            textStyle: context.textTheme.headlineMedium,
            inputFormatters: [
              UpperCaseTextFormatter(),
            ],
            pinTheme: PinTheme(
              activeColor: halloweenOrange,
              selectedColor: halloweenOrange,
              inactiveColor: halloweenOrange,
              disabledColor: halloweenOrange,
              activeFillColor: halloweenOrange,
              selectedFillColor: halloweenOrange,
              inactiveFillColor: halloweenOrange,
              errorBorderColor: halloweenOrange,
              shape: PinCodeFieldShape.box,
              fieldOuterPadding: EdgeInsets.symmetric(horizontal: MiMSizes.small),
              fieldHeight: MiMSizes.xLarge,
            ),
            onCompleted: (String player) async {
              _playerName = player;
              setState(() {
                _currentFocus = _backKey;
              });
            },
          ),
          SizedBox(height: MiMSizes.large),
          FocusableButton(
            buttonKey: _backKey,
            label: context.trad.backToStart,
            focused: _currentFocus == _backKey,
            onPressed: () async {
              if (_playerName.isNotEmpty) {
                await _highScores.saveHighScore(_playerName, _currentScore);
              }
              _goBackToStartScreen();
            },
            size: ButtonSize.small,
            onFocusChanged: (key) => setState(() => _currentFocus = key),
          ),
        ],
      ),
    );
  }

  Future<void> _setupAnimations() async {
    _animationsController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 3),
    );

    /// Setting of the animations
    _youWonAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationsController,
        curve: const Interval(
          0.0,
          0.5,
          curve: Curves.fastOutSlowIn,
        ),
      ),
    );

    _congratsAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationsController,
        curve: const Interval(
          0.5,
          1,
          curve: Curves.fastOutSlowIn,
        ),
      ),
    );
  }

  void _goBackToStartScreen() {
    FocusManager.instance.primaryFocus?.unfocus();
    Future.delayed(const Duration(milliseconds: 500), () async {
      if (context.mounted) context.read<GameStatusCubit>().reset();
      widget.game.router.pushReplacementNamed(Routes.start);
    });
  }
}
