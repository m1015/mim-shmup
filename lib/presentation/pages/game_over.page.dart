import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/core/utils/database/high_scores.service.dart';
import 'package:mim_shmup/core/utils/di.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/utils/upper_case_text.formatter.dart';
import 'package:mim_shmup/presentation/widgets/custom_animated.page.dart';
import 'package:mim_shmup/presentation/widgets/custom_button.widget.dart';
import 'package:mim_shmup/presentation/widgets/focusable_button.widget.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class GameOverPage extends StatefulWidget {
  final MiMGame game;

  const GameOverPage({
    super.key,
    required this.game,
  });

  @override
  State<GameOverPage> createState() => _GameOverPageState();
}

class _GameOverPageState extends State<GameOverPage> {
  final FocusNode _focusNode = FocusNode();
  final Key _pinKey = UniqueKey();
  final Key _restartKey = UniqueKey();
  final Key _backKey = UniqueKey();

  int? _playerHighScore;

  int _highScore = 0;

  String _playerName = '';

  late HighScoresService _highScores;

  late int _currentScore;

  Timer? _countDown;

  Key? _currentFocus;

  @override
  void initState() {
    super.initState();
    _countDown = Timer(const Duration(seconds: 30), () => _goBackToStartScreen());
    _highScores = getIt<HighScoresService>();
    _highScores.loadAllHighScores().then((savedScores) {
      if (savedScores.isNotEmpty) {
        _highScore = savedScores[0].score;
      }
    });
    _currentFocus = _pinKey;

    _focusNode.onKeyEvent = (FocusNode node, KeyEvent event) {
      if (_currentFocus == _pinKey) {
        if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
          setState(() {
            _currentFocus = _restartKey;
          });
          return KeyEventResult.ignored;
        } else if (event.logicalKey == LogicalKeyboardKey.arrowUp) {
          setState(() {
            _currentFocus = _backKey;
          });
          return KeyEventResult.ignored;
        }
      } else {
        setState(() {
          _currentFocus = _pinKey;
        });
      }
      return KeyEventResult.ignored;
    };
  }

  @override
  void didChangeDependencies() {
    _currentScore = widget.game.gameSession?.currentScore ?? 0;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _countDown?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomAnimatedPage(
      heightFactor: 0.8,
      child: FocusScope(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              context.trad.gameOver,
              style: context.textTheme.displayMedium,
            ),
            PinCodeTextField(
              key: _pinKey,
              appContext: context,
              showCursor: true,
              cursorColor: halloweenOrange,
              autoFocus: true,
              focusNode: _focusNode,
              mainAxisAlignment: MainAxisAlignment.center,
              length: 3,
              keyboardType: TextInputType.name,
              textStyle: context.textTheme.headlineMedium,
              inputFormatters: [
                UpperCaseTextFormatter(),
              ],
              pinTheme: PinTheme(
                activeColor: halloweenOrange,
                selectedColor: halloweenOrange,
                inactiveColor: halloweenOrange,
                disabledColor: halloweenOrange,
                activeFillColor: halloweenOrange,
                selectedFillColor: halloweenOrange,
                inactiveFillColor: halloweenOrange,
                errorBorderColor: halloweenOrange,
                shape: PinCodeFieldShape.box,
                fieldOuterPadding: EdgeInsets.symmetric(horizontal: MiMSizes.small),
                fieldHeight: MiMSizes.xLarge,
              ),
              onCompleted: (String player) async {
                _playerName = player;
                int score = (await _highScores.loadHighScoreForPlayer(player))?.score ?? 0;
                setState(() {
                  _playerHighScore = max(score, _currentScore);
                  _currentFocus = _restartKey;
                });
              },
            ),
            if (_currentScore <= _highScore)
              Text(
                context.trad.currentScore(_currentScore),
                style: context.textTheme.headlineMedium,
              ),
            if (_currentScore > _highScore)
              Text(
                context.trad.newHighScore,
                style: context.textTheme.headlineMedium!.copyWith(color: gameYellow),
              ),
            Text(
              context.trad.highScore(_highScore),
              style: context.textTheme.headlineMedium,
            ),
            FocusableButton(
              buttonKey: _restartKey,
              label: context.trad.restart,
              focused: _currentFocus == _restartKey,
              onPressed: () async {
                FocusManager.instance.primaryFocus?.unfocus();
                Future.delayed(const Duration(milliseconds: 500), () async {
                  if (_playerName.isNotEmpty && _currentScore >= (_playerHighScore ?? 0)) {
                    await _highScores.saveHighScore(_playerName, _currentScore);
                  }
                  if (context.mounted) context.read<GameStatusCubit>().starting();
                  widget.game.router.pushReplacementNamed(Routes.countDown);
                });
              },
              onFocusChanged: (key) => setState(() => _currentFocus = key),
            ),
            FocusableButton(
              buttonKey: _backKey,
              label: context.trad.backToStart,
              focused: _currentFocus == _backKey,
              onPressed: () async {
                if (_playerName.isNotEmpty && _currentScore >= (_playerHighScore ?? 0)) {
                  await _highScores.saveHighScore(_playerName, _currentScore);
                }
                _goBackToStartScreen();
              },
              size: ButtonSize.small,
              onFocusChanged: (key) => setState(() => _currentFocus = key),
            ),
          ],
        ),
      ),
    );
  }

  void _goBackToStartScreen() {
    FocusManager.instance.primaryFocus?.unfocus();
    Future.delayed(const Duration(milliseconds: 500), () async {
      if (context.mounted) context.read<GameStatusCubit>().reset();
      widget.game.router.pushReplacementNamed(Routes.start);
    });
  }
}
