import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/game/components/hud/hud_score.component.dart';
import 'package:mim_shmup/game/controllers/game_level_controller.component.dart';
import 'package:mim_shmup/game/controllers/game_phase_controller.component.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/presentation/widgets/custom_animated.page.dart';
import 'package:mim_shmup/presentation/widgets/custom_button.widget.dart';

class RewardsPage extends StatefulWidget {
  final MiMGame game;

  const RewardsPage({super.key, required this.game});

  @override
  State<RewardsPage> createState() => _RewardsPageState();
}

class _RewardsPageState extends State<RewardsPage> with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  late GameLevelController _gameLevelController;

  late AnimationController _animationsController;

  late Animation<double> _enemiesAnimation;
  late Animation<double> _untouchableAnimation;
  late Animation<double> _levelCompletedAnimation;

  Timer? _countDown;
  int _counting = 3;
  bool _paused = false;

  bool _enemiesRewardAdded = false;
  bool _untouchableRewardAdded = false;

  final FocusNode _keyboardNode = FocusNode();
  final FocusNode _buttonNode = FocusNode();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);

    _gameLevelController = widget.game.findByKeyName(GameConstants.levelController)!;

    _setupAnimations();

    _animationsController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _countDown = Timer.periodic(
          const Duration(seconds: 1),
              (Timer timer) {
            if (_counting == 0) {
              setState(() => timer.cancel());
              _continue();
            } else {
              setState(() => _counting--);
            }
          },
        );
      }
    });

    _keyboardNode.requestFocus();
    _buttonNode.requestFocus();

    super.initState();
  }

  @override
  void dispose() {
    _animationsController.dispose();
    _countDown?.cancel();

    _keyboardNode.dispose();
    _buttonNode.dispose();

    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState appState) {
    switch (appState) {
      case AppLifecycleState.resumed:

      /// do nothing
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
      case AppLifecycleState.hidden:
        _finishAnimation();
        _countDown?.cancel();
        setState(() => _paused = true);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final textSize = context.textTheme.displayMedium!.fontSize!;

    return KeyboardListener(
      focusNode: _keyboardNode,
      autofocus: true,
      onKeyEvent: (event) => _finishAnimation(),
      child: GestureDetector(
        onTap: () => _finishAnimation(),
        child: CustomAnimatedPage(
          heightFactor: 0.8,
          statusListener: (status) {
            if (status == PageStatus.opened) {
              _animationsController.forward();
            }
          },
          child: Padding(
            padding: EdgeInsets.all(MiMSizes.regular),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedBuilder(
                  animation: _enemiesAnimation,
                  builder: (context, child) {
                    final enemiesReward = widget.game.getEnemiesReward();
                    if (_enemiesAnimation.value == _gameLevelController.totalLevelEnemies && !_enemiesRewardAdded) {
                      _enemiesRewardAdded = true;
                      widget.game.gameSession!.addScore(enemiesReward);
                    }
                    return Column(
                      children: [
                        Text(
                          context.trad.rewardsEnemiesDestroyed(
                              (_enemiesAnimation.value * 100 / _gameLevelController.totalLevelEnemies).floor()),
                          style: context.textTheme.displayMedium!.copyWith(
                              color: _enemiesAnimation.value == _gameLevelController.totalLevelEnemies
                                  ? halloweenOrange
                                  : null),
                        ),
                        if (_enemiesAnimation.value == _gameLevelController.totalLevelEnemies) ...[
                          SizedBox(height: MiMSizes.large),
                          Text(
                            '+$enemiesReward',
                            style: context.textTheme.displayMedium!.copyWith(color: halloweenOrange),
                          ),
                        ],
                      ],
                    );
                  },
                ),
                SizedBox(height: MiMSizes.large),
                _gameLevelController.hasBeenShot
                    ? AnimatedBuilder(
                  animation: _untouchableAnimation,
                  builder: (context, child) {
                    return Text(
                      context.trad.rewardsUntouchableFailure,
                      style: context.textTheme.displayMedium!
                          .copyWith(fontSize: textSize * _untouchableAnimation.value),
                    );
                  },
                )
                    : AnimatedBuilder(
                  animation: _untouchableAnimation,
                  builder: (context, child) {
                    final untouchableReward = widget.game.getUntouchableReward();
                    if (_untouchableAnimation.value == 1 && !_untouchableRewardAdded) {
                      _untouchableRewardAdded = true;
                      widget.game.gameSession!.addScore(untouchableReward);
                    }
                    return Column(
                      children: [
                        Text(
                          context.trad.rewardsUntouchableSuccess,
                          style: context.textTheme.displayMedium!
                              .copyWith(fontSize: textSize * _untouchableAnimation.value, color: halloweenOrange),
                        ),
                        if (_untouchableAnimation.value == 1) ...[
                          SizedBox(height: MiMSizes.large),
                          Text(
                            '+$untouchableReward',
                            style: context.textTheme.displayMedium!.copyWith(color: halloweenOrange),
                          ),
                        ],
                      ],
                    );
                  },
                ),
                SizedBox(height: MiMSizes.large),
                AnimatedBuilder(
                  animation: _levelCompletedAnimation,
                  builder: (context, child) {
                    return Column(children: [
                      Text(
                        context.trad.rewardsLevelCompleted,
                        style: context.textTheme.displayMedium!.copyWith(
                          fontSize: textSize * _levelCompletedAnimation.value,
                          color: halloweenOrange,
                        ),
                      ),
                      SizedBox(height: MiMSizes.large),
                      if (_countDown != null || _paused)
                        CustomButtonWidget(
                          focusNode: _buttonNode,
                          label: _paused ? context.trad.rewardContinueAction : _counting.toString(),
                          onPressed: _continue,
                        ),
                    ]);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  TickerFuture _finishAnimation() => _animationsController.animateTo(1, duration: const Duration(seconds: 0));

  Future<void> _setupAnimations() async {
    _animationsController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 6),
    );

    /// Setting of the animations
    _enemiesAnimation = Tween<double>(
      begin: 0.0,
      end: _gameLevelController.destroyedLevelEnemies.toDouble(),
    ).animate(
      CurvedAnimation(
        parent: _animationsController,
        curve: const Interval(
          0.0,
          0.33,
          curve: Curves.linearToEaseOut,
        ),
      ),
    );

    _untouchableAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationsController,
        curve: const Interval(
          0.34,
          0.66,
          curve: Curves.fastOutSlowIn,
        ),
      ),
    );

    _levelCompletedAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationsController,
        curve: const Interval(
          0.67,
          1.0,
          curve: Curves.fastOutSlowIn,
        ),
      ),
    );
  }

  void _continue() {
    widget.game.router.pop();
    widget.game.findByKeyName<HudScoreComponent>(GameConstants.hudScore)?.setFinalScore();
    widget.game.findByKeyName<GamePhaseController>(GameConstants.phaseController)?.currentPhase.setFinished();
  }
}
