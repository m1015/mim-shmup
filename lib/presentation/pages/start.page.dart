import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_shmup/core/constants/color.constants.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/presentation/cubits/game_status.cubit.dart';
import 'package:mim_shmup/presentation/widgets/custom_animated.page.dart';
import 'package:mim_shmup/presentation/widgets/custom_button.widget.dart';

class StartPage extends StatefulWidget {
  final MiMGame game;

  const StartPage({
    super.key,
    required this.game,
  });

  @override
  State<StartPage> createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  Timer? _countDown;
  final FocusNode _node = FocusNode();

  @override
  void initState() {
    _countDown = Timer(const Duration(seconds: 10), () => _gotToLeaderboard());
    _node.requestFocus();
    super.initState();
  }

  @override
  void dispose() {
    _countDown?.cancel();
    _node.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomAnimatedPage(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            GameConstants.gameName,
            style: context.textTheme.displayLarge,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                style: ButtonStyle(
                  overlayColor: WidgetStateProperty.resolveWith<Color>(
                    (Set<WidgetState> states) {
                      if (states.contains(WidgetState.focused)) {
                        return focusedButton;
                      }
                      return unFocusedButton;
                    },
                  ),
                ),
                onPressed: () => _gotToSettings(),
                icon: Icon(
                  Icons.settings_outlined,
                  size: MiMSizes.xLarge,
                ),
              ),
              CustomButtonWidget(
                focusNode: _node,
                label: context.trad.start,
                onPressed: () {
                  context.read<GameStatusCubit>().starting();
                  widget.game.router.pushReplacementNamed(Routes.countDown);
                },
              ),
              IconButton(
                style: ButtonStyle(
                  overlayColor: WidgetStateProperty.resolveWith<Color>(
                    (Set<WidgetState> states) {
                      if (states.contains(WidgetState.focused)) {
                        return focusedButton;
                      }
                      return unFocusedButton;
                    },
                  ),
                ),
                onPressed: () => _gotToLeaderboard(),
                icon: Icon(
                  Icons.emoji_events_outlined,
                  size: MiMSizes.xLarge,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _gotToLeaderboard() => widget.game.router.pushReplacementNamed(Routes.leaderboard);

  void _gotToSettings() => widget.game.router.pushReplacementNamed(Routes.settings);
}
