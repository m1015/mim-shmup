import 'package:flutter/material.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/presentation/widgets/custom_animated.page.dart';
import 'package:mim_shmup/presentation/widgets/custom_button.widget.dart';

class PausePage extends StatefulWidget {
  final MiMGame game;

  const PausePage({
    super.key,
    required this.game,
  });

  @override
  State<PausePage> createState() => _PausePageState();
}

class _PausePageState extends State<PausePage> {
  final FocusNode _node = FocusNode();

  @override
  void initState() {
    _node.requestFocus();
    super.initState();
  }

  @override
  void dispose() {
    _node.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomAnimatedPage(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            context.trad.pause,
            style: context.textTheme.displayLarge,
          ),
          CustomButtonWidget(
            focusNode: _node,
            label: context.trad.resume,
            onPressed: () => widget.game.router.pushReplacementNamed(Routes.countDown),
          ),
        ],
      ),
    );
  }
}
