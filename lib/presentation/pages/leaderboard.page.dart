import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/core/utils/database/high_scores.service.dart';
import 'package:mim_shmup/core/utils/di.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/presentation/widgets/custom_animated.page.dart';

class LeaderboardPage extends StatefulWidget {
  final MiMGame game;

  const LeaderboardPage({
    super.key,
    required this.game,
  });

  @override
  State<LeaderboardPage> createState() => _LeaderboardPageState();
}

class _LeaderboardPageState extends State<LeaderboardPage> {
  late HighScoresService _highScores;

  Timer? _countDown;

  @override
  void initState() {
    super.initState();

    /// we wait for the last events to disappear
    Future.delayed(const Duration(seconds: 1), () => ServicesBinding.instance.keyboard.addHandler(_onKey));
    _countDown = Timer(const Duration(seconds: 10), () => _goToStartPage());
    _highScores = getIt<HighScoresService>();
  }

  @override
  void dispose() {
    ServicesBinding.instance.keyboard.removeHandler(_onKey);
    _countDown?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _goToStartPage(),
      child: CustomAnimatedPage(
        child: Padding(
          padding: EdgeInsets.all(MiMSizes.regular),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  context.trad.highScores,
                  style: context.textTheme.headlineLarge,
                ),
                FutureBuilder(
                  future: _highScores.loadAllHighScores(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      final scores = snapshot.data!;
                      return Padding(
                        padding: EdgeInsets.all(MiMSizes.xxxLarge),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ...scores.map(
                              (score) => Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    score.player,
                                    style: context.textTheme.headlineMedium,
                                  ),
                                  Text(
                                    score.score.toString(),
                                    style: context.textTheme.headlineMedium,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    return const SizedBox();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _goToStartPage() => widget.game.router.pushReplacementNamed(Routes.start);

  bool _onKey(KeyEvent event) {
    if (event is KeyUpEvent) {
      _goToStartPage();
      return true;
    }

    return false;
  }
}
