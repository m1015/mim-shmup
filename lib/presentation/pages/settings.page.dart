import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/sizes.constants.dart';
import 'package:mim_shmup/core/extensions/context.extension.dart';
import 'package:mim_shmup/core/utils/database/settings.service.dart';
import 'package:mim_shmup/core/utils/di.dart';
import 'package:mim_shmup/core/utils/platform.utils.dart';
import 'package:mim_shmup/game/mim_game.dart';
import 'package:mim_shmup/game/router/routes.dart';
import 'package:mim_shmup/presentation/widgets/custom_animated.page.dart';
import 'package:mim_shmup/presentation/widgets/custom_button.widget.dart';

class SettingsPage extends StatelessWidget {
  final MiMGame game;
  final SettingsService _parameters = getIt<SettingsService>();

  SettingsPage({super.key, required this.game});

  @override
  Widget build(BuildContext context) {
    return CustomAnimatedPage(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            context.trad.settings,
            style: context.textTheme.headlineLarge,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                context.trad.onScreenGamePad,
                style: context.textTheme.titleLarge,
              ),
              SizedBox(width: MiMSizes.medium),
              _ToggleButtons(
                value: _parameters.getBoolParameters(Settings.onScreenGamepad) ?? PlatformUtils.isMobile,
                listener: (value) => _parameters.setBoolParameter(Settings.onScreenGamepad, value),
                firstIcon: Icons.smartphone_outlined,
                secondIcon: Icons.sports_esports_outlined,
              ),
            ],
          ),
          if (PlatformUtils.isMobile)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  context.trad.screenOrientation,
                  style: context.textTheme.titleLarge,
                ),
                SizedBox(width: MiMSizes.medium),
                _ToggleButtons(
                  value: _parameters.getBoolParameters(Settings.portraitOrientation) ?? PlatformUtils.isMobile,
                  listener: (value) => _parameters.setBoolParameter(Settings.portraitOrientation, value),
                  firstIcon: Icons.screen_lock_portrait_outlined,
                  secondIcon: Icons.screen_lock_landscape_outlined,
                ),
              ],
            ),
          CustomButtonWidget(
            label: context.trad.done,
            onPressed: () => game.router.pushReplacementNamed(Routes.start),
            size: ButtonSize.small,
          )
        ],
      ),
    );
  }
}

class _ToggleButtons extends StatefulWidget {
  final bool value;
  final IconData firstIcon;
  final IconData secondIcon;
  final Function(bool) listener;

  const _ToggleButtons({
    required this.value,
    required this.listener,
    required this.firstIcon,
    required this.secondIcon,
  });

  @override
  State<_ToggleButtons> createState() => _ToggleButtonsState();
}

class _ToggleButtonsState extends State<_ToggleButtons> {
  final List<bool> _isSelected = [false, false];
  final FocusNode _firstNode = FocusNode();
  final FocusNode _secondNode = FocusNode();

  @override
  void didChangeDependencies() {
    if (widget.value) {
      _firstNode.requestFocus();
    } else {
      _secondNode.requestFocus();
    }
    _toggle(widget.value ? 0 : 1);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return ToggleButtons(
      isSelected: _isSelected,
      onPressed: _toggle,
      focusNodes: [
        _firstNode,
        _secondNode,
      ],
      children: [
        Icon(widget.firstIcon),
        Icon(widget.secondIcon),
      ],
    );
  }

  void _toggle(int index) {
    setState(() {
      _isSelected[0] = false;
      _isSelected[1] = false;
      _isSelected[index] = true;
      widget.listener(index == 0);
    });
  }
}
