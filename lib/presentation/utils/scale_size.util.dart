import 'dart:developer';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:mim_shmup/core/constants/game.constants.dart';

class ScaleSize {
  static double textScaleFactor(BuildContext context, {double maxTextScaleFactor = 1}) {
    final height = MediaQuery.of(context).size.height;
    double val = (height / GameConstants.baseScreenHeight) * maxTextScaleFactor;
    log('DEBUG --- $val');
    return math.min(val, maxTextScaleFactor);
  }
}
